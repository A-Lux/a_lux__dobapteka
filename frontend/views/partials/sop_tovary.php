<div class="owl1 owl-carousel owl-theme ">
    <? foreach ($sop_tovary as $v) : ?>

        <div class="item">
            <a href="<?=$v->getUrl();?>">
                <div class="top-sale-item">

                    <a href="<?=$v->getUrl();?>">
                        <img src="<?=$v->getImage();?>" >
                    </a>
                    <div class="name">
                        <p><?= $v->name ?></p>
                    </div>
                    <div class="price">
                        <p class="gradient-text"><?= number_format($v->price, 0, '', ' '); ?> <span> тг</span></p>
                    </div>
                    <div class="basket-button">
                        <button class="btn-in-basket" data-id="<?= $v->id; ?>"><img src="/images/light-basket.png" alt="">В корзину</button>
                    </div>
                </div>
            </a>
        </div>

    <? endforeach; ?>
</div>
