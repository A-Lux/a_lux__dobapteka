<div class="catalog-menu">
    <div class="title gradient-text">
        Каталог товаров
    </div>
    <ul>
        <? foreach (Yii::$app->view->params['catalog'] as $k => $v):?>
        <? $childs = $v->childs;?>
        <li>
            <a href="<?= $v->getUrl(); ?>">
                <span class="menu-icon"><img src="<?=$v->getDesktopImage();?>" alt=""></span>
                <p><?=$v->name;?></p>
            </a>
            <div class="modal-drug">

                <? $col = count($childs) / 2.0;?>
                <? $col2 = count($childs) % 2.0;?>
            <div class="modal-drug-menus">
                <ul class="second-level-menu px-0">
                    <? $m = 0;?>
                    <? foreach ($childs as $child):?>

                    <? if($col2 == 0):?>
                    <? $m++;?>
                    <? endif;?>

                    <? if($col >= $m):?>
                    <li class="px-0"><a class="px-2" href="<?= $child->getUrl(); ?>"><?=$child->name;?></a></li>
                    <? endif;?>

                    <? if($col2 != 0):?>
                    <? $m++;?>
                    <? endif;?>

                    <? endforeach;?>
                </ul>
                <ul class="second-level-menu px-0">
                    <? $m = 0;?>
                    <? foreach ($childs as $child):?>

                    <? if($col2 == 0):?>
                    <? $m++;?>
                    <? endif;?>

                    <? if($col < $m):?>
                    <li class="px-0"><a class="px-2" href="<?= $child->getUrl(); ?>"><?=$child->name;?></a></li>
                    <? endif;?>

                    <? if($col2 != 0):?>
                    <? $m++;?>
                    <? endif;?>

                    <? endforeach;?>
                </ul>
            </div>

            <div class="link-catalog"><a href="<?= $v->getUrl(); ?>">Перейти к товарам</a></div>

            </div>

        </li>
        <? endforeach;?>
    </ul>
</div>
