<?

use yii\helpers\Url;
use yii\widgets\LinkPager;


?>

<style>
    /* ===========
    pagination
    ==============*/
    .pagination {
        padding-left: 0;
        margin: 0 0 50px;
        border-radius: 4px;
    }

    .pagination>li>a,
    .pagination>li>span {
        position: relative;
        float: left;
        padding: 6px 12px;
        margin-left: 5px;
        line-height: 1.42857143;
        color: #333;
        text-decoration: none;
        background-color: #fff;
        border: 0;
        -webkit-transition: all 0.3s;
        transition: all 0.3s;
        font-weight: 600;

    }

    .pagination>li:first-child>a,
    .pagination>li:first-child>span {
        margin-left: 0;
        border-top-left-radius: 0;
        border-bottom-left-radius: 0;
    }

    .pagination>li>a:focus,
    .pagination>li>a:hover,
    .pagination>li>span:focus,
    .pagination>li>span:hover {
        z-index: 2;
        color: #ffffff;
        background-color: #68c316;
    }

    .pagination>li:last-child>a,
    .pagination>li:last-child>span {
        border-top-right-radius: 0;
        border-bottom-right-radius: 0;
    }

    .pagination>.active>a,
    .pagination>.active>a:focus,
    .pagination>.active>a:hover,
    .pagination>.active>span,
    .pagination>.active>span:focus,
    .pagination>.active>span:hover {
        z-index: 3;
        color: #fff;
        background-color: #68c316;
    }
</style>

<div class="main">
    <div class="desktop-version serch-page">
        <div class="container">
            <div class="row">
                <div class="col-lg-9 col-md-6 col-sm-12">
                    <div class="row-list" style="margin-bottom: 30px;margin-top: 30px;">
                        <div class="title gradient-text">
                            <?php
                            if ($count) { ?>
                                <div style="font-size: 16px;">Результаты поиска по запросу <span style="font-weight: bold;"><?= $keyword ?></span></div>
                            <? } else { ?>
                                <div class="" style="font-size: 22px;">По запросу <span style="font-weight: bold;"><?= $keyword ?></span> ничего не найдено</div>
                            <? } ?>
                        </div>
                    </div>
                    <? if ($count) : ?>
                        <div class="tablet-version-hide row wow fadeInUp">
                            <? foreach ($product as $v) : ?>
                                <div class="col-sm-12 col-md-12 col-lg-6 mb-4">
                                    <div class="catalog-item mb-0">
                                        <div class="image">
                                            <a href="<?=$v->getUrl();?>">
                                                <img src="<?=$v->getImage();?>" alt="">
                                            </a>
                                        </div>
                                        <div class="text-block">
                                            <? if ($v->status_products) { ?>
                                                <div class="red-label">
                                                    дефицит
                                                </div>
                                            <? } ?>
                                            <div class="name">
                                                <a href="<?=$v->getUrl();?>" style="text-decoration: none;">
                                                    <p><?= $v->name ?></p>
                                                </a>
                                            </div>
                                            <div class="status">
                                                <p><img src="/images/ok.png" alt="">Есть в наличии</p>
                                                <? if (Yii::$app->user->isGuest) : ?><a href="#"><i class="fas fa-heart favorite" onclick="locateToSignIn()"></i></a>
                                                <? else : ?><i class="fas fa-heart like-active favorites <?= $v->getFavoriteStatus() ? 'favorites-active' : ''; ?>" data-id="<?= $v->id; ?>" title="Добавить в избранное"></i>
                                                <? endif; ?>
                                            </div>
                                            <div class="flex-one">
                                                <div class="price">
                                                    <p class="gradient-text"><?= $v->getCalculatePrice() ?> <span> тг</span></p>
                                                </div>
                                                <div class="catalog-basket-button">
                                                    <button class="btn-in-basket" data-id="<?= $v->id; ?>"><img src="/images/light-basket.png" alt="">В корзину</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <? endforeach; ?>



                        </div>

                        <?= LinkPager::widget([
                            'pagination' => $pagination,
                            'maxButtonCount' => 10
                        ]);
                        ?>

                    <? endif; ?>
                </div>

                <div class="tablet-version col-sm-12">
                    <? if ($product != null) : ?>
                        <div class="row">

                            <? foreach ($product as $v) : ?>
                                <div class="col-sm-12 col-md-12 col-lg-6">
                                    <div class="catalog-item">
                                        <div class="image">
                                            <a href="<?=$v->getUrl();?>">
                                                <img src="<?=$v->getImage();?>" alt="">
                                            </a>
                                        </div>
                                        <div class="text-block">
                                            <? if ($v->status_products) { ?>
                                                <div class="red-label">
                                                    дефицит
                                                </div>
                                            <? } ?>
                                            <div class="name">
                                                <p><?= $v->name ?></p>
                                            </div>
                                            <div class="status">
                                                <p><img src="/images/ok.png" alt="">Есть в наличии</p>
                                                <? if (Yii::$app->user->isGuest) : ?><a href="#"><i class="fas fa-heart favorite" onclick="locateToSignIn()"></i></a>
                                                <? else : ?><i class="fas fa-heart like-active favorites <?= $v->getFavoriteStatus() ? 'favorites-active' : ''; ?>" data-id="<?= $v->id; ?>" title="Добавить в избранное"></i>
                                                <? endif; ?>
                                            </div>
                                            <div class="flex-one">
                                                <div class="price">
                                                    <p class="gradient-text"><?= $v->price ?> <span> тг</span></p>
                                                </div>
                                                <div class="catalog-basket-button">
                                                    <button class="btn-in-basket" data-id="<?= $v->id; ?>"><img src="/images/light-basket.png" alt="">В корзину</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <? endforeach; ?>


                        </div>

                        <?= LinkPager::widget([
                            'pagination' => $pagination,
                            'maxButtonCount' => 10
                        ]);
                        ?>
                    <? endif; ?>
                </div>
            </div>
        </div>
    </div>


    <div class="mobile-version mobile-version-search">
        <div class="main-content">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 wow fadeInUp">
                        <div class="row-list" style="margin-bottom: 30px">
                            <div class="title gradient-text">
                                <?php
                                if ($count) { ?>
                                    <div style="font-size: 16px;">Результаты поиска по запросу <span style="font-weight: bold;"><?= $keyword ?></span>.</div>
                                <? } else { ?>
                                    <div class="" style="font-size: 22px;">По запросу <span style="font-weight: bold;"><?= $keyword ?></span> ничего не найдено.</div>
                                <? } ?>
                            </div>
                        </div>
                        <div class="tab-content tablet-version-hide-mob items">
                            <div class="tab-item tab-item-active" id="home">
                                <div class="row">
                                    <? if ($product != null) : ?>
                                        <? foreach ($product as $v) { ?>
                                            <div class="col-sm-12 col-md-12 col-lg-6">
                                                <div class="catalog-item">
                                                    <div class="image">
                                                        <a href="<?=$v->getUrl();?>">
                                                            <img src="<?=$v->getImage();?>" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="text-block">
                                                        <? if ($v->status_products) { ?>
                                                            <div class="red-label">
                                                                дефицит
                                                            </div>
                                                        <? } ?>
                                                        <div class="name">
                                                            <a href="<?=$v->getUrl();?>">
                                                                <p><?= $v->name ?></p>
                                                            </a>
                                                        </div>
                                                        <div class="status">
                                                            <p><img src="/images/ok.png" alt="">Есть в наличии</p>
                                                            <? if (Yii::$app->user->isGuest) : ?><a href="#"><i class="fas fa-heart favorite" onclick="locateToSignIn()"></i></a>
                                                            <? else : ?><i class="fas fa-heart like-active favorites <?= $v->getFavoriteStatus() ? 'favorites-active' : ''; ?>" data-id="<?= $v->id; ?>" title="Добавить в избранное"></i>
                                                            <? endif; ?>
                                                        </div>
                                                        <div class="flex-one">
                                                            <div class="price">
                                                                <p class="gradient-text"><?= $v->price ?> <span> тг.</span></p>
                                                            </div>
                                                            <div class="catalog-basket-button">
                                                                <button class="btn-in-basket" data-id="<?= $v->id; ?>"><img src="/images/light-basket.png" alt="">В корзину</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <? } ?>

                                        <?= LinkPager::widget([
                                            'pagination' => $pagination,
                                            'maxButtonCount' => 7
                                        ]);
                                        ?>
                                    <? endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
