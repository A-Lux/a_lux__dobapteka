<? if (!Yii::$app->user->isGuest && $user_addresses != null) : ?>
    <div class="basket-select">
        <select id="typeAddress" onchange="setAddress($(this).val())">
            <option value="0">Выберите адрес</option>
            <? foreach ($user_addresses as $v) : ?>
                <option value="<?= $v->address; ?>"><?= $v->address; ?></option>
            <? endforeach; ?>
        </select>
    </div>
    <div id="user_address" style="visibility:hidden"></div>
<? endif; ?>

<div class="delivery-cost">
    <a href="#" data-toggle="modal" data-target="#exampleModal">Укажите адрес доставки на карте</a>
</div>


<div class="delivery-cost">
    <p>Адрес доставки:</p>
    <div class="price">
        <p class="gradient-text" id="deliveryAddress"></p>
    </div>
</div>



<!-- <div id="map" style="width: 100%; height: 300px; float:left;"></div> -->
<div id="viewContainer"></div>
<div class="modal modal-map fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="w-50 adress-title">
                    <h5 class="modal-title" id="exampleModalLabel">Адрес доставки</h5>
                    <div class="search-map">
                        <form>
                            <!-- <span deliveryAddressclass="delete" onclick="deleteBasketSearchText()"> &#10005</span> -->
                            <input type="text" id="addressSearch" placeholder="ул. Макатаева, д. 28, стр. 1"
                                   class="deliveryAddressInput" style="width: 72%;">
                            <input type="number" id="apartment" placeholder="Кв" class="deliveryApartmentInput" style="width: 26%;padding:0.5rem;">
                        </form>
                        <button class="btn-map" id="btnSaveDelivery" onclick="saveAddress()">Хорошо</button>
                        <button class="btn-map-disabled" id="btnSaveDeliveryDisabled" disabled>Хорошо</button>

                    </div>
                </div>
                <div class="delivery-modal">

                    <div class="delivery-cost">
                        <p>Сумма за доставку:</p>
                        <div class="price">
                            <p class="gradient-text deliveryPrice"></p>
                        </div>
                    </div>
                    <div class="delivery-cost">
                        <p>Время доставки:</p>
                        <div class="price">
                            <p class="gradient-text deliveryTime"></p>
                        </div>
                    </div>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
            <div class="modal-body p-0">
                <div id="map" style="width: 100%; height: 67vh; float:left;"></div>
            </div>
        </div>
    </div>
</div>
<script>

    ymaps.ready(init);

    function init() {
        var myPlacemark, myMap = new ymaps.Map("map", {
            center: [43.238293, 76.945465],
            zoom: 11,
            controls: ['zoomControl']
        }, {
            searchControlProvider: 'yandex#search'
        });

        var myGeocoder = ymaps.geocode("<?= Yii::$app->session["city_name"]; ?>");
        myGeocoder.then(
            function(res) {
                var street = res.geoObjects.get(0);
                var coords = street.geometry.getCoordinates();
                myMap.setCenter(coords);
            },
            function(err) {

            }
        );


        $("#addressSearch").autocomplete({
            source: function(request, response) {
                hideSaveButton();

                var address = request.term;
                var myGeocoder = ymaps.geocode("<?= Yii::$app->session["city_name"]; ?>, " + address);
                myGeocoder.then(
                    function(res) {
                        var coords = res.geoObjects.get(0).geometry.getCoordinates();
                        var myGeocoder = ymaps.geocode(coords, {
                            kind: 'house'
                        });
                        myGeocoder.then(
                            function(res) {
                                var myGeoObjects = res.geoObjects;
                                var addresses = [];
                                myGeoObjects.each(function(el, i) {
                                    var arr = [];
                                    arr['title'] = el.properties.get('name');
                                    arr['value'] = '<?= Yii::$app->session["city_name"]; ?> , ' + el.properties.get('name');
                                    arr['coords'] = el.geometry.getCoordinates();
                                    addresses.push(arr);
                                });

                                response($.map(addresses, function(address) {
                                    return {
                                        label: address.title,
                                        value: address.value,
                                        coords: address.coords
                                    }
                                }));
                            }
                        );
                    },
                    function(err) {
                        alert('Ошибка');
                    }
                );

            },
            minLength: 2,
            mustMatch: true,
            select: function(event, ui) {
                $('#addressSearch').val(ui.item.value);

                // --- проверка точки кординаты внутри ли адрес доставки
                var inPolygon = false;
                <? foreach ($model as $v) : ?>
                    <? $array = unserialize($v->coords); ?>
                    var coords = [];
                    <? foreach ($array[0] as $arr) : ?>
                        coords.push([<?= $arr[0] ?>, <?= $arr[1] ?>]);
                    <? endforeach; ?>

                    var myPolygon = new ymaps.Polygon([coords]);
                    myPolygon.options.setParent(myMap.options);
                    myPolygon.geometry.setMap(myMap);
                    if (myPolygon.geometry.contains(ui.item.coords)) {

                        inPolygon = true;
                        <? $sum = $v->summ; ?>
                        <? $time = $v->name; ?>

                        // if selected address in current polygon
                        myMap.geoObjects.remove(myColumn);


                        var coord = ui.item.coords;

                        $.ajax({
                            url: "/card/update-session",
                            type: "GET",
                            data: {
                                deliveryPrice: <?= $sum ?>,
                                deliveryTime: <?= $time; ?>,
                                longitude: coord[1].toPrecision(9),
                                latitude: coord[0].toPrecision(9),
                            },
                            dataType: "json",
                            success: function(data) {
                                if (data.status == 1) {
                                    $('.deliveryPrice').html(data.deliveryPrice + " <span> тг</span>");
                                    $('.deliveryTime').html("<?= $v->name; ?> <span>минут</span>");

                                    $('.delivery-fucking-cost p')[0].innerText = 'Сумма доставки';
                                    $('#deliveryFuckingPrice').html(data.deliveryPrice + " <span> тг</span>");

                                    $('.total-cost')[1].children[0].innerText = 'Итого с доставкой';
                                    $('#sumBasket').html(data.sum + " <span> тг</span>");

                                    document.getElementById('typeAddress').value = 0;
                                }
                            },
                            error: function() {
                                swal('Упс!', 'Что-то пошло не так.', 'error');
                            }
                        });

                        placemark(myMap, ui.item.coords);
                        showSaveButton();

                        return false;
                    }

                <? endforeach; ?>
                // ---

                if (!inPolygon) {
                    swal('', 'К сожалению, доставка в данной черте города не осуществляется', 'error');
                    return false;
                }


            },
            open: function() {
                $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
            },
            close: function() {
                $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
            }
        });


        <? foreach ($model as $v) { ?>
            <? $array = unserialize($v->coords); ?>
            var coords = [];
            <? foreach ($array[0] as $arr) { ?>
                coords.push([<?= $arr[0] ?>, <?= $arr[1] ?>]);
            <? } ?>
            var myPolygon = new ymaps.Polygon([coords], {
                // Описываем свойства геообъекта.
                // Содержимое балуна.
                hintContent: "Адрес доставки"
            }, {
                // Задаем опции геообъекта.
                // Цвет заливки.
                fillColor: '#00FF0088',
            });

            myMap.geoObjects.add(myPolygon);
            var myColumn;
            myPolygon.events.add('click', function(e) {
                myMap.geoObjects.remove(myColumn);

                var coords = e.get('coords');

                $.ajax({
                    url: "/card/update-session",
                    type: "GET",
                    data: {
                        deliveryPrice: <?= $v->summ ?>,
                        deliveryTime: <?= $v->name; ?>,
                        longitude: coords[1].toPrecision(9),
                        latitude: coords[0].toPrecision(9),
                    },
                    dataType: "json",
                    success: function(data) {
                        if (data.status == 1) {
                            $('.deliveryPrice').html(data.deliveryPrice + " <span> тг</span>");
                            $('.deliveryTime').html("<?= $v->name; ?> <span>минут</span>");

                            $('.delivery-fucking-cost p')[0].innerText = 'Сумма доставки';
                            $('#deliveryFuckingPrice').html(data.deliveryPrice + " <span> тг</span>");

                            $('.total-cost')[1].children[0].innerText = 'Итого с доставкой';
                            $('#sumBasket').html(data.sum + " <span> тг</span>");
                        }
                    },
                    error: function() {
                        swal('Упс!', 'Что-то пошло не так.', 'error');
                    }
                });

                placemark(myMap, coords);
                showSaveButton();

            });
        <? } ?>

        function placemark(myMap, coords) {
            myColumn = new ymaps.Placemark(coords, {
                balloonContent: ''
            }, {
                preset: 'islands#icon',
                iconColor: '#0095b6',
                strokeWidth: 0
            });
            myMap.geoObjects.add(myColumn);

            // Если метка уже создана – просто передвигаем ее.
            if (myPlacemark) {
                myPlacemark.geometry.setCoordinates(coords);
            }
            // Если нет – создаем.
            else {
                myPlacemark = createPlacemark(coords);
                myMap.geoObjects.add(myPlacemark);
                // Слушаем событие окончания перетаскивания на метке.
                myPlacemark.events.add('dragend', function() {
                    getAddress(myPlacemark.geometry.getCoordinates());
                });
            }
            getAddress(coords);
        }


        // Создание метки.
        function createPlacemark(coords) {
            return new ymaps.Placemark(coords, {
                iconCaption: 'поиск...'
            }, {
                preset: 'islands#violetDotIconWithCaption',
                draggable: true
            });
        }
        // Определяем адрес по координатам (обратное геокодирование).
        function getAddress(coords) {
            myPlacemark.properties.set('iconCaption', 'поиск...');
            ymaps.geocode(coords).then(function(res) {
                var firstGeoObject = res.geoObjects.get(0);

                myPlacemark.properties
                    .set({
                        // Формируем строку с данными об объекте.
                        iconCaption: [
                            // Название населенного пункта или вышестоящее административно-территориальное образование.
                            firstGeoObject.getLocalities().length ? firstGeoObject.getLocalities() : firstGeoObject.getAdministrativeAreas(),
                            // Получаем путь до топонима, если метод вернул null, запрашиваем наименование здания.
                            firstGeoObject.getThoroughfare() || firstGeoObject.getPremise()
                        ].filter(Boolean).join(', '),
                        // В качестве контента балуна задаем строку с адресом объекта.
                        balloonContent: firstGeoObject.getAddressLine()
                    });
                $('.deliveryAddress').html(firstGeoObject.getAddressLine());
                $('.deliveryAddressInput').val(firstGeoObject.getAddressLine());
                $('#deliveryAddress').html(firstGeoObject.getAddressLine());
            });
        }




        function showSaveButton() {
            $('#btnSaveDeliveryDisabled').hide();
            $('#btnSaveDelivery').show();
        }



        function hideSaveButton() {
            $('#btnSaveDeliveryDisabled').show();
            $('#btnSaveDelivery').hide();
            $('.deliveryPrice').html("");
            $('.deliveryTime').html("");
        }


    }


    function saveAddress() {

        var address = $('.deliveryAddressInput').val();
        var apartment = $('#apartment').val();
        if(apartment == ''){
            swal('', 'Необходимо указать номер квартиры!', 'error');

        }else{
            $('#deliveryAddress').html(address+', '+apartment+'кв.');
            $(".close").click();
        }

    }




    function deleteBasketSearchText() {
        $('.deliveryAddressInput').val("");
        $('#deliveryAddress').html("");
        $('#btnSaveDeliveryDisabled').show();
        $('#btnSaveDelivery').hide();
        $('.deliveryPrice').html("");
        $('.deliveryTime').html("");
    }




    function setAddress(address) {


        if (address != 0) {
            showLoader();
            ymaps.ready(init);

            function init() {
                var map = new ymaps.Map("user_address", {
                    center: [43.238293, 76.945465],
                    zoom: 11,
                    controls: ['zoomControl']
                }, {
                    searchControlProvider: 'yandex#search'
                });

                var myGeocoder = ymaps.geocode(address);
                myGeocoder.then(
                    function(res) {

                        if (typeof res.geoObjects.get(0) == "undefined") {
                            swal('', 'К сожалению, ваше адрес не нейдено на карте', 'error');
                        } else {

                            var street = res.geoObjects.get(0);
                            var coord = street.geometry.getCoordinates();
                            // --- проверка точки кординаты внутри ли адрес доставки
                            var inPolygon = false;
                            <? foreach ($model as $v) : ?>
                                <? $array = unserialize($v->coords); ?>
                                var coords = [];

                                <? foreach ($array[0] as $arr) : ?>
                                    coords.push([<?= $arr[0] ?>, <?= $arr[1] ?>]);
                                <? endforeach; ?>

                                var myPolygon = new ymaps.Polygon([coords]);
                                myPolygon.options.setParent(map.options);
                                myPolygon.geometry.setMap(map);
                                if (myPolygon.geometry.contains(coord)) {
                                    inPolygon = true;
                                    <? $sum = $v->summ; ?>
                                    <? $time = $v->name; ?>



                                    $.ajax({
                                        url: "/card/update-session",
                                        type: "GET",
                                        data: {
                                            deliveryPrice: <?= $sum ?>,
                                            deliveryTime: <?= $time; ?>,
                                            longitude: coord[1].toPrecision(9),
                                            latitude: coord[0].toPrecision(9),
                                        },
                                        dataType: "json",
                                        success: function(data) {
                                            if (data.status == 1) {
                                                $('.deliveryPrice').html(data.deliveryPrice + " <span> тг</span>");
                                                $('.deliveryTime').html("<?= $v->name; ?> <span>минут</span>");

                                                $('.delivery-fucking-cost p')[0].innerText = 'Сумма доставки';
                                                $('#deliveryFuckingPrice').html(data.deliveryPrice + " <span> тг</span>");

                                                $('.total-cost')[1].children[0].innerText = 'Итого с доставкой';
                                                $('#sumBasket').html(data.sum + " <span> тг</span>");

                                                $('#deliveryAddress').html(address);
                                                hideLoader();
                                            }
                                        },
                                        error: function() {
                                            hideLoader();
                                            swal('Упс!', 'Что-то пошло не так.', 'error');
                                        }
                                    });
                                }

                            <? endforeach; ?>
                            // --- END


                            if (!inPolygon) {
                                hideLoader();
                                swal('', 'К сожалению, доставка в данной черте города не осуществляется', 'error');
                            }

                        }

                    },
                    function(err) {
                        hideLoader();
                        swal('', 'К сожалению, ваше адрес не нейдено на карте', 'error');
                    }
                );


            }
        }
    }
</script>
