<? $m = 0; ?>
<? foreach ($model as $v) : ?>
    <? $m++; ?>
    <? if ($m < $count) : ?>

        <div class="col-lg-3 col-md-6 col-sm-12 wow fadeInUp">
            <a href="<?=$v->getUrl();?>">
                <div class="top-sale-item">
                    <a href="product-card.php">
                        <? if ($v->isHit == 1) : ?>
                            <div class="hit">
                                <img src="images/hit.png" alt="">
                            </div>
                        <? endif; ?>

                        <a href="<?=$v->getUrl();?>">
                            <img src="<?=$v->getImage();?>" >
                        </a>
                        <div class="name">
                            <a href="<?=$v->getUrl();?>" style="text-decoration: none;">
                                <p><?= $v->name ?></p>
                            </a>
                        </div>
                        <div class="price">
                            <p class="gradient-text"><?= number_format($v->calculatePrice, 0, '', ' '); ?><span> тг</span></p>
                        </div>
                        <div class="basket-button">
                            <button class="btn-in-basket" data-id="<?= $v->id; ?>" data-slider-id="<?= $id; ?>">
                                <img src="/images/light-basket.png" alt="">В корзину</button>
                        </div>
                    </a>

                </div>
            </a>
        </div>

    <? endif; ?>
<? endforeach; ?>
