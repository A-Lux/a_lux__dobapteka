<?php

use yii\widgets\LinkPager;

?>

<style>
    /* ===========
    pagination
    ==============*/
    .pagination {
        padding-left: 0;
        margin: 0 0 50px;
        border-radius: 4px;
    }

    .pagination>li>a,
    .pagination>li>span {
        position: relative;
        float: left;
        padding: 6px 12px;
        margin-left: 5px;
        line-height: 1.42857143;
        color: #333;
        text-decoration: none;
        background-color: #fff;
        border: 0;
        -webkit-transition: all 0.3s;
        transition: all 0.3s;
        font-weight: 600;

    }

    .pagination>li:first-child>a,
    .pagination>li:first-child>span {
        margin-left: 0;
        border-top-left-radius: 0;
        border-bottom-left-radius: 0;
    }

    .pagination>li>a:focus,
    .pagination>li>a:hover,
    .pagination>li>span:focus,
    .pagination>li>span:hover {
        z-index: 2;
        color: #ffffff;
        background-color: #68c316;
    }

    .pagination>li:last-child>a,
    .pagination>li:last-child>span {
        border-top-right-radius: 0;
        border-bottom-right-radius: 0;
    }

    .pagination>.active>a,
    .pagination>.active>a:focus,
    .pagination>.active>a:hover,
    .pagination>.active>span,
    .pagination>.active>span:focus,
    .pagination>.active>span:hover {
        z-index: 3;
        color: #fff;
        background-color: #68c316;
    }
</style>
<div class="main">
    <div class="tab-line">
        <div class="container">
            <ul class="nav nav-tabs wow slideInDown" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link <?= $tab == 'home' ? 'active' : ''; ?>" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Личные данные</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link <?= $tab == 'orders' ? 'active' : ''; ?>" id="profile-tab" data-toggle="tab" href="#orders" role="tab" aria-controls="profile" aria-selected="false">Заказы</a>
                </li>
<!--                <li class="nav-item">-->
<!--                    <a class="nav-link --><?//= $tab == 'bonus' ? 'active' : ''; ?><!--" id="contact-tab" data-toggle="tab" href="#bonus" role="tab" aria-controls="bonus" aria-selected="false">Бонусы</a>-->
<!--                </li>-->
                <!--                <li class="nav-item">-->
                <!--                    <a class="nav-link --><? //=$tab=='gift'?'active':'';
                                                                ?>
                <!--" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Подарки</a>-->
                <!--                </li>-->
                <li class="nav-item">
                    <a class="nav-link <?= $tab == 'favorite' ? 'active' : ''; ?>" id="contact-tab" data-toggle="tab" href="#favorite" role="tab" aria-controls="favorite" aria-selected="false">Избранное</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="white-section">
        <div class="container">
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane <?= $tab == 'home' ? 'fade  show active' : ''; ?>" id="home" role="tabpanel" aria-labelledby="home-tab">
                    <form class="row">
                        <input type="hidden" name="<?= Yii::$app->request->csrfParam ?>" value="<?= Yii::$app->request->getCsrfToken() ?>" />
                        <div class="col-sm-6 wow fadeInLeft">

                            <div class="personal-input">
                                <div class="icon">
                                    <img src="/images/user-icon-light.png" alt="">
                                </div>
                                <input type="text" placeholder="ФИО" value="<?= $profile->fio; ?>" name="UserProfile[fio]">
                            </div>
                            <div class="personal-input">
                                <div class="icon">
                                    <img src="/images/msg-icon.png" alt="">
                                </div>
                                <input type="text" placeholder="Почта" value="<?= $user->email; ?>" name="User[email]">
                            </div>

                            <? foreach ($address as $v) : ?>
                                <div class="personal-input">
                                    <div class="icon">
                                        <img src="/images/loc-icon.png" alt="">
                                    </div>
                                    <input type="text" class="searcherrr" placeholder="Алматы, улица Зауыт, 1Б"
                                           value="<?= $v->address; ?>" name="UserAddress[address][]">

                                </div>
                            <? endforeach; ?>

                            <div class="field_wrapper">
                                <div>

                                </div>
                                <div class="add-address gradient-text">
                                    <a class="add_button" href="javascript:void(0);">+ Добавить адрес</a>
                                    <div id="map" style="width: 0; height: 0; float:left;"></div>
                                </div>
                                <script>

                                    // $('input[name="UserAddress[address][]"]').inputmask({ mask: "Алматы, i{0,}",
                                    //     definitions: {
                                    //         "i": {
                                    //             validator: ".",
                                    //             definitionSymbol: "i"
                                    //         }
                                    //     },});

                                    ymaps.ready(init);

                                    function init() {
                                        var myPlacemark, myMap = new ymaps.Map("map", {
                                            center: [43.238293, 76.945465],
                                            zoom: 11,
                                            controls: ['zoomControl']
                                        }, {
                                            searchControlProvider: 'yandex#search'
                                        });

                                        var myGeocoder = ymaps.geocode("<?= Yii::$app->session["city_name"]; ?>");
                                        myGeocoder.then(
                                            function(res) {
                                                var street = res.geoObjects.get(0);
                                                var coords = street.geometry.getCoordinates();
                                                myMap.setCenter(coords);
                                            },
                                            function(err) {

                                            }
                                        );

                                        $(`input[name="UserAddress[address][]"]`).autocomplete({
                                            source: function(request, response) {
                                                var address = request.term;
                                                var myGeocoder = ymaps.geocode("<?= Yii::$app->session["city_name"]; ?>, " + address);
                                                myGeocoder.then(
                                                    function(res) {
                                                        var coords = res.geoObjects.get(0).geometry.getCoordinates();
                                                        var myGeocoder = ymaps.geocode(coords, {
                                                            kind: 'house'
                                                        });
                                                        myGeocoder.then(
                                                            function(res) {
                                                                var myGeoObjects = res.geoObjects;
                                                                var addresses = [];
                                                                myGeoObjects.each(function(el, i) {
                                                                    var arr = [];
                                                                    arr['title'] = el.properties.get('name');
                                                                    arr['value'] = "<?= Yii::$app->session["city_name"]; ?>, " +  el.properties.get('name');
                                                                    arr['coords'] = el.geometry.getCoordinates();
                                                                    addresses.push(arr);
                                                                });

                                                                response($.map(addresses, function(address) {
                                                                    return {
                                                                        label: address.title,
                                                                        value: address.value,
                                                                        coords: address.coords
                                                                    }
                                                                }));
                                                            }
                                                        );
                                                    },
                                                    function(err) {
                                                        alert('Ошибка');
                                                    }
                                                );
                                            },
                                            minLength: 2,
                                            mustMatch: true
                                        });

                                    }
                                </script>
                                <!-- <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAzvvm_ui7_AaOY4TtbhpdZ2ifac3vBWVc&libraries=places"></script> -->
                            </div>
                        </div>
                        <div class="col-sm-6 wow fadeInRight">

                            <div class="personal-input">
                                <div class="icon">
                                    <img src="/images/phone-icon-light.png" alt="">
                                </div>
                                <input type="text" placeholder="Номер" value="<?= $user->username; ?>" name="User[username]" class="telephone">
                                <script>
                                    $('.telephone').inputmask("8(999) 999-9999");
                                </script>
                            </div>

                            <div class="personal-input">
                                <div class="icon">
                                    <img src="/images/lock-icon.png" alt="">
                                </div>
                                <input type="password" id="myInput" placeholder="Пароль" value="<?= $user->password; ?>" name="User[password]">
                                <button class="show-pass" type="button" onclick="showPass()"><img src="/images/show-icon.png" alt=""></button>
                            </div>

                            <div class="personal-input">
                                <div class="icon">
                                    <img src="/images/lock-icon.png" alt="">
                                </div>
                                <input type="password" id="myInput2" placeholder="Повторить пароль" value="<?= $user->password_verify; ?>" name="User[password_verify]">
                                <button class="show-pass" type="button" onclick="showPass2()"><img src="/images/show-icon.png" alt=""></button>
                            </div>
                            <div class="personal-input">
                                <div class="icon">
                                    <img src="/images/date-icon.png" alt="">
                                </div>
                                <input type="date" placeholder="Дата рождения" value="<?= $profile->date_of_birth; ?>" name="UserProfile[date_of_birth]" <?= $profile->date_of_birth != null ? "disabled" : ""; ?>>
                            </div>
                        </div>
                        <div class="col-sm-12 wow fadeInUp">
                            <div class="save-btn">
                                <button type="button" class="update-profile-button">Сохранить изменения</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="tab-pane <?= $tab == 'orders' ? 'fade  show active' : ''; ?>" id="orders" role="tabpanel" aria-labelledby="profile-tab">
                    <div class="row">
                        <div class="col-sm-6 ">
                            <? if ($orders['data'] == null) : ?>
                                <div>
                                    <p> Список заказов пуст!</p>
                                </div>
                            <? endif; ?>
                            <? $class = ""; ?>
                            <? foreach ($orders['data'] as $v) : ?>

                                <div class="purchased-item <?= $class; ?>">
                                    <a href="order-details?id=<?= $v->id; ?>" style="text-decoration: none;">
                                        <div class="title">
                                            <div class="text">
                                                <div class="left">
                                                    <p>Номер заказа:</p>
                                                </div>
                                                <div class="right">
                                                    <p><?= $v->id; ?></p>
                                                </div>
                                            </div>
                                            <div class="text">
                                                <div class="left">
                                                    <p>Статус заказа:</p>
                                                </div>
                                                <div class="right green-text">
                                                    <? if ($v->typeDelivery == 1) : ?>
                                                        <? if ($v->statusProgress == 1) : ?>
                                                            <p class="gradient-text">Доставлено</p>
                                                        <? else : ?>
                                                            <p class="red-text">Не доставлено</p>
                                                        <? endif; ?>
                                                    <? else : ?>
                                                        <? if ($v->statusProgress == 1) : ?>
                                                            <p class="gradient-text">Получено</p>
                                                        <? else : ?>
                                                            <p class="red-text">Не получено</p>
                                                        <? endif; ?>
                                                    <? endif; ?>
                                                </div>
                                            </div>

                                            <div class="text">
                                                <div class="left">
                                                    <p>Статус оплаты:</p>
                                                </div>
                                                <div class="right green-text">
                                                    <? if ($v->statusPay == 1) : ?>
                                                        <p class="gradient-text">Оплачено</p>
                                                    <? else : ?>
                                                        <p class="red-text">Не оплачено</p>
                                                    <? endif; ?>
                                                </div>
                                            </div>
                                            <div class="text">
                                                <div class="left">
                                                    <p>Цена:</p>
                                                </div>
                                                <div class="right">
                                                    <p><?= number_format($v->sum, 0, '', ' '); ?> тг</p>
                                                </div>
                                            </div>
                                            <? if ($v->typeDelivery == 1) : ?>
                                                <div class="text">
                                                    <div class="left">
                                                        <p>Дата прибытия:</p>
                                                    </div>
                                                    <div class="right">
                                                        <p><?= $v->arrivalDate; ?></p>
                                                    </div>
                                                </div>
                                            <? else : ?>
                                                <div class="text">
                                                    <div class="left">
                                                        <p>Адрес забора:</p>
                                                    </div>
                                                    <div class="right">
                                                        <p><?= $v->address; ?></p>
                                                    </div>
                                                </div>
                                            <? endif; ?>
                                        </div>
                                    </a>
                                </div>
                                <? $class = "space-top-sm"; ?>

                            <? endforeach; ?>

                            <? if ($orders['data'] != null) : ?>
                                <p>Нажмите на номер заказа, чтобы увидеть детали Вашего заказа</p>
                            <? endif; ?>

                            <?= LinkPager::widget([
                                'pagination' => $orders['pagination'],
                                'maxButtonCount' => 10
                            ]);
                            ?>

                        </div>
                    </div>
                </div>
<!--                <div class="tab-pane --><?//= $tab == 'bonus' ? 'fade  show active' : ''; ?><!--" id="bonus" role="tabpanel" aria-labelledby="contact-tab">-->
<!--                    <div class="row">-->
<!--                        <div class="col-sm-4 ">-->
<!--                            <img src="/images/bonus-img.png" class="img-fluid" alt="">-->
<!--                        </div>-->
<!--                        <div class="col-sm-8 ">-->
<!--                            <div class="bonus-title">-->
<!--                                <h5>Бонусы</h5>-->
<!--                            </div>-->
<!--                            <div class="bonus-text">-->
<!--                                <span class="gradient-text">--><?//= $balance->sum; ?><!--</span> бонусов-->
<!--                            </div>-->
<!---->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->

                <!--                <div class="tab-pane --><? //=$tab=='gift'?'fade  show active':'';
                                                            ?>
                <!--" id="contact" role="tabpanel" aria-labelledby="contact-tab">-->
                <!--                    <div class="wow fadeInUp">-->
                <!--                        --><? //if($gift == null):
                                                ?>
                <!--                            <div >-->
                <!--                                <p> Список подарков пуст!</p>-->
                <!--                            </div>-->
                <!--                        --><? // endif;
                                                ?>
                <!--                        --><? //if($gift != null):
                                                ?>
                <!--                            --><? // foreach ($gift as $v):
                                                    ?>
                <!--                                --><? // $product = $v->product;
                                                        ?>
                <!--                                <div class="col-sm-3">-->
                <!--                                    <div class="gift-item">-->
                <!--                                        --><? //$img = unserialize($product->images);$img = $img[0]
                                                                ?>
                <!--                                        <a href="/product/--><? //=$product->url
                                                                                    ?>
                <!--">-->
                <!--                                            <img src="/backend/web/--><? //=$product->path.$img
                                                                                            ?>
                <!--" alt="">-->
                <!--                                        </a>-->
                <!--                                        <p>--><? //=$product->name
                                                                    ?>
                <!--</p>-->
                <!--                                        <p><b> --><? //=$v->quantity;
                                                                        ?>
                <!-- штук</b></p>-->
                <!--                                        <div class="free">-->
                <!--                                            <p class="gradient-text">Бесплатно</p>-->
                <!--                                        </div>-->
                <!--                                        <div class="checked">-->
                <!--                                            <img src="/images/check.png" alt="">-->
                <!--                                        </div>-->
                <!--                                    </div>-->
                <!--                                </div>-->
                <!--                            --><? // endforeach;
                                                    ?>
                <!--                            <div class="col-sm-12">-->
                <!--                                <div class="basket-button">-->
                <!--                                    <button style="margin: 40px 0;" id="gift-in-basket"><img src="/images/light-basket.png"  >В корзину</button>-->
                <!--                                </div>-->
                <!--                            </div>-->
                <!--                        --><? // endif;
                                                ?>
                <!--                    </div>-->
                <!--                </div>-->


                <div class="tab-pane <?= $tab == 'favorite' ? 'fade  show active' : ''; ?>" id="favorite" role="tabpanel" aria-labelledby="contact-tab">
                    <div class="row">
                        <? if ($fav == null) : ?>
                            <div>
                                <p>В избранном отсутствуют товары.</p>
                            </div>
                        <? endif; ?>
                        <? if ($fav != null) : ?>
                            <? foreach ($fav as $v) : ?>
                                <div class="col-sm-4">
                                    <div class="catalog-item">
                                        <div class="image">
                                            <a href="<?=$v->getUrl();?>">
                                                <img src="<?=$v->getImage();?>" alt="">
                                            </a>
                                        </div>
                                        <div class="text-block">
                                            <div class="delete-icon btn-delete-product-from-favorite" data-id="<?= $v->id; ?>">
                                                &#10005
                                            </div>
                                            <div class="name">
                                                <p><?= $v->name ?></p>
                                            </div>
                                            <div class="status">
                                                <p><img src="/images/ok.png" alt=""><? if ($v->status == 1) { ?>Есть<? } else { ?>Нет<? } ?>
                                                    в наличии</p>
                                            </div>
                                            <div class="flex-one">
                                                <div class="price">
                                                    <p class="gradient-text"><?= number_format($v->price, 0, '', ' '); ?> <span> тг</span></p>
                                                </div>
                                                <div class="sale-basket-button">
                                                    <button class="btn-in-basket" data-id="<?= $v->id; ?>"><img src="/images/light-basket.png" alt="">В корзину</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <? endforeach; ?>
                        <? endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
</div>
