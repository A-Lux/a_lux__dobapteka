<?


?>
<div class="main">
    <div class="desktop-version">
        <div class="bg-white">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 wow fadeIn">
                        <div class="register-title text-center gradient-text">
                            <h5>Авторизация</h5>
                        </div>
                        <div class="register-by  gradient-text">
                            <p>Войти c помощью:</p>
                            <?= yii\authclient\widgets\AuthChoice::widget([
                                'baseAuthUrl' => ['account/auth'],
                                'popupMode' => false,
                            ]) ?>
                        </div>
                    </div>
                    <?php if (Yii::$app->session->getFlash('facebook_error')) : ?>
                        <div class="alert alert-danger" role="alert">
                            <?= Yii::$app->session->getFlash('facebook_error'); ?>
                        </div>
                    <?php endif; ?>

                    <form class="offset-lg-4 col-lg-4 col-sm-12 wow fadeInUp">
                        <input type="hidden" name="<?= Yii::$app->request->csrfParam ?>" value="<?= Yii::$app->request->getCsrfToken() ?>" />
                        <div class="personal-input">
                            <div class="icon">
                                <img src="/images/user-icon-light.png" alt="">
                            </div>
                            <input type="text" placeholder="8(___)__-__" name="LoginForm[username]" class="telephone">
                            <script>
                                $('.telephone').inputmask("8(999) 999-9999");
                            </script>
                        </div>
                        <div class="personal-input">
                            <div class="icon">
                                <img src="/images/lock-icon.png" alt="">
                            </div>
                            <input type="password" id="myInput" placeholder="Пароль" name="LoginForm[password]">
                            <button class="show-pass" type="button" onclick="showPass()"><img src="/images/show-icon.png" alt=""></button>
                        </div>
                        <div class="button-wrap">
                            <div class="basket-button">
                                <button type="button" class="login-button">Войти</button>
                            </div>
                            <div class="save-btn">
                                <a href="sign-up"><button type="button">Зарегистрироваться </button></a>
                            </div>
                        </div>
                        <div class="wrapper-pass-reset">
                            <button type="button" class="btn" data-toggle="modal" data-target="#pass-reset"> Забыли пароль </button>
                        </div>
                    </form>
                    <!-- <div class="col-sm-12 wow fadeInUp"> -->
                    <!-- <div class="register-bonus">
                            <p>Начисление бонусов при регистрации</p>
                            <p><span class="gradient-text">500</span> тг!</p>
                        </div> -->
                    <!-- </div> -->
                </div>
            </div>
        </div>
    </div>
    <div class="mobile-version">
        <div class="bg-white">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="register-title text-center gradient-text wow fadeInUp">
                            <h5>Авторизация</h5>
                        </div>
                        <div class="register-by  gradient-text">
                            <p>Войти c помощью:</p>
                            <div class="row">
                                <?= yii\authclient\widgets\AuthChoice::widget([
                                    'baseAuthUrl' => ['account/auth'],
                                    'popupMode' => false,
                                ]) ?>
                            </div>
                        </div>
                    </div>

                    <form class="offset-lg-4 col-lg-4 col-sm-12 wow fadeInUp">
                        <input type="hidden" name="<?= Yii::$app->request->csrfParam ?>" value="<?= Yii::$app->request->getCsrfToken() ?>" />
                        <div class="personal-input">
                            <div class="icon">
                                <img src="/images/user-icon-light.png" alt="">
                            </div>
                            <input type="text" placeholder="8(___)__-__" name="LoginForm[username]" class="telephone_mobile">
                            <script>
                                $('input[name="LoginForm[username]"]').inputmask("8(999) 999-9999");
                            </script>
                        </div>
                        <div class="personal-input">
                            <div class="icon">
                                <img src="/images/lock-icon.png" alt="">
                            </div>
                            <input type="password" id="myInput2" placeholder="Пароль" name="LoginForm[password]">
                            <button class="show-pass" onclick="showPass2()" type="button"><img src="/images/show-icon.png" alt=""></button>
                        </div>
                        <div class="button-wrap2">
                            <div class="save-btn">
                                <button type="button" class="login-button-mobile">Войти</button>
                            </div>
                            <div class="basket-button">
                                <a href="sign-up"><button type="button">Зарегистрироваться </button></a>
                            </div>
                        </div>

                        <div class="wrapper-pass-reset">
                            <button type="button" class="btn" data-toggle="modal" data-target="#pass-reset"> Забыли пароль </button>
                        </div>
                    </form>
<!--                    <div class="col-sm-12 wow fadeInUp">-->
<!--                        <div class="register-bonus">-->
<!--                            <p>Начисление бонусов при регистрации</p>-->
<!--                            <p><span class="gradient-text">500</span> тг!</p>-->
<!--                        </div>-->
<!--                    </div>-->
                </div>
            </div>
        </div>
    </div>
</div>
</div>
