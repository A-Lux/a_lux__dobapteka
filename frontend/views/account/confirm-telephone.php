<div class="main">
    <div class="desktop-version">
        <div class="bg-white">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 wow fadeIn">
                        <div class="register-title text-center gradient-text">
                            <h5>Укажите ваш телефон</h5>
                        </div>
                    </div>

                    <form class="offset-lg-4 col-lg-4 col-sm-12 wow fadeInUp">
                        <input type="hidden" name="<?= Yii::$app->request->csrfParam ?>" value="<?= Yii::$app->request->getCsrfToken() ?>"/>
                        <div class="personal-input">
                            <div class="icon">
                                <img src="/images/user-icon-light.png" alt="" >
                            </div>
                            <input type="text"  placeholder="8(___)__-__" name="User[username]" class="telephone">
                            <script>
                                $('input[name="User[username]"]').inputmask("8(999) 999-9999");
                            </script>
                        </div>
                        <div class="button-wrap">
                            <div class="basket-button">
                                <button type="button" class="confirm-telephone">Подвердить</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="mobile-version">
        <div class="bg-white">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="register-title text-center gradient-text wow fadeInUp">
                            <h5>Укажите ваш телефон</h5>
                        </div>
                    </div>

                    <form class="offset-lg-4 col-lg-4 col-sm-12 wow fadeInUp">
                        <input type="hidden" name="<?= Yii::$app->request->csrfParam ?>" value="<?= Yii::$app->request->getCsrfToken() ?>"/>
                        <div class="personal-input">
                            <div class="icon">
                                <img src="/images/user-icon-light.png" alt="">
                            </div>
                            <input type="text"  placeholder="8(___)__-__" name="User[username]" class="telephone_mobile">
                            <script>
                                $('input[name="User[username]"]').inputmask("8(999) 999-9999");
                            </script>
                        </div>
                        <div class="button-wrap2">
                            <div class="save-btn">
                                <button type="button" class="confirm-telephone-mobile">Подвердить</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>