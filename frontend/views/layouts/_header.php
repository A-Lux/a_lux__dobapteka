<script>
    <? if (Yii::$app->session['modal-pass-reset']) : ?>
    swal("Уважаемый пользователь!", "Ваш старый пароль был сброшен. Новый пароль был отправлен на вашу электронную почту.");
    <? unset(Yii::$app->session['modal-pass-reset']); ?>
    <? endif; ?>

    <? if (Yii::$app->session['guest-order-success']) : ?>
    swal("Спасибо!", "Ваш заказ принят!", "success");
    <? unset(Yii::$app->session['guest-order-success']); ?>
    <? endif; ?>
</script>


<div class="message">
    <span>Обращаем Ваше внимание, что возможны задержки в доставке лекарств по г. Алматы!</span>
</div>
<div class="overlay">
    <div class="pick-city-modal">
        <div class="close-modal">
            &#10005
        </div>
        <h2>Выберите ваш город</h2>
        <div class="city-list">

            <? $len = count(Yii::$app->view->params['cities']); ?>
            <ul>
                <? $m = 0; ?>
                <? foreach (Yii::$app->view->params['cities'] as $v) : ?>
                <? if ($len / 3 >= $m) : ?>
                <li><a href="/city/set-city?city=<?= $v->city_id; ?>"><?= $v->city->name; ?></a></li>
                <? endif; ?>
                <? $m++; ?>
                <? endforeach; ?>

            </ul>

            <ul>
                <? $m = 0; ?>
                <? foreach (Yii::$app->view->params['cities'] as $v) : ?>
                <? if ($len / 3 < $m && ($len / 3) * 2 >= $m) : ?>
                <li><a href="/city/set-city?city=<?= $v->city_id; ?>"><?= $v->city->name; ?></a></li>
                <? endif; ?>
                <? $m++; ?>
                <? endforeach; ?>
            </ul>
            <ul>
                <? $m = 0; ?>
                <? foreach (Yii::$app->view->params['cities'] as $v) : ?>
                <? if (($len / 3) * 2 < $m) : ?>
                <li><a href="/city/set-city?city=<?= $v->city_id; ?>"><?= $v->city->name; ?></a></li>
                <? endif; ?>
                <? $m++; ?>
                <? endforeach; ?>
            </ul>
        </div>
    </div>
</div>
<div class="content">
    <? if (!isMobile()) : ?>

    <div class="desktop-version">
        <header class="header">
            <div class="container">
                <div class="row">
                    <div class="col-sm-2">
                        <div class="logo wow fadeInLeft">
                            <a href="/">
                                <img src="<?= Yii::$app->view->params['logo']->getImage(); ?>" alt="">
                            </a>
                        </div>
                    </div>
                    <!-- <div class="col-sm-2 ">
                        <div class="loc" id="city">
                            <div class="loc-icon">
                                <img src="/images/loc-icon.png" alt="">
                            </div>
                            <span><?= Yii::$app->session['city_name']; ?> <img src="/images/arrow.png" alt=""></span>
                        </div>
                    </div> -->
                    <div class="col-sm-8 d-flex align-items-center col-md-7 col-xl-8">
                        <div class="menu wow fadeInDown">
                            <ul class="m-0">
                                <? foreach (Yii::$app->view->params['menu'] as $v) : ?>
                                <li><a href="/<?= $v->url; ?>"><?= $v->text; ?></a></li>
                                <? endforeach; ?>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-2 col-md-3 col-xl-2 p-0">
                        <a href="" class="call-center"><i class="fa fa-phone" aria-hidden="true"></i>+7 (747) 094-13-00</a>
                    </div>
                </div>
            </div>
        </header>
    </div>
    <div id="myHeader" class="desctop-header-sticky">
        <div class="container">
            <div class="row wow fadeInUp">
                <div class="col-sm-3 col-md-3 col-lg-2">
                    <div class="catalog">
                        <a href="/catalog/<?= Yii::$app->view->params['catalog'][0]->url; ?>">Каталог</a>
                    </div>
                </div>
                <div class="col-sm-8 col-md-8">
                    <form class="search" method="get" action="/search">
                        <input type="text" id="searchbox" name="text" placeholder="Введите название товара">
                    </form>
                </div>
                <div class="col-sm-1">
                    <div class="user-icon">
                        <? if (!Yii::$app->user->isGuest) : ?>
                        <a href="/account/?tab=home">
                            <img src="/images/user-icon.png" alt="">
                            <span style="font-size: 9px">Личный кабинет</span>
                        </a>
                        <a href="/account/logout" style="margin-left: 20px;"><i class="fa fa-sign-out" aria-hidden="true"></i><span style="font-size: 9px">Выйти</span></a>
                        <? endif; ?>
                        <? if (Yii::$app->user->isGuest) : ?>
                        <a href="/account/sign-in">
                            <img src="/images/user-icon.png" alt="">
                            <span style="font-size: 9px">Личный кабинет</span>
                        </a>
                        <? endif; ?>
                    </div>
                </div>
                <div class="col-sm-1">
                    <div class="basket-icon">
                        <a href="/card/">
                            <img src="/images/basket-icon.png" alt="">
                            <span class="count count-add"><?= Yii::$app->view->params['count']; ?></span>
                            <p style="font-size: 9px; margin-top: 6px;">Корзина</p>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <? else : ?>

    <div class="mobile-version header-sticky">
        <header>
            <div class="col-2" style="display: flex">
                <nav id="main-nav">
                    <ul>
                        <li><a href="#">Каталог</a>
                            <ul>
                                <? $m = 0; ?>
                                <? foreach (Yii::$app->view->params['catalogLevel1'] as $one) : ?>
                                <? $m++; ?>
                                <? if ($m < 7) : ?>
                                <? if ($one->childs != null) : ?>
                                <li><a href="<?= $one->getUrl(); ?>"><span class="menu-icon"><img src="<?= $one->getMobileImage(); ?>" alt=""></span>
                                        <p><?= $one->name; ?></p>
                                    </a>
                                    <ul>
                                        <? foreach ($one->childs as $two) : ?>
                                        <? if ($two->childs != null) : ?>
                                        <li>
                                            <a href="<?= $two->getUrl(); ?>">
                                                <p><?= $two->name; ?></p>
                                            </a>
                                            <ul>
                                                <? foreach ($two->childs as $three) : ?>
                                                <li>
                                                    <a href="<?= $three->getUrl(); ?>"><?= $three->name ?></a>
                                                </li>
                                                <? endforeach; ?>
                                            </ul>
                                        </li>
                                        <? else : ?>
                                        <li><a href="<?= $two->getUrl(); ?>"><?= $two->name; ?></a></li>
                                        <? endif; ?>
                                        <? endforeach; ?>
                                    </ul>
                                </li>
                                <? else : ?>
                                <li><a href="<?= $one->getUrl(); ?>"><?= $one->name; ?></a></li>
                                <? endif; ?>
                                <? endif; ?>
                                <? endforeach; ?>
                            </ul>
                        </li>

                        <? foreach (Yii::$app->view->params['menu'] as $v) : ?>

                        <li><a href="/<?= $v->url; ?>"><i class="<?= $v->icon; ?>"></i><?= $v->text; ?></a></li>
                        //
                        <? if ($v->url == "/news") : ?>
                        <li><a href="/<?= $v->url; ?>"><img src="/images/newspaper.png"><?= $v->text; ?></a></li>
                        <? endif; ?>
                        <? if ($v->url == "address") : ?>
                        <ul>
                            <? foreach (Yii::$app->view->params['address'] as $v) : ?>
                            <li><a href="/site/shops"><?= $v->address; ?></a></li>
                            <? endforeach; ?>
                        </ul>
                        <? endif; ?>
                        <? endforeach; ?>
                    </ul>
                </nav>

            </div>
            <div class="container">
                <div class="row wow fadeInDown p-1">
                    <div class="col-4">
                        <div class="logo">
                            <a href="/">
                                <img src="<?= Yii::$app->view->params['logo']->getImage(); ?>" alt="">
                            </a>
                        </div>
                    </div>
                    <div class="col-4">
                        <!-- <a href="" class="call-center">+7 (747) 094-13-00</a> -->
                    </div>
                    <div class="col-4" style="display: flex; justify-content: flex-end; align-items: center; height: 2rem;">
                        <div class="user-icon">
                            <? if (!Yii::$app->user->isGuest) : ?>
                            <a href="/account/?tab=home">
                                <img src="/images/user-icon.png" alt="">
                            </a>
                            <a href="/account/logout" style="margin-left: 20px;"><i class="fa fa-sign-out" aria-hidden="true"></i></a>
                            <? endif; ?>
                            <? if (Yii::$app->user->isGuest) : ?>
                            <a href="/account/sign-in">
                                <img src="/images/user-icon.png" alt="">
                            </a>
                            <? endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </header>
    </div>
    <div class="col-sm-12 fixed-search">
        <div class="d-flex justify-content-end">
            <form class="search" method="get" action="/search/index">
                <input type="text" id="searchboxMobile" name="text" placeholder="Введите название товара">
            </form>
            <div class="mobile-flex">
                <div class="basket-icon">
                    <a href="/card/">
                        <img src="/images/basket-icon.png" alt="">
                        <span class="count count-add"><?= count($_SESSION['basket']) + count($_SESSION['gift']); ?></span>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <? endif; ?>
