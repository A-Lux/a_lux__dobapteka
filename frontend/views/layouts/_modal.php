<!-- Modal -->
<div class="modal fade pass-reset-modal" id="pass-reset" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Забыли пароль?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <input type="email" class="form-control forget-pass-email" aria-describedby="emailHelp" placeholder="Ваш e-mail">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary forget-password">Отправить</button>
            </div>
        </div>
    </div>
</div>



<!-- Modal -->
<div class="modal fade card-confirm-modal" id="card-confirm" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form class="modal-content">
            <input type="hidden" name="<?= Yii::$app->request->csrfParam ?>" value="<?= Yii::$app->request->getCsrfToken() ?>"/>
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Пожалуйста, заполните следующие поля</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="order-name" class="col-form-label">ФИО:</label>
                    <input id="order-name" type="text" class="form-control"  placeholder="Ваше фио" name="OrderGuestForm[fio]">
                </div>
                <div class="form-group">
                    <label for="order-telephone" class="col-form-label">Номер телефона:</label>
                    <input id="order-telephone" type="text" class="form-control"  placeholder="Ваш номер телефона" name="OrderGuestForm[telephone]">
                </div>
                <div class="form-group">
                    <label for="order-email" class="col-form-label">E-mail:</label>
                    <input id="order-email" type="email" class="form-control"  placeholder="Ваш e-mail" name="OrderGuestForm[email]">
                </div>
            </div>
            <div class="modal-footer pay-btn">
                <button type="button" class="order_guest_confirm">Отправить</button>
            </div>
        </form>
    </div>
</div>

<script>
    $('#order-telephone').inputmask("8(999) 999-9999");
</script>

