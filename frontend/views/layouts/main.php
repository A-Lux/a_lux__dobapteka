<?php

/* @var $this \yii\web\View */
/* @var $content string */

use frontend\assets\AppAsset;
use yii\helpers\Html;
use yii\helpers\Url;

AppAsset::register($this);



?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>
    <title><?= Html::encode($this->title) ?></title>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <?php $this->registerCsrfMetaTags() ?>
    <link rel="shortcut icon" href="/images/logo_n.png" type="image/png">
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>


    <script src="/js/jquery-3.2.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.60/inputmask/jquery.inputmask.js"></script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-166804029-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());
        gtag('config', 'UA-166804029-1');
    </script>


    <?php $this->beginBody() ?>


    <!-- HEADER -->
    <?= $this->render('_modal') ?>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://api-maps.yandex.ru/2.1/?apikey=0b0a8929-e785-47a7-904a-87e8c66e8dcc&lang=ru_RU" type="text/javascript"></script>
    <?= $this->render('_header') ?>


    <?= $content ?>


    <?= $this->render('_footer') ?>



    <?php $this->endBody() ?>
    <script defer>
        if (!document.querySelector(".owl-stage").childElementCount) {
            document.querySelectorAll(".owl-prev, .owl-next").forEach((btn) => {
                btn.style.display = "none";
            });
        }
    </script>

</html>

<?php $this->endPage() ?>