<footer class="footer">
    <div class="container">
        <div class="row wow fadeInUp">
            <div class="mobile-version col-sm-12">
                <div class="mobile-phone">
                    <a href="tel:+7 747 094-13-00"><img src="/images/mobile-phone.png" alt=""></a>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="footer-text">
                    <p><?= Yii::$app->view->params['logo']->copyright; ?></p>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="footer-text">
                    <p>Мы в соцсетях: <a href="<?= Yii::$app->view->params['contact']->facebook; ?>" target="_blank"><i class="fab fa-facebook-f"></i>
                        </a><a href="<?= Yii::$app->view->params['contact']->instagram; ?>" target="_blank"><i class="fab fa-instagram"></i></p></a>
                </div>
            </div>
            <div class="col-sm-3 d-flex flex-column align-items-center">
                <a href="/docs/Публичная оферта_Добрая.pdf" target="_blank" style="color: #fff; font: 14px 'Century Gothic', sans-serif">Публичная оферта</a>
                <a href="/docs/Политика конф-тиДобрая.pdf" target="_blank" style="color: #fff; font: 14px 'Century Gothic', sans-serif">Политика конфиденциальности</a>
            </div>
            <div class="col-sm-3">
                <div class="footer-text">
                    <p><a href="https://www.a-lux.kz/">Разработка сайтов в Алматы</a></p>
                </div>
            </div>
        </div>
    </div>
</footer>