<div class="main">
    <? if (!isMobile()) : ?>
        <div class="desktop-version">
            <div class="main-content">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-md-6 col-sm-12 wow fadeInLeft">
                            <?= $this->render('/partials/catalog'); ?>
                            <div class="main-title gradient-text">
                                <h5>Товар месяца</h5>
                            </div>
                            <? foreach ($top_products as $v) : ?>
                                <div class="month-sale-item">
                                    <a href="">
                                        <div class="top">
                                            <div class="image">
                                                <a href="<?=$v->getUrl();?>">
                                                    <img src="<?=$v->getImage();?>" >
                                                </a>
                                            </div>
                                            <div class="text-block">
                                                <? if ($v->status_products) { ?>
                                                    <div class="red-label">
                                                        дефицит
                                                    </div>
                                                <? } ?>
                                                <div class="name">
                                                    <a href="<?=$v->getUrl();?>" style="text-decoration: none;">
                                                        <p><?= $v->name ?></p>
                                                    </a>
                                                </div>
                                                <div class="price">
                                                    <div class="price">
                                                        <? if ($v->discount) {
                                                            echo '<p>' . $v->price . '</p>';
                                                        } ?>
                                                        <span class="gradient-text"><?= number_format($v->calculatePrice, 0, '', ' '); ?> </span> тг
                                                    </div>
                                                </div>
                                                <div class="status d-block">

                                                    <p class="d-flex"><img src="/images/ok.png" alt="">Есть в наличии</p>

                                                    <? if (Yii::$app->user->isGuest) : ?><a href="#"><i class="fas fa-heart favorite" onclick="locateToSignIn()"></i></a>
                                                    <? else : ?><i class="fas fa-heart like-active favorites <?= $v->getFavoriteStatus() ? 'favorites-active' : ''; ?>" data-id="<?= $v->id; ?>" title="Добавить в избранное"></i>
                                                    <? endif; ?>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="bot">
                                            <div class="sale">
                                                <? if ($v->discount) : ?>
                                                    <p><?= $v->discount ?>%</p>
                                                <? endif; ?>

                                                <? if ($v->bonus && false) : ?>
                                                    <div class="bonus">
                                                        <?= $v->bonus ?> Бонус
                                                    </div>
                                                <? endif; ?>
                                            </div>

                                            <div class="sale-basket-button">
                                                <button class="btn-in-basket" data-id="<?= $v->id; ?>" data-slider-id="relatedProductsForCatalog">
                                                    <img src="/images/light-basket.png" alt="">В корзину</button>
                                            </div>

                                        </div>
                                    </a>
                                </div>
                            <? endforeach; ?>


                        </div>
                        <div class="col-lg-9 col-md-6 col-sm-12 wow fadeInRight">
                            <div class="white-bg wow fadeInRight">
                                <div class="title gradient-text wow fadeInRight">
                                    <h6><?= $catalog->name; ?></h6>
                                </div>
                                <div class="row-list">

                                    <? $childs = $categories;?>

                                    <? if(count($categories) <= 16):?>
                                            <? $col = count($childs) / 2.0;?>
                                            <ul>
                                                <? $m = 0; $n = 0; ?>
                                                <? foreach ($childs as $child):?>

                                                    <? if(count($childs) % 2 == 0):?>
                                                        <? $m++;?>
                                                    <? endif;?>

                                                    <? if($col >= $m && $n < 6):?>
                                                        <? $n++;?>
                                                        <li><a href="<?= $child->getUrl(); ?>"><?=$child->name;?></a></li>
                                                    <? endif;?>

                                                    <? if(count($childs) % 2 != 0):?>
                                                        <? $m++;?>
                                                    <? endif;?>

                                                <? endforeach;?>
                                            </ul>
                                            <ul>
                                                <? $m = 0;$n = 0;?>
                                                <? foreach ($childs as $child):?>

                                                    <? if(count($childs) % 2 == 0):?>
                                                        <? $m++;?>
                                                    <? endif;?>

                                                    <? if($col < $m  && $col*2 >= $m  && $n < 6):?>
                                                        <? $n++;?>
                                                        <li><a href="<?= $child->getUrl(); ?>"><?=$child->name;?></a></li>
                                                    <? endif;?>

                                                    <? if(count($childs) % 2 != 0):?>
                                                        <? $m++;?>
                                                    <? endif;?>

                                                <? endforeach;?>
                                            </ul>
                                        <? else:?>

                                            <? $m = 0;?>
                                            <ul>
                                                <? foreach ($childs as $child):?>
                                                    <? $m++;?>
                                                    <? if($m <= 8):?>
                                                        <li><a href="<?= $child->getUrl(); ?>"><?=$child->name;?></a></li>
                                                    <? endif;?>
                                                <? endforeach;?>
                                            </ul>

                                            <? $m = 0;?>
                                            <ul>
                                                <? foreach ($childs as $child):?>
                                                    <? $m++;?>
                                                    <? if($m > 8 && $m <= 16):?>
                                                        <li><a href="<?= $child->getUrl(); ?>"><?=$child->name;?></a></li>
                                                    <? endif;?>
                                                <? endforeach;?>
                                            </ul>
                                        <? endif?>


                                    <div id="demo" class="collapse">
                                        <div class="row-list">

                                            <? $col = (count($categories)-16) / 2;?>
                                            <? $col2 = (count($categories)-16) % 2;?>

                                            <ul>
                                                <? $c = 0; $m = 0;?>
                                                <? foreach ($childs as $child):?>
                                                    <? $c++;?>
                                                    <? if($c > 16):?>

                                                        <? if($col2 == 0):?>
                                                            <? $m++;?>
                                                        <? endif;?>

                                                        <? if($col >= $m):?>
                                                            <li><a href="<?= $child->getUrl(); ?>"><?=$child->name;?></a></li>
                                                        <? endif;?>

                                                        <? if($col2 != 0):?>
                                                            <? $m++;?>
                                                        <? endif;?>

                                                    <? endif;?>

                                                <? endforeach;?>
                                            </ul>

                                            <ul>
                                                <? $c = 0; $m = 0;?>
                                                <? foreach ($childs as $child):?>

                                                    <? $c++;?>
                                                    <? if($c > 16):?>

                                                        <? if($col2 == 0):?>
                                                            <? $m++;?>
                                                        <? endif;?>

                                                        <? if($col < $m):?>
                                                            <li><a href="<?= $child->getUrl(); ?>"><?=$child->name;?></a></li>
                                                        <? endif;?>

                                                        <? if($col2 != 0):?>
                                                            <? $m++;?>
                                                        <? endif;?>

                                                    <? endif;?>

                                                <? endforeach;?>
                                            </ul>

                                        </div>
                                    </div>

                                    <? if (count($categories) > 16) : ?>
                                        <div class="show-more gradient-text" data-toggle="collapse" data-target="#demo"
                                             id="show-more-catalog" aria-expanded="false">
                                            + Показать все
                                        </div>
                                    <? endif; ?>
                                </div>
                            </div>
                            <div class="tablet-version-hide filter wow fadeInUp">
                                <div class="row">
                                    <div class="col-sm-3 border-right">
                                    </div>
                                    <div class="col-sm-6 border-right">
                                        <div class="filter-form">
                                            <label for="from">Цена от:</label>
                                            <input type="text" id="from" class="from_price">
                                            <label for="to">Цена до:</label>
                                            <input type="text" id="to" class="to_price">
                                            <label for="to">тг</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="checkbox-flex">
                                            <label class="filter-checkbox">В наличии
                                                <input type="checkbox" checked="checked" class="v_nalichii">
                                                <span class="checkmark"></span>
                                            </label>
                                            <label class="filter-checkbox">Акции
                                                <input type="checkbox" class="akcii">
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="tablet-version-hide items">
                                <div class="row" id="loadMoreResults">
                                    <input type="hidden" value="<?= $catalog->id ?>" class="category_id">
                                    <? $m = 0; ?>
                                    <? foreach ($products as $v) { ?>
                                        <? $m++; ?>
                                        <? if ($m > $per_page) break; ?>
                                        <div class="col-sm-12 col-md-12 col-lg-6">
                                            <div class="catalog-item">
                                                <div class="image">
                                                    <a href="<?=$v->getUrl();?>">
                                                        <img src="<?=$v->getImage();?>" >
                                                    </a>
                                                </div>
                                                <div class="text-block">
                                                    <? if ($v->status_products) { ?>
                                                        <div class="red-label">
                                                            дефицит
                                                        </div>
                                                    <? } ?>
                                                    <div class="name">
                                                        <a href="<?=$v->getUrl();?>" style="text-decoration: none;">
                                                            <p><?= $v->name ?></p>
                                                        </a>
                                                    </div>
                                                    <div class="status">

                                                        <p><img src="/images/ok.png" alt="">Есть в наличии</p>

                                                        <? if (Yii::$app->user->isGuest) : ?><a href="#"><i class="fas fa-heart favorite" onclick="locateToSignIn()"></i></a>
                                                        <? else : ?><i class="fas fa-heart like-active favorites <?= $v->getFavoriteStatus() ? 'favorites-active' : ''; ?>" data-id="<?= $v->id; ?>" title="Добавить в избранное"></i>
                                                        <? endif; ?>
                                                    </div>
                                                    <div class="flex-one">
                                                        <div class="price">
                                                            <p class="gradient-text"><?= number_format($v->calculatePrice, 0, '', ' '); ?><span> тг</span></p>
                                                        </div>

                                                        <div class="catalog-basket-button">
                                                            <button class="btn-in-basket" data-id="<?= $v->id; ?>" data-slider-id="relatedProductsForCatalog">
                                                                <img src="/images/light-basket.png" alt="">В корзину</button>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <? } ?>

                                </div>
                                <div class="row" id="loadMoreButton">
                                    <? if (count($products) > $per_page) : ?>
                                        <div class="col-sm-12">
                                            <div class="collapse-wrap">
                                                <div class="load-more" id="load-more">
                                                    <button>Загрузить еще</button>
                                                </div>
                                            </div>
                                        </div>
                                    <? endif; ?>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12 wow fadeInUp" id="relatedProductsForCatalog"></div>
                                    <!--                                    --><? //= $this->render("_bonus_web"); 
                                                                                ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <? else : ?>

        <div class="mobile-version">
            <div class="main-content">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 wow fadeInUp">
                            <div class="owl1 owl-carousel owl-theme" style='opacity: 0; transtion: all .3s ease;'>
                                <? foreach ($categories as $v) : ?>
                                    <div class="item" ">
                                        <div class=" green">
                                        <a href="<?= $v->getUrl(); ?>"><?= $v->name; ?></a>
                                    </div>
                            </div>
                        <? endforeach; ?>
                        </div>
                    </div>
                    <div class="col-sm-12 wow fadeInUp">
                        <div class="filter">
                            <div class="row">
                                <div class="col-sm-3 border-top">
                                    <div class="filter-form">
                                        <label for="fromMob">Цена от:</label>
                                        <input type="text" id="fromMob" class="from_price_mob">
                                        <label for="toMob">Цена до:</label>
                                        <input type="text" id="toMob" class="to_price_mob">
                                        <label for="toMob">тг</label>
                                    </div>

                                </div>
                                <div class="col-sm-3 border-top">
                                    <div class="checkbox-flex">
                                        <label class="filter-checkbox">В наличии
                                            <input type="checkbox" checked="checked" class="v_nalichiiMob">
                                            <span class="checkmark"></span>
                                        </label>
                                        <label class="filter-checkbox">Акции
                                            <input type="checkbox" class="akciiMob">
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-content">
                            <div class="tab-item tab-item-active" id="home">
                                <input type="hidden" value="<?= $catalog->id ?>" class="category_id">
                                <div class="row" id="loadMoreResultsMob">
                                    <? $m = 0; ?>
                                    <? foreach ($products as $v) : ?>
                                        <? $m++; ?>
                                        <? if ($m > $per_page) break; ?>


                                        <div class="col-sm-12 col-md-12 col-lg-6">
                                            <div class="catalog-item">
                                                <div class="image">
                                                    <a href="<?=$v->getUrl();?>">
                                                        <img src="<?=$v->getImage();?>" >
                                                    </a>
                                                </div>
                                                <div class="text-block">
                                                    <? if ($v->status_products) { ?>
                                                        <div class="red-label">
                                                            дефицит
                                                        </div>
                                                    <? } ?>
                                                    <div class="name">
                                                        <a href="<?=$v->getUrl();?>" style="text-decoration: none;">
                                                            <p><?= $v->name ?></p>
                                                        </a>
                                                    </div>
                                                    <div class="status">

                                                        <p><img src="/images/ok.png" alt="">Есть в наличии</p>

                                                        <? if (Yii::$app->user->isGuest) : ?><a href="#"><i class="fas fa-heart favorite" onclick="locateToSignIn()"></i></a>
                                                        <? else : ?><i class="fas fa-heart like-active favorites <?= $v->getFavoriteStatus() ? 'favorites-active' : ''; ?>" data-id="<?= $v->id; ?>" title="Добавить в избранное"></i>
                                                        <? endif; ?>
                                                    </div>
                                                    <div class="flex-one">
                                                        <div class="price">
                                                            <p class="gradient-text"><?= number_format($v->calculatePrice, 0, '', ' '); ?><span> тг</span></p>
                                                        </div>

                                                        <div class="catalog-basket-button">
                                                            <button class="btn-in-basket" data-id="<?= $v->id; ?>" data-slider-id="relatedProductsForCatalogMob">
                                                                <img src="/images/light-basket.png" alt="">В корзину</button>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <? endforeach; ?>
                                </div>

                                <div class="row" id="loadMoreButtonMob">
                                    <? if (count($products) > $per_page) : ?>
                                        <div class="col-sm-12">
                                            <div class="collapse-wrap">
                                                <div class="load-more" id="load-more-mob">
                                                    <button>Загрузить еще</button>
                                                </div>
                                            </div>
                                        </div>
                                    <? endif; ?>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12 wow fadeInUp" id="relatedProductsForCatalogMob"></div>
                                    <!--                                    --><? //= $this->render("_bonus_mobile"); 
                                                                                ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <? endif; ?>
</div>
</div>
