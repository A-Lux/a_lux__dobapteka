<? $m = 0; ?>
<? foreach ($products as $v) : ?>
    <? $m++; ?>
    <? if ($m < 11) : ?>
    <div class="col-sm-12 col-md-12 col-lg-6">
        <div class="catalog-item">
            <div class="image">
                <a href="<?=$v->getUrl();?>">
                    <img src="<?=$v->getImage();?>" >
                </a>
            </div>
            <div class="text-block">
                <? if ($v->status_products) { ?>
                    <div class="red-label">
                        дефицит
                    </div>
                <? } ?>
                <div class="name">
                    <a href="<?=$v->getUrl();?>" style="text-decoration: none;">
                        <p><?= $v->name ?></p>
                    </a>
                </div>
                <div class="status">

                    <p><img src="/images/ok.png" alt="">Есть в наличии</p>

                    <? if (Yii::$app->user->isGuest) : ?><a href="#"><i class="fas fa-heart favorite" onclick="locateToSignIn()"></i></a>
                    <? else : ?><i class="fas fa-heart like-active favorites <?= $v->getFavoriteStatus() ? 'favorites-active' : ''; ?>" data-id="<?= $v->id; ?>" title="Добавить в избранное"></i>
                    <? endif; ?>
                </div>
                <div class="flex-one">
                    <div class="price">
                        <p class="gradient-text"><?= number_format($v->calculatePrice, 0, '', ' '); ?><span> тг</span></p>
                    </div>

                    <div class="catalog-basket-button">
                        <button class="btn-in-basket" data-id="<?= $v->id; ?>" data-slider-id="relatedProductsForCatalog">
                            <img src="/images/light-basket.png" alt="">В корзину</button>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <? endif;?>
<? endforeach; ?>
