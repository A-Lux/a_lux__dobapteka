<div class="main">
    <div class="desktop-version">
        <div class="main-content">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 wow slideInLeft">
                        <div class="main-title gradient-text">
                            <h5><?= $Product->name ?></h5>
                        </div>
                    </div>
                    <div class="col-sm-5 wow fadeInLeft">
                        <div class="product-img">
                            <img src="<?=$Product->getImage();?>" class="img-fluid" alt="">
                        </div>
                    </div>
                    <div class="col-sm-7 wow fadeInRight">
                        <div class="product-description">
                            <ul>
                                <li>
                                    <? if($Product->checkRemainder()):?>
                                        <p>Наличие</p><span class="gradient-text">Есть в наличии</span>
                                    <? else:?>
                                        <p>Наличие</p><span  style="color: #df2a2a;">Нет в наличии</span>
                                    <? endif;?>
                                </li>
                                <li>
                                    <p>Цена</p> <span class="gradient-text" style="padding-right: 5px;"><?= number_format($Product->getCalculatePrice(), 0, '', ' '); ?></span> <span> тг</span>
                                </li>
                                <li>
                                    <p>Производитель</p><span>
                                        <? if ($Product->this_country != null) : ?>
                                            <?= $Product->this_country->name ?>
                                        <? endif; ?>
                                    </span>
                                </li>
                                <li>
                                    <p>Описание</p><span><?= $Product->description ?></span>
                                </li>
                            </ul>
                        </div>
                        <div class="button-wrap">
                            <div class="save-btn">
                                <button class="btn-add-favorite" data-id="<?= $Product->id; ?>" data-user-id="<?= Yii::$app->user->id ?>">В избранное</button>
                            </div>
                            <? if($Product->checkRemainder()):?>
                                <div class="basket-button">
                                    <button class="btn-in-basket" data-id="<?= $Product->id; ?>"><img src="/images/light-basket.png" alt="">В корзину</button>
                                </div>
                            <? endif; ?>

                        </div>


                    </div>
                    <div class="col-lg-9 col-sm-12 wow fadeInUp">
                        <div class="instruction">
                            <p data-toggle="collapse" class="collapsed" data-target="#demo"><img src="/images/collapse-arrow.png" alt="">Инструкция по применению</p>
                        </div>
                        <div id="demo" class="collapse">
                            <div class="collapse-text">
                                <?= $Product->text ?>
                            </div>
                        </div>


                        <div class="instruction">
                            <p data-toggle="collapse" class="collapsed" data-target="#ostatki"><img src="/images/collapse-arrow.png" alt="">Наличие в аптеках</p>
                        </div>
                        <div id="ostatki" class="collapse">
                            <div class="collapse-text">
                                <div class="box" id='result'>
                                    <? if ($remainder != null) : ?>
                                        <!-- /.box-header -->
                                        <div class="box-body">
                                            <table id="example1" class="table table-bordered table-striped">
                                                <thead>
                                                <tr>
                                                    <th>№</th>
                                                    <th>Аптека</th>
                                                    <th>Количество</th>
                                                </tr>
                                                </thead>
                                                <tbody id="nearlyPharmaciesData">
                                                <? foreach ($remainder as $v) : ?>
                                                    <tr>
                                                        <td><?= $v->filialNumber; ?></td>
                                                        <td><?= $v->filialName; ?></td>
                                                        <td>
                                                            <? if($v->amount > 0):?>
                                                                <?= $v->amount; ?>
                                                            <? else:?>
                                                                Нет в наличии
                                                            <? endif;?>
                                                        </td>
                                                    </tr>
                                                <? endforeach; ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    <? else : ?>
                                        <div class="box-body">
                                            Нет
                                        </div>
                                    <? endif; ?>

                                </div>
                            </div>
                        </div>

                    </div>

                    <? if ($sop_tovary != null) : ?>
                        <div class="col-sm-12 wow fadeInUp">
                            <div class="main-title gradient-text border-top">
                                <h5>Сопутствующие товары</h5>
                            </div>
                        </div>
                        <?= $this->render('../partials/sop_tovary', compact('sop_tovary')) ?>
                    <? endif; ?>
                    <? if ($analogy != null) : ?>
                        <div class="col-sm-12 wow fadeInUp">
                            <div class="product-title gradient-text border-top">
                                <h5>Аналоги лекарственных средств</h5>
                            </div>
                        </div>
                        <?= $this->render('../partials/analogy', compact('analogy')) ?>
                    <? endif; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="mobile-version">
        <div class="main-content">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 wow fadeInUp mb-5">
                        <div class="product-title gradient-text">
                            <h5><?= $Product->name ?></h5>
                        </div>
                        <div class="product-img">
                            <img src="<?=$Product->getImage();?>" class="img-fluid" alt="">
                        </div>
                        <div class="product-description">
                            <ul>
                                <li>
                                    <p>Наличие</p><span class="gradient-text">Есть в наличии</span>
                                </li>
                                <li>
                                    <p>Цена</p> <span class="gradient-text" style="padding-right: 5px;"><?= $Product->getCalculatePrice() ?></span> <span> тг</span>
                                </li>
                                <li>
                                    <p>Страна</p><span>
                                        <? if ($Product->this_country != null) : ?>
                                            <?= $Product->this_country->name ?>
                                        <? endif; ?>
                                    </span>
                                </li>
                                <li>
                                    <p>Описание</p><span><?= $Product->description ?></span>
                                </li>
                            </ul>
                        </div>
                        <div class="button-wrap">
                            <div class="save-btn">
                                <button class="btn-add-favorite" data-id="<?= $Product->id; ?>" data-user-id="<?= Yii::$app->user->id ?>">В избранное</button>
                            </div>

                            <div class="basket-button">
                                <button class="btn-in-basket" data-id="<?= $Product->id; ?>"><img src="/images/light-basket.png" alt="">В корзину</button>
                            </div>
                        </div>
                        <div class="col-lg-9 col-sm-12">
                            <div class="instruction">
                                <p data-toggle="collapse" class="collapsed" data-target="#demo"><img src="/images/collapse-arrow.png" alt="">Инструкция по применению</p>
                            </div>
                            <div id="demo" class="collapse instruction-collapse">
                                <div class="collapse-text">
                                    <?= nl2br($Product->text) ?>
                                </div>
                            </div>
                            <div class="instruction">
                                <p data-toggle="collapse" class="collapsed" data-target="#ostatki"><img src="/images/collapse-arrow.png" alt="">Наличие в аптеках</p>
                            </div>
                            <div id="ostatki" class="collapse">
                                <div class="collapse-text">
                                    <div class="box" id='result'>
                                        <? if ($remainder != null) : ?>
                                            <!-- /.box-header -->
                                            <div class="box-body">
                                                <table id="example1" class="table table-bordered table-striped">
                                                    <thead>
                                                        <tr>
                                                            <th>№</th>
                                                            <th>Аптека</th>
                                                            <th>Количество</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="nearlyPharmaciesData">
                                                        <? foreach ($remainder as $v) : ?>
                                                            <tr>
                                                                <td><?= $v->filialNumber; ?></td>
                                                                <td><?= $v->filialName; ?></td>
                                                                <td><?= $v->amount; ?></td>
                                                            </tr>
                                                        <? endforeach; ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        <? else : ?>
                                            <div class="box-body">
                                                Нет
                                            </div>
                                        <? endif; ?>

                                    </div>
                                </div>
                            </div>

                        </div>


                    </div>
                    <? if ($sop_tovary != null) : ?>
                        <div class="col-sm-12 wow fadeInUp">
                            <div class="product-title gradient-text border-top d-flex align-items-center justify-conten-between">
                                <h5>Сопутствующие товары</h5>
                                <img class="owl-arrow-right" src="/backend/web/images/catalogproducts/arrow-right.png" alt="">
                            </div>
                            <?= $this->render('../partials/sop_tovary', compact('sop_tovary')) ?>
                        </div>
                    <? endif; ?>

                    <? if ($analogy != null) : ?>
                        <div class="col-sm-12 wow fadeInUp">
                            <div class="product-title gradient-text border-top d-flex align-items-center justify-conten-between">
                                <h5>Аналоги лекарственных средств</h5>
                                <img class="owl-arrow-right2" src="/backend/web/images/catalogproducts/arrow-right.png" alt="">
                            </div>
                            <?= $this->render('../partials/analogy', compact('analogy')) ?>
                        </div>
                    <? endif; ?>
                </div>
            </div>
        </div>
    </div>

</div>
</div>
