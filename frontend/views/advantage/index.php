<?php

/**
 * Created by PhpStorm.
 * User: admin
 * Date: 10.12.2019
 * Time: 13:04
 */

?>

<div class="inner-page">
    <div class="container py-5 px-5 px-sm-0">
        <?= $model->content; ?>
    </div>
</div>