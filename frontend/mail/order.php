<div style="position:relative;background-color:#F8F8F8;padding:3rem .5rem;">
  <img src="/images/border-bottom.png" alt="" style="position:absolute;top:0;">
  <img src="/images/border-top.png" alt="" style="position:absolute;bottom:0;">
  <div class="logo-mail" style="text-align: center">
    <a href="/" style="width:50%;">
      <img src="<?= Yii::$app->view->params['logo']->getImage(); ?>" alt="">
    </a>
  </div>
  <!--  -->

  <div style="text-align: center;">
    <p><b><?= $fio; ?>, благодарим Вас за оформление заказа <br> на нашем сайте</b> <b><a href="<?= $host; ?>" style="color:green;">dobraya-apteka.kz</a></b></p>
    <P>Ниже, предоставляем Вам информацию о заказах: </P>
  </div>
  <br>
  <table class="table table-striped table-bordered table-mail" style="width:100%;font-size:14px;border-bottom:1px dashed silver;">
    <thead>
      <tr style="text-align: left;background: -moz-linear-gradient( 90deg, rgb(108,196,21) 0%, rgb(182,214,3) 100%);
  background: -webkit-linear-gradient( 90deg, rgb(108,196,21) 0%, rgb(182,214,3) 100%);
  background: -ms-linear-gradient( 90deg, rgb(108,196,21) 0%, rgb(182,214,3) 100%);color:#fff;">
        <th style="padding: 1rem 1rem;" scope="col"><b>Наименование</b></th>
        <th style="padding: 1rem 1rem;text-align:center;" scope="col"><b>Цена за ед.</b></th>
        <th style="padding: 1rem 1rem;text-align:center;" scope="col"><b>Кол-во</b></th>
        <th style="padding: 1rem 1rem;text-align:center;" scope="col"><b>Сумма</b></th>
      </tr>
    </thead>
    <div>
      <tbody style="overflow-x: auto">
        <? $m = 0; ?>
        <? foreach ($model as $k => $v) : ?>
          <? $m++; ?>
          <tr style="<?= $m % 2 == 1 ? "text-align: left;background-color:#fff;" : "text-align: left"; ?>">
            <th style="padding: 1rem 1rem;" scope="row"><?= $v->product->name; ?> </th>
            <td style="padding: 1rem 1rem;text-align:center;"><?= number_format($v->product->price, 0, '', ' '); ?> тг</td>
            <td style="padding: 1rem 1rem;text-align:center;"><?= $v->count; ?></td>
            <td style="padding: 1rem 1rem;text-align:center;"><?= $v->product->calculatePrice * $v->count; ?> тг</td>
          </tr>
        <? endforeach; ?>

        <tr class="table-price">
          <? if ($deliveryMethod == 1) : ?>
            <td style="padding: 1rem 1rem;text-align-left;" scope="row">Доставка: <b><?= $deliveryPrice; ?> тг</b></th>
            <? else : ?>
            <td></td>
          <? endif; ?>
          <td></td>
          <td></td>
          <td style="padding: 1rem 1rem;text-align-right;">Итого товаров на сумму: <b> <?=$sum;?> тг</b></td>
        </tr>
      </tbody>
    </div>
    <style>
    </style>
  </table>
  <br>
  <div style="width: 40%;margin:0 auto;text-align:center;">
    <p>С уважением, <br> Интернет-аптека <a href="<?= $host; ?>" style="color:green;"><b>"Добрая"</b></a></p>
  </div>
</div>

<style>
  @media only screen and (max-width:576px) {
    .table-price {
      display: block !important;
    }

    .table-price td {
      text-align: left !important;
      display: block !important;
    }

    .table-price tr {
      font-size: 11px !important;
    }
  }
</style>
