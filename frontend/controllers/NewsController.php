<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 19.05.2020
 * Time: 15:46
 */

namespace frontend\controllers;


use common\models\News;
use yii\web\NotFoundHttpException;

class NewsController extends FrontendController
{

    public function actionIndex(){

        $model = News::getAll();
        return $this->render('index', compact('model'));
    }


    public function actionIndex2($id){

        $model = News::findOne($id);
        if($model){
            return $this->render('inner', compact('model'));
        }else{
            throw new NotFoundHttpException();
        }

    }
}
