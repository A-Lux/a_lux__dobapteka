<?php

/**
 * Created by PhpStorm.
 * User: admin
 * Date: 08.08.2019
 * Time: 16:19
 */

namespace frontend\controllers;


use common\models\Products;
use yii\data\Pagination;
use frontend\models\ProductSearch;

class SearchController extends FrontendController
{
    function wordcombos ($words) {
        if ( count($words) <= 1 ) { 
            $result=$words; 
        } 
        else { 
            $result=array(); 
            for ( $i=0; $i < count($words); ++$i ) {
                $firstword=$words[$i]; $remainingwords=array(); 
                for ( $j=0; $j < count($words); ++$j ) { 
                    if ( $i <> $j )
                        $remainingwords[] = $words[$j];
                }
                $combos = $this->wordcombos($remainingwords);
                for ( $j = 0; $j < count($combos); ++$j ) { 
                    $result[]=$firstword . '%' . $combos[$j]; 
                } 
            } 
        } 
        
        return $result; 
    }


    public function actionIndex($text)
    {
        // if (apc_exists($text . 'newSearch')) {
        //     return apc_fetch($text . 'newSearch');
        // }
        
        $final = 'name like \'%';
        if(count(explode(' ', $text)) <= 3){
            foreach ($this->wordcombos(explode(' ', $text)) as $words) {
                $final = $final . $words .'%\' or name like \'%';
            }
            $final = preg_replace('/ or name like \'%$/', '', $final);
        }
        else{
            $final = $final . str_replace(' ', '%', $text) . '%\'';
        }
        
        $keyword = $text;
        // str_replace(' ', '%', $text);

        $count = ProductSearch::find()
            // ->andFilterWhere(['like', 'name', $keyword])
            ->where($final)
            ->count();
        $pagination = new Pagination(['totalCount' => $count, 'pageSize' => 10]);
        $product = ProductSearch::find()
            ->offset($pagination->offset)
            ->limit($pagination->limit)
            ->where($final)
            ->all();

        $keyword = str_replace('%', ' ', $text);

        // apc_store($text . 'newSearch', $product, 1800);

        return $this->render('index', compact('keyword', 'count', 'product', 'pagination'));

        // $product = [];

        // if($keyword){

        //     // -- get similar
        //     $product = [];
        //     $array_isset = [];

        //     $query = "name LIKE '%$keyword%'";

        //     for($i=2;$i<strlen($keyword);$i+=2){
        //         $val = substr($keyword,0,$i).'-'.substr($keyword,$i,strlen($keyword));
        //         $query .= " OR name LIKE '%$val%'";
        //     }

        //     $ru = ['а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'э', 'ю', 'я'];
        //     for ($i = 0; $i < strlen($keyword); $i += 2) {
        //         foreach ($ru as $v){
        //             $val = substr($keyword,0,$i).''.$v.''.substr($keyword,$i+2,strlen($keyword));
        //             $query .= "OR name LIKE '%$val%'";
        //         }

        //     }

        //     $products = Products::searchByName($keyword);

        //     foreach ($products as $v) {
        //         if (!in_array($v->id, $array_isset)) {
        //             $array_isset[] = $v->id;
        //         }

        //     }

        //     $products = Products::getByQuery($query);

        //     foreach ($products as $v) {
        //         if (!in_array($v->id, $array_isset)) {
        //             $array_isset[] = $v->id;
        //         }
        //     }





        //     if(count($array_isset) > 0){


        // $query = Products::find()
        // $count = $query->count();
        //         $pagination = new Pagination(['totalCount' => $count, 'pageSize' => 10]);
        //         $product = $query->offset($pagination->offset)
        //             ->limit($pagination->limit)
        //             ->all();

        //     return $this->render('index', compact('product','count', 'keyword', 'pagination'));
        // }



        // -- end get similar

        // }
        // $query = Products::find()

    }
}