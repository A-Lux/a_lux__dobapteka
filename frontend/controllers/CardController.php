<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 29.04.2019
 * Time: 11:32
 */

namespace frontend\controllers;

use app\models\OrderGuestForm;
use common\models\DiscountEarliestPersons;
use common\models\Filial;
use common\models\FilialProduct;
use common\models\Geocoords;
use common\models\Orders;
use common\models\PaymentToken;
use common\models\PriceFreeDelivery;
use common\models\Products;
use common\models\PromoCode;
use common\models\PromoUsers;
use common\models\Text;
use common\models\User;
use common\models\UserAddedProduct;
use common\models\UserAddress;
use common\models\UserBalance;
use common\models\UserGift;

use Yii;
use yii\web\NotFoundHttpException;

class CardController extends FrontendController
{


    public function actionIndex(){

        $_SESSION['deliveryPrice'] = 0;
        $_SESSION['promo'] = 0;
        $_SESSION['earliest'] = 0;
        $_SESSION['notification_status'] = true;

        $bonus = null;
        if(!Yii::$app->user->isGuest) { 
            $bonus = UserBalance::findOne(['user_id'=>Yii::$app->user->id]);
            $_SESSION['earliest'] = DiscountEarliestPersons::getContent()->discountPercent;
        }else{
            $order = new Orders();
            Yii::$app->view->params['order'] = $order;
        }

        unset($_SESSION['promo_id']);
        unset($_SESSION['bonus']);
        unset($_SESSION['selectedAddress']);
        unset($_SESSION['guest_data']);
        unset($_SESSION['deliveryPrice']);

        $sum = Products::getSum();
        $model = Geocoords::getAll();
        $address = Filial::findAll(['city_id'=>Yii::$app->session->get('city_id')]);
        $text = Text::findAll(['type_id' => 2]);

        if(!Yii::$app->user->isGuest){
            $user_addresses = UserAddress::getUserAddresses();
        }else{
            $user_addresses = null;
        }

        return $this->render('basket',compact('sum','model','address','bonus','text', 'user_addresses'));
    }


    public function actionGift()
    {
        if (count($_SESSION['gift_for_type']) != null){
            if ($_SESSION['gift_for_type'] == 1) {
                return $this->render('gift_for_price');
            } elseif ($_SESSION['gift_for_type'] == 0) {
                return $this->render('gift_for_product');
            } elseif ($_SESSION['gift_for_type'] == 2) {
                return $this->render('gift_for_price');
            } else {
                return $this->goHome();
            }
        }else{
            throw new NotFoundHttpException();
        }
    }



    public function actionDeleteProduct($id)
    {
        if(!Yii::$app->user->isGuest) UserAddedProduct::deleteAll(['user_id'=>Yii::$app->user->id, 'product_id'=>$id]);
        $m=0;
        foreach ($_SESSION['basket'] as $v){
            if($v->id == $id){
                unset($_SESSION['basket'][$m]);
                $_SESSION['basket'] = array_values($_SESSION['basket']);
                break;
            }
            $m++;
        }
        return $this->redirect(['index']);
    }


    public function actionDeleteGift($id)
    {
        $m=0;
        foreach ($_SESSION['gift'] as $v){
            if($v->id == $id){
                unset($_SESSION['gift'][$m]);
                $_SESSION['gift'] = array_values($_SESSION['gift']);
                break;
            }
            $m++;
        }
        return $this->redirect(['index']);
    }


    public function actionAddProduct()
    {
        if (Yii::$app->request->isAjax) {

            $id = $_GET['id'];
            $status = 0;

            if (!Yii::$app->user->isGuest) {
                UserAddedProduct::addProduct(Yii::$app->user->id, $id);
            }

            $basket = Products::findOne($id);
            $basket->count = 1;
            $check = true;

            $sum = FilialProduct::find()
                ->distinct()
                ->innerJoin('filial', '`filial`.`id` = `filial_product`.`filial_id`')
                ->where('filial.status = 2')
                ->andWhere(['filial_product.product_id' => $id])
                ->sum('amount');

            foreach ($_SESSION['basket'] as $k => $v) {
                if ($v->id == $id) {
                    $check = false;

                    if($sum >= $_SESSION['basket'][$k]['count']+1) {
                        $status = 1;
                        $_SESSION['basket'][$k]['count'] += 1;
                    }

                    break;
                }
            }

            if ($check) {
                if($sum > 0){
                    $status = 1;
                    array_push($_SESSION['basket'], $basket);
                }

            }

            $count = $this->getCount();
            $array = ['status' => $status, 'count' => $count];
            return json_encode($array);
        }else{
            throw new NotFoundHttpException();
        }
    }



    public function actionAddGift()
    {
        if (Yii::$app->request->isAjax) {
            $basket = UserGift::findAll(['user_id' => Yii::$app->user->id]);
            foreach ($basket as $val) {
                $check = true;
                $product = $val->product;
                foreach ($_SESSION['gift'] as $v) {
                    if ($product->id == $v->id) {
                        $check = false;
                        break;
                    }
                }
                if ($check) {
                    $product->count = $val->quantity;
                    $product->price = 0;
                    array_push($_SESSION['gift'], $product);
                }
            }

            $count = $this->getCount();
            $array = ['status' => 1, 'count' => $count];

            return json_encode($array);
        }else{
            throw new NotFoundHttpException();
        }
    }


    public function actionDownClicked()
    {
        if (Yii::$app->request->isAjax) {
            $count = 1;
            foreach ($_SESSION['basket'] as $k => $v) {
                if ($v->id == $_GET['id'] && $_SESSION['basket'][$k]->count > 1) {
                    $_SESSION['basket'][$k]['count'] -= 1;
                    $count = $_SESSION['basket'][$k]['count'];
                    break;
                }
            }

            $deliveryPrice = $this->getDeliveryPrice();
            $sum = $this->getSumBasket($deliveryPrice);
            $sumProduct = Products::getSumProduct($_GET['id']);
            $array = ['countProduct' => $count, 'sum' => intval($sum), 'sumProduct' => $sumProduct, 'deliveryPrice' => $deliveryPrice];
            return json_encode($array);
        }else{
            throw new NotFoundHttpException();
        }
    }


    public function actionUpClicked()
    {
        if (Yii::$app->request->isAjax) {

            $status = false;
            $count = 1;
            foreach ($_SESSION['basket'] as $k => $v) {
                if ($v->id == $_GET['id']) {
                    $sum = FilialProduct::find()
                        ->distinct()
                        ->innerJoin('filial', '`filial`.`id` = `filial_product`.`filial_id`')
                        ->where('filial.status = 2')
                        ->andWhere(['filial_product.product_id' => $_GET['id']])
                        ->sum('amount');
                    if($sum >= $_SESSION['basket'][$k]['count']+1){
                        $_SESSION['basket'][$k]['count'] += 1;
                        $status = true;
                    }

                    $count = $_SESSION['basket'][$k]['count'];
                    break;
                }
            }

            $deliveryPrice = $this->getDeliveryPrice();
            $sum = $this->getSumBasket($deliveryPrice);
            $sumProduct = Products::getSumProduct($_GET['id']);


            if($status){
                $array = [
                    'status' => 1,
                    'countProduct' => $count,
                    'sum' => intval($sum),
                    'sumProduct' => $sumProduct,
                    'deliveryPrice' => $deliveryPrice
                ];
            }else{
                $array = [
                    'status' => 0,
                    'countProduct' => $count,
                ];
            }

            return json_encode($array);
        }else{
            throw new NotFoundHttpException();
        }
    }

    public function actionCountChanged()
    {
        if (Yii::$app->request->isAjax) {

            $status = false;
            foreach ($_SESSION['basket'] as $k => $v) {
                if ($v->id == $_GET['id']) {
                    if ($_GET['v'] > 0) {
                        $sum = FilialProduct::find()
                            ->distinct()
                            ->innerJoin('filial', '`filial`.`id` = `filial_product`.`filial_id`')
                            ->where('filial.status = 2')
                            ->andWhere(['filial_product.product_id' => $_GET['id']])
                            ->sum('amount');
                        if($sum >= $_GET['v']) {
                            $_SESSION['basket'][$k]['count'] = $_GET['v'];
                            $status = true;
                        }
                    } else {
                        $_SESSION['basket'][$k]['count'] = 1;
                        $status = true;
                    }

                    $count = $_SESSION['basket'][$k]['count'];
                    break;
                }
            }

            $deliveryPrice = $this->getDeliveryPrice();
            $sum = $this->getSumBasket($deliveryPrice);
            $sumProduct = Products::getSumProduct($_GET['id']);

            if($status){
                $array = [
                    'status' => 1,
                    'countProduct' => $count,
                    'sum' => intval($sum),
                    'sumProduct' => $sumProduct,
                    'deliveryPrice' => $deliveryPrice
                ];
            }else{
                $array = [
                    'status' => 0,
                    'countProduct' => $count,
                ];
            }

            return json_encode($array);
        }else{
            throw new NotFoundHttpException();
        }
    }


    public function actionOrder()
    {
        if($_SESSION['basket'] != null && Yii::$app->request->isAjax) {

            if($_GET['deliveryMethod'] == 1 || $_GET['deliveryMethod'] == 2) {

                $order = new Orders();

                if (!Yii::$app->user->isGuest) {
                    $used_promo = Orders::getUsedPromo();
                    $used_bonus = Orders::getUsedBonus();
                } else {
                    $used_promo = 0;
                    $used_bonus = 0;
                }

                if (Yii::$app->user->isGuest && !isset($_SESSION['guest_data'])) {
                    return Orders::clientIsGuest;
                }



                $deliveryMethod = $_GET['deliveryMethod'];
                $paymentMethod = $_GET['paymentMethod'];

                if ($deliveryMethod == 1) {

                    if(isset($_SESSION['deliveryPrice'])){

                        date_default_timezone_set('Asia/Almaty');
                        if(date('H') >= 21 && $_SESSION['notification_status']){
                            $_SESSION['notification_status'] = false;
                            return Orders::notificationStatus;
                        }

                        $deliveryPrice = $this->getDeliveryPrice();
                        $sum = $this->getSumBasket($deliveryPrice);

                        $deliveryPrice = $this->getDeliveryPrice();
                        $delivery_id = Orders::IntegrationWithRelogSystem(intval($sum), $used_bonus, $used_promo, $deliveryPrice, $_GET['address']);

                        return $order->saveOrderWithDelivery($delivery_id, intval($sum), $used_bonus, $used_promo, $_GET['change'], $_GET['address'], $deliveryPrice, $paymentMethod);

                    }else{
                        return 'Необходимо выбрать место доставки!';
                    }

                } elseif ($deliveryMethod == 2) {

                    return 'Неправильно выбран способ доставки!';

//                    if ((isset($_SESSION['selectedAddress']) && $_SESSION['selectedAddress'] == null) ||
//                        !isset($_SESSION['selectedAddress'])) {
//                        return 'Выберите пункт который вы будете забрать заказ';
//                    }else {
//                        $deliveryPrice = 0;
//                        $sum = $this->getSumBasket($deliveryPrice);
//                        return $order->saveOrder(intval($sum), $used_bonus, $used_promo, $_GET['change'], $paymentMethod);
//                    }
                }
            }else {
                return 'Неправильно выбран способ доставки!';
            }

        }else{
            throw new NotFoundHttpException();
        }

    }





    public function actionConfirmOrderedGuestData(){

        if (Yii::$app->request->isAjax) {
            $model = new OrderGuestForm();
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $phoneStatus = true;
                for ($i = 0; $i < strlen($model->telephone); $i++) {
                    if ($model->telephone[$i] == '_') {
                        $phoneStatus = false;
                    }
                }
                if ($phoneStatus) {
                    $_SESSION['guest_data'] = $model;
                    return 1;
                } else {
                    return "Необходимо заполнить «Телефон».";
                }

            } else {
                $error = "";
                $errors = $model->getErrors();
                foreach ($errors as $v) {
                    $error .= $v[0];
                    break;
                }
                return $error;
            }
        }else{
            throw new NotFoundHttpException();
        }
    }




    public function actionCheckPromo(){
        if (Yii::$app->request->isAjax) {
            date_default_timezone_set('Asia/Almaty');
            $check = PromoCode::find()->where('quantity>0 AND code="' . $_GET['code'] . '"')->one();
            if ($check) {
                $checkUsedPromo = PromoUsers::find()->where('user_id=' . Yii::$app->user->id . ' AND promo_id=' . $check->id)->one();
                if ($checkUsedPromo) {
                    $response['status'] = 0;
                    $response['error'] = "Промокод уже использован.";
                    return json_encode($response);
                } else {
                    $start_time = strtotime($check->start_date);
                    $end_time = strtotime($check->end_date);
                    $time_now = strtotime(date('Y-m-d H:i:s'));
                    if ($start_time < $time_now && $end_time > $time_now) {
                        $_SESSION['promo'] = $check->percent;
                        $_SESSION['promo_id'] = $check->id;
                        $deliveryPrice = $this->getDeliveryPrice();
                        $sum = $this->getSumBasket($deliveryPrice);
                        $response = ['status' => 1, 'percent' => $check->percent, 'sum' => intval($sum), 'deliveryPrice' => $deliveryPrice];
                        return json_encode($response);
                    } else {
                        $response['status'] = 0;
                        $response['error'] = "Промокод не найдено.";
                        return json_encode($response);
                    }
                }

            } else {
                $response['status'] = 0;
                $response['error'] = "Промокод не найдено.";
                return json_encode($response);
            }
        }else{
            throw new NotFoundHttpException();
        }

    }

    public function actionUpdateSession(){

        if (Yii::$app->request->isAjax) {
            $_SESSION['deliveryPrice'] = $_GET['deliveryPrice'];
            $_SESSION['deliveryTime'] = $_GET['deliveryTime'];
            $_SESSION['latitude'] = $_GET['latitude'];
            $_SESSION['longitude'] = $_GET['longitude'];
            $deliveryPrice = $this->getDeliveryPrice();
            $sum = $this->getSumBasket($deliveryPrice);
            $response = ['status' => 1, 'sum' => intval($sum), 'deliveryPrice' => $deliveryPrice];
            return json_encode($response);
        }else{
            throw new NotFoundHttpException();
        }
    }

    public function actionBonus(){

        if (Yii::$app->request->isAjax) {
            $deliveryPrice = $this->getDeliveryPrice();
            $sum = Products::getSum();
            $bonus = $_GET['bonus'];
            if ($bonus > 0) {

                if($bonus <= $sum){
                    $_SESSION['bonus'] = $bonus;
                }else{
                    $_SESSION['bonus'] = $sum;
                }

                $sum = $this->getSumBasket($deliveryPrice);
            } else {

                $_SESSION['bonus'] = 0;
                $sum = $this->getSumBasket($deliveryPrice);
            }

            $response = ['sum' => intval($sum), 'deliveryPrice' => $deliveryPrice, 'bonus' => $_SESSION['bonus']];
            return json_encode($response);

        }else{
            throw new NotFoundHttpException();
        }
    }





    public function actionCheckGift(){
        if (Yii::$app->request->isAjax) {

            $id = $_GET['id'];
            $type = $_GET['type'];
            $_SESSION['gift_id'] = $id;
            if ($type == 1) $gifts = $_SESSION['gift_for_price'];
            if ($type == 0) $gifts = $_SESSION['gift_for_product'];
            return $this->renderAjax('gift_result', compact('id', 'gifts', 'error', 'type'));
        }else{
            throw new NotFoundHttpException();
        }
    }


    public function actionGetSelectAddress(){
        if (Yii::$app->request->isAjax) {
            $id = $_GET['id'];
            $model = Filial::findOne(['city_id' => Yii::$app->session->get('city_id'), 'id' => $id]);
            return $model->address;
        }else{
            throw new NotFoundHttpException();
        }
    }




    public function actionSetSelectAddress(){
        if (Yii::$app->request->isAjax) {
            $_SESSION['selectedAddress'] = $_GET['address'];
        }else{
            throw new NotFoundHttpException();
        }
    }



    public function actionSaveGift(){

        if (Yii::$app->request->isAjax) {
            if (!isset($_SESSION['gift_id'])) {
                return 'Выберите товар.';
            } else {
                $product = Products::findOne(['id' => $_SESSION['gift_id']]);
                $check = UserGift::findOne(['product_id' => $product->id, 'user_id' => Yii::$app->user->id]);
                if ($check) {
                    $gift = $check;
                    $gift->quantity = $gift->quantity + 1;
                } else {
                    $gift = new UserGift();
                    $gift->quantity = 1;
                }
                $gift->user_id = Yii::$app->user->id;
                $gift->product_id = $product->id;

                if ($gift->save(false)) {
                    if ($_SESSION['gift_for_type'] == 2) {
                        unset($_SESSION['gift_id']);
                        unset($_SESSION['gift_for_price']);
                        $_SESSION['gift_for_type'] = 0;
                        return 2;
                    } else {
                        unset($_SESSION['gift_id']);
                        unset($_SESSION['gift_for_price']);
                        unset($_SESSION['gift_for_product']);
                        unset($_SESSION['gift_for_type']);
                        return 1;
                    }
                } else {
                    return 'Упс, что-то пошло не так!';
                }

            }
        }else{
            throw new NotFoundHttpException();
        }
    }


    public function getDeliveryPrice(){

        $sum = $this->getSumBasketWithoutDelivery();
        $check_free_delivery = PriceFreeDelivery::getContent();
        if($check_free_delivery->price < $sum){
            return 0;
        }else{
             if(isset($_SESSION['deliveryPrice'])){
                 return $_SESSION['deliveryPrice'];
             }else{
                 return 0;
             }
        }
    }

    public function getSumBasket($deliveryPrice){

        $sum = Products::getSum();
        $sum = $sum + $deliveryPrice;

        if(isset($_SESSION['bonus']) && $_SESSION['bonus'] > 0){
            $sum = $sum - $_SESSION['bonus'];
        }

        return $sum;
    }

    public function actionSetPaymentChoice(){
        if (Yii::$app->request->isAjax) {
            Yii::$app->session['payment_choice'] = $_GET['payment_choice'];
        }else{
            throw new NotFoundHttpException();
        }
    }

    public function actionSetDeliveryChoice(){
        if (Yii::$app->request->isAjax) {
            Yii::$app->session['delivery_choice'] = $_GET['delivery_choice'];
        }else{
            throw new NotFoundHttpException();
        }
    }

    public static function getSumBasketWithoutDelivery(){

        $sum = Products::getSum();
        if(isset($_SESSION['bonus']) && $_SESSION['bonus']> 0){
            $sum = $sum - $_SESSION['bonus'];
        }

        return $sum;
    }







}
