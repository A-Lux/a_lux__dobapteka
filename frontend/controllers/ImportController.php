<?php
namespace frontend\controllers;

use app\models\CatalogProducts;
use Yii;
use yii\web\NotFoundHttpException;
use common\models\Products;

/**
 * Site controller
 */
class ImportController extends FrontendController
{
	public function actionRun() {
		
		
		if (($handle = fopen("../../standart_n/warebase.csv", "r")) !== FALSE) {
			$count = 0;
			$analogs = [];
            $soput = [];
			while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
				if(!$count) {$count++;continue;}
        		$data = array_map(function($str) {
					return iconv( "Windows-1251", "UTF-8", $str );
				}, $data );
				if(!($model = Products::find()->where(['ware_id' => $data[16]])->one())) {
                	$model = new Products();
				}
				if(!($category_id = CatalogProducts::find()->where(['title' => $data[15]])->one())){
                    $category = new CatalogProducts();
                    $cyr = [
                        'а','б','в','г','д','е','ё','ж','з','и','й','к','л','м','н','о','п',
                        'р','с','т','у','ф','х','ц','ч','ш','щ','ъ','ы','ь','э','ю','я',
                        'А','Б','В','Г','Д','Е','Ё','Ж','З','И','Й','К','Л','М','Н','О','П',
                        'Р','С','Т','У','Ф','Х','Ц','Ч','Ш','Щ','Ъ','Ы','Ь','Э','Ю','Я',
                        ',','.',' ','/','(',')','|','_'
                    ];
                    $lat = [
                        'a','b','v','g','d','e','io','zh','z','i','y','k','l','m','n','o','p',
                        'r','s','t','u','f','h','ts','ch','sh','sht','a','i','y','e','yu','ya',
                        'a','b','v','g','d','e','io','zh','z','i','y','k','l','m','n','o','p',
                        'r','s','t','u','f','h','ts','ch','sh','sht','a','i','y','e','yu','ya',
                        '-','-','-','-','-','-','-','-'
                    ];

                    $url = str_replace($cyr,$lat, $data[15]);
                    if(substr($url, -1) == '-') $url = substr($url, 0, -1);
                    $url =  preg_replace('/[^a-zA-Z0-9-]/','', $url);

                    $parentUrl = str_replace($cyr,$lat, $data[15]);
                    if(substr($parentUrl, -1) == '-') $parentUrl = substr($parentUrl, 0, -1);
                    $parentUrl =  preg_replace('/[^a-zA-Z0-9-]/','', $parentUrl);

                    $model = CatalogProducts::count();

                    if(!($parent = CatalogProducts::find()->where(['title' => $data[14]])->one())) {
                        $parent = new CatalogProducts();
                        $count = $count + 1;
                        $parent_data = [
                            'parent_id' => null,
                            'title' => $data[14],
                            'description' => $data[14],
                            'name' => $data[14],
                            'url' => $parentUrl,
                            'img' => null,
                            'level' => 1,
                            'sort' => $count,
                            'status' => 1,
                            'created_at' => time(),
                        ];
                    }

                    $count = $count + 1;

                    $category_data = [
                        'parent_id' => $parent->id,
                        'title' => $data[15],
                        'description' => $data[15],
                        'name' => $data[15],
                        'url' => $url,
                        'img' => null,
                        'level' => 2,
                        'sort' => $count,
                        'status' => 1,
                        'created_at' => time(),
                    ];
                }
                $model_data = [
                    'category_id' => $category->id,
                    'title' => !empty($data[2]) ? $data[2] : '',
                    'description' => !empty($data[12]) ? $data[12] : '',
                    'name' => !empty($data[2]) ? $data[2] : '',
                    'status' => !((int)$data[7]<=0),
                    'price' => !empty($data[13]) ? (int)$data[13] : 0,
                    'text' => !empty($data[17]) ? $data[17] : '',
                    'country' => !empty($data[4]) ? $data[4] : '',
                    'model' => '',
					'isHit' => !empty($data[21]) ? $data[21] : 0,
					'isNew' => !empty($data[22]) ? $data[22] : 0,
                    'ware_id' => !empty($data[16]) ? $data[16] : 0,
                ];
                $model->attributes = $model_data;
                $model->url = $model->generateCyrillicToLatin();
                $model->save(false);
                $model->url = $model->url.'_'.$model->id;
                $model->save(false);

                if(!empty($data[25])){
                    array_push($soput,
                        [
                            'soputs' => explode(str_replace('\'', $data[25]), ','),
                            'id' => $model->id
                        ])
                    ;
                }
                if(!empty($data[26])){
                    array_push($analogs,
                        [
                            'analogs' => explode(str_replace('\'', $data[26]), ','),
                            'id' => $model->id
                        ]);
                }
            }

            // TODO: Add soputs and analogs

			fclose($handle);
		}
	}
}