<?php

namespace frontend\controllers;

use common\models\Advantages;
use common\models\Banner;
use common\models\CatalogProducts;
use common\models\Filial;
use common\models\FilialProduct;
use common\models\Mainsub;
use common\models\Menu;
use common\models\Products;
use Yii;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;


/**
 * Site controller
 */
class SiteController extends FrontendController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post', 'get'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
                'foreColor' => 0xffffff,
                'offset' => 3,
                'backColor' =>  0x4a5e68,
                'fontFile'  => '@frontend/web/fonts/Century Gothic.ttf',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */

    public function actionIndex()
    {


        $titles = Mainsub::getAll();
        $banner = Banner::getAll();
        $advantages = Advantages::getAll();
        $filial = Filial::getAll();
        $topMonthProducts = Products::getSomeTopMonthProducts(0);
        $hitProducts = Products::getSomeHitProducts(0);
        $catalog = CatalogProducts::getFirstLevel();

        unset($_SESSION['hit_product_of_set']);
        unset($_SESSION['top_month_product_of_set']);
        // phpinfo();
        // die();
        return $this->render('index', compact('titles', 'banner', 'advantages', 'filial', 'hitProducts', 'catalog', 'topMonthProducts'));
    }


    public function actionIndex2($url)
    {

        $page = Menu::findOne(['url' => $url]);

        if ($page) {
            return $this->render('index2', compact('page'));
        } else {
            throw new NotFoundHttpException();
        }
    }


    public function actionLoadMoreHitProducts()
    {

        if (Yii::$app->request->isAjax) {
            if (isset($_SESSION['hit_product_of_set'])) {
                $_SESSION['hit_product_of_set'] += Products::HIT_PRODUCT_LOAD_QUANTITY;
            } else {
                $_SESSION['hit_product_of_set'] = 4;
            }

            $model = Products::getSomeHitProducts($_SESSION['hit_product_of_set']);
            $id = $_GET['id'];
            $count = 9;

            return $this->renderAjax('products', compact('model', 'id', 'count'));
        } else {
            throw new NotFoundHttpException();
        }
    }





    public function actionLoadMoreHitButton()
    {

        if (Yii::$app->request->isAjax) {
            $offSet = $_SESSION['hit_product_of_set'] + Products::HIT_PRODUCT_LOAD_QUANTITY;
            $model = Products::getSomeHitProducts($offSet);
            if (count($model) > 4) $status = 1;
            else $status = 0;

            return json_encode(['status' => $status]);
        } else {
            throw new NotFoundHttpException();
        }
    }



    public function actionLoadMoreTopMonthProducts()
    {

        if (Yii::$app->request->isAjax) {
            if (isset($_SESSION['top_month_product_of_set'])) {
                $_SESSION['top_month_product_of_set'] += Products::TOP_MONTH_PRODUCT_LOAD_QUANTITY;
            } else {
                $_SESSION['top_month_product_of_set'] = 4;
            }

            $model = Products::getSomeTopMonthProducts($_SESSION['top_month_product_of_set']);
            $id = $_GET['id'];
            $count = 9;

            return $this->renderAjax('products', compact('model', 'id', 'count'));
        } else {
            throw new NotFoundHttpException();
        }
    }



    public function actionLoadMoreTopMonthButton()
    {

        if (Yii::$app->request->isAjax) {
            $offSet = $_SESSION['top_month_product_of_set'] + Products::TOP_MONTH_PRODUCT_LOAD_QUANTITY;
            $model = Products::getSomeTopMonthProducts($offSet);
            if (count($model) > 8) $status = 1;
            else $status = 0;

            return json_encode(['status' => $status]);
        } else {
            throw new NotFoundHttpException();
        }
    }




    public function actionLoadMoreHitProductsMobile()
    {

        if (Yii::$app->request->isAjax) {
            if (isset($_SESSION['hit_product_of_set'])) {
                $_SESSION['hit_product_of_set'] += Products::HIT_PRODUCT_LOAD_QUANTITY;
            } else {
                $_SESSION['hit_product_of_set'] = 4;
            }

            $model = Products::getSomeHitProducts($_SESSION['hit_product_of_set']);
            $id = $_GET['id'];
            $count = 9;

            return $this->renderAjax('mobile_hit_products', compact('model', 'id', 'count'));
        } else {
            throw new NotFoundHttpException();
        }
    }



    public function actionLoadMoreTopMonthProductsMobile()
    {

        if (Yii::$app->request->isAjax) {
            if (isset($_SESSION['top_month_product_of_set'])) {
                $_SESSION['top_month_product_of_set'] += Products::TOP_MONTH_PRODUCT_LOAD_QUANTITY;
            } else {
                $_SESSION['top_month_product_of_set'] = 4;
            }

            $model = Products::getSomeTopMonthProducts($_SESSION['top_month_product_of_set']);
            $id = $_GET['id'];
            $count = 9;

            return $this->renderAjax('mobile_top_month_products', compact('model', 'id', 'count'));
        } else {
            throw new NotFoundHttpException();
        }
    }
}
