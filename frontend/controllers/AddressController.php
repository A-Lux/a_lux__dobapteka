<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 10.12.2019
 * Time: 13:10
 */

namespace frontend\controllers;


use common\models\Filial;
use Yii;
use common\models\Menu;

class AddressController extends FrontendController
{

    public function actionIndex(){

        $filial = Filial::getAll();
        return $this->render('shops',compact('filial'));
    }


    public function actionGetOpenShops(){
        $res = Filial::getAll();
        $filial = array();
        foreach ($res as $v){
            if($v->currentStatus == 'Открыто'){
                array_push($filial,$v);
            }
        }
        return $this->renderAjax('shop_is_open', compact('filial','error'));
    }


    public function actionGetCloseShops(){
        $filial = Filial::getAll();
        return $this->renderAjax('shop_is_open', compact('filial','error'));
    }



    public function actionGetOpenMapShops(){
        $model = Filial::getAll();
        $m=0;
        foreach ($model as $v){
            if($v->currentStatus == 'Открыто'){
                $array = [0 => $v->latitude, 1 => $v->longitude, 2 => $v->address, 3 => $v->telephone];
                $res[$m] = $array;
                $m++;
            }
        }
        return json_encode($res);
    }


    public function actionGetCloseMapShops(){

        $model = Filial::getAll();
        $m=0;
        foreach ($model as $v){
            $array = [0 => $v->latitude, 1 => $v->longitude, 2 => $v->address, 3 => $v->telephone];
            $res[$m] = $array;
            $m++;
        }
        return json_encode($res);
    }



    public function actionGetSearchResult(){
        $text = $_GET['text'];
        $filial = Filial::find()->where("city_id=".Yii::$app->session['city_id']." AND address like '%$text%'")->all();
        return $this->renderAjax('shop_is_open', compact('filial','error'));
    }


    public function actionGetSearchMapResult()
    {
        $text = $_GET['text'];
        $model = Filial::find()->where("city_id=".Yii::$app->session['city_id']." AND address like '%$text%'")->all();
        $m = 0;
        foreach ($model as $v) {
            $array = [0 => $v->latitude, 1 => $v->longitude, 2 => $v->address, 3 => $v->telephone];
            $res[$m] = $array;
            $m++;
        }
        return json_encode($res);
    }
}
