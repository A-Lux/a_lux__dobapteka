<?php

namespace frontend\models;

use common\models\UserFavorites;
use Yii;

class ProductSearch extends \yii\db\ActiveRecord
{
    public $path = 'images/products/';

    public $lev;

    public static function tableName()
    {
        return 'products_search_view';
    }

    public function getCalculatePrice()
    {
        if (!$this->discount) {
            $price =  $this->price;
        } else {
            $price = $this->price * (100 - $this->discount) / 100;
        }

        return intval($price);
    }

    public function getFavoriteStatus()
    {
        if (UserFavorites::findOne(['user_id' => Yii::$app->user->id, 'product_id' => $this->id])) return 1;
        else return 0;
    }

    public function getImage()
    {


        $img = preg_replace_callback('!s:(\d+):"(.*?)";!', function ($match) {
            return ($match[1] == strlen($match[2])) ? $match[0] : 's:' . strlen($match[2]) . ':"' . $match[2] . '";';
        }, $this->images);

        $img = unserialize($img);
        if ($img === false) {
            return null;
        } else {
            return '/backend/web/' . $this->path . $img[0];
        }
    }

    public function getUrl()
    {
        return '/product/' . $this->id . '/' . $this->url;
    }
}
