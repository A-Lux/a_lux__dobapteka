$(document).ready(function () {
    $("#searchbox").autocomplete({
        appendTo: ".search",
        source: function (request, response) {
            $.ajax({
                url: "/autocomplete/index",
                dataType: "json",
                data: {
                    keyword: request.term
                },
                success: function (data) {
                    response($.map(data.users, function (user) {
                        return {
                            label: user.title,
                            value: user.value
                        }
                    }));
                }
            });
        },
        minLength: 2,
        mustMatch: true,
        select: function (event, ui) {
            $('#searchbox').val(ui.item.value);
            var url = "/search?text=" + ui.item.value;
            window.location.href = url;
        },
        open: function () {
            $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
        },
        close: function () {
            $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
        },

    });

    $("#searchboxFixed").autocomplete({
        appendTo: "#searchResult",
        source: function (request, response) {
            $.ajax({
                url: "/autocomplete/index",
                dataType: "json",
                data: {
                    keyword: request.term
                },
                success: function (data) {
                    response($.map(data.users, function (user) {
                        return {
                            label: user.title,
                            value: user.value
                        }
                    }));
                }
            });
        },
        minLength: 2,
        mustMatch: true,
        select: function (event, ui) {
            $('#searchboxFixed').val(ui.item.value);
            return false;
        },
        open: function () {
            $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
        },
        close: function () {
            $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
        }
    });

    $("#searchboxMobile").autocomplete({
        appendTo: ".search",
        source: function (request, response) {
            $.ajax({
                url: "/autocomplete/index",
                dataType: "json",
                data: {
                    keyword: request.term
                },
                success: function (data) {
                    response($.map(data.users, function (user) {
                        return {
                            label: user.title,
                            value: user.value
                        }
                    }));
                }
            });
        },
        minLength: 2,
        mustMatch: true,
        select: function (event, ui) {
            $('#searchboxMobile').val(ui.item.value);
            var url = "/search?text=" + ui.item.value;
            window.location.href = url;
        },
        open: function () {
            $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
        },
        close: function () {
            $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
        }
    });



    $("#searchboxMobileFixed").autocomplete({
        // appendTo: "#searchResult",
        source: function (request, response) {
            $.ajax({
                url: "/card/index",
                dataType: "jsonp",
                data: {
                    featureClass: "P",
                    style: "full",
                    maxRows: 12,
                    name_startsWith: request.term
                },

                success: function (data) {
                    response($.map(data.users, function (user) {
                        return {
                            label: user.title,
                            value: user.value
                        }
                    }));
                }
            });
        },
        minLength: 2,
        mustMatch: true,
        // focus: function(event, ui) {
        //     $('#searchboxMobileFixed').val(ui.item.label);
        //     return false;
        // },
        select: function (event, ui) {
            $('#searchboxMobileFixed').val(ui.item.value);
            return false;
        },
        open: function () {
            $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
        },
        close: function () {
            $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
        }
    });




});
