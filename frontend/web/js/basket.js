$.busyLoadSetup({
  // animation: "slide",
  color: "rgb(183,214,2)",
  background: "rgba(219, 222, 219 , 0.30)",
});

function showLoader() {
  $("body").css("overflow-y", "visible");
  $.busyLoadFull("show", {
    fontawesome: "sbl-circ-ripple animated fa-2x fa-fw",
    maxSize: "100px",
    minSize: "100px",
  });
}

function hideLoader() {
  $.busyLoadFull("hide");
}

function showButtonLoader(id) {
  $(id).html(
    '<i style="margin-right: 7px;" class="fa fa-spinner fa-spin"></i> Проверка'
  );
  $(id).attr("disabled", true);
  $(id).css({ cursor: "default", background: "#818181" });
}

function hideButtonLoader(id, button) {
  $(id).html(button);
  $(id).removeAttr("disabled");
  $(id).css({
    cursor: "pointer",
    background: "linear-gradient(to bottom, #b7d602 0%, #68c316 100%)",
  });
}

$("#order-surname").keyup(function (e) {
  var regex = /^[a-zA-Zа-яА-Яа-яА-Я ]+$/;
  if (regex.test(this.value) !== true)
    this.value = this.value.replace(/[^a-zA-Zа-яА-Яа-яА-Я ]+/, "");
});
$("#order-name").keyup(function (e) {
  var regex = /^[a-zA-Zа-яА-Яа-яА-Я ]+$/;
  if (regex.test(this.value) !== true)
    this.value = this.value.replace(/[^a-zA-Zа-яА-Яа-яА-Я ]+/, "");
});

$("body").on("click", ".btn-in-basket", function (e) {
  var id = $(this).attr("data-id");
  NProgress.start();
  $.ajax({
    url: "/card/add-product",
    type: "GET",
    dataType: "json",
    data: { id: id },
    success: function (data) {
      NProgress.done();
      if (data.status) {

        $(".count").html(data.count);
        $(".count-add").toggleClass("count-active");
        setTimeout(function () {
          $(".count-add").removeClass("count-active");
        }, 1000);
      }else{
        swal("", "Извините, у нас нет больше!", "error");
      }

    },
    error: function () {

    },
  });

  var slider_id = $(this).attr("data-slider-id");
  if (slider_id != null) {
    $.ajax({
      url: "/catalog/offer-related-products",
      type: "GET",
      data: { id: id },
      success: function (data) {
        if (data != 0) {
          $("#" + slider_id).html(data);
          $(".owl2").owlCarousel({
            loop: false,
            margin: 10,
            nav: true,
            responsive: {
              0: {
                items: 2,
              },
              600: {
                items: 3,
              },
              1000: {
                items: 5,
                nav: true,
              },
            },
          });
          document.getElementById(slider_id).scrollIntoView({
            behavior: "smooth",
            block: "end",
            inline: "start",
          });
        }
      },
      error: function () {

      },
    });
  }
  let owlItem2 = $(".owl2 > .owl-item");

  if (owlItem2.length < 5) {
    $(".owl-prev").hide();
    $(".owl-next").hide();
  }
});

$("body").on("click", ".btn-delete-product-from-basket", function (e) {
  var id = $(this).attr("data-id");
  swal({
    title: "Вы уверены?",
    text: "что хотите удалить этот товар!",
    icon: "warning",
    buttons: ["Отмена", "OK"],
    dangerMode: true,
  }).then((willDelete) => {
    if (willDelete) {
      window.location.href = "/card/delete-product?id=" + id;
    } else {
    }
  });
});

$("body").on("click", ".btn-delete-gift-from-basket", function (e) {
  var id = $(this).attr("data-id");
  swal({
    title: "Вы уверены?",
    text: "что хотите удалить этот товар!",
    icon: "warning",
    buttons: ["Отмена", "OK"],
    dangerMode: true,
  }).then((willDelete) => {
    if (willDelete) {
      window.location.href = "/card/delete-gift?id=" + id;
    } else {
    }
  });
});

$("body").on("click", ".minus", function (e) {
  showLoader();
  var id = $(this).attr("data-id");
  $.ajax({
    url: "/card/down-clicked",
    type: "GET",
    dataType: "json",
    data: { id: id },
    success: function (data) {
      $("#countProduct" + id).html(data.countProduct);
      $("#sumBasket").html(data.sum + " <span> тг</span>");
      $("#sumProduct" + id).html(data.sumProduct + " <span> тг</span>");
      $(".deliveryPrice").html(data.deliveryPrice + " <span> тг</span>");
      hideLoader();
    },
    error: function () {
      hideLoader();
      swal("Упс!", "Что-то пошло не так.", "error");
    },
  });
});

$("body").on("click", ".plus", function (e) {
  showLoader();
  var id = $(this).attr("data-id");
  $.ajax({
    url: "/card/up-clicked",
    type: "GET",
    dataType: "json",
    data: { id: id },
    success: function (data) {
        if(data.status == 1){
            $("#countProduct" + id).html(data.countProduct);
            $("#sumBasket").html(data.sum + " <span> тг</span>");
            $("#sumProduct" + id).html(data.sumProduct + " <span> тг</span>");
            $(".deliveryPrice").html(data.deliveryPrice + " <span> тг</span>");
           hideLoader();
        }else{
            hideLoader();
            $("#countProduct" + id).val(data.countProduct);
            swal("", "Извините, у нас нет больше", "error");
        }
    },
    error: function () {
        hideLoader();
        swal("Упс!", "Что-то пошло не так.", "error");
    },
  });
});

$("body").on("keyup", ".quantity", function (e) {
  showLoader();
  var id = $(this).attr("data-id");
  var v = $(this).val();
  $.ajax({
      url: "/card/count-changed",
      type: "GET",
      dataType: "json",
      data: { id: id, v: v },
      success: function (data) {
          if(data.status == 1) {
            $("#countProduct" + id).html(data.countProduct);
            $("#sumBasket").html(data.sum + " <span> тг</span>");
            $("#sumProduct" + id).html(data.sumProduct + " <span> тг</span>");
            $(".deliveryPrice").html(data.deliveryPrice + " <span> тг</span>");
            hideLoader();
          }else{
            hideLoader();
            $("#countProduct" + id).val(data.countProduct);
            swal("", "Извините, у нас нет больше!", "error");
          }
      },
    error: function () {
      hideLoader();
      swal("Упс!", "Что-то пошло не так.", "error");
    },
  });
});

$("#button-pay").click(function () {

  showLoader();
  var paymentMethod = $("#typePay").val();
  var deliveryMethod = $("#typeDelivery").val();
  var change = $("#change").val();
  var address = $("#deliveryAddress").html();
  var apartment = $('#apartment').val();
  var longitude = $("#longitude").val();
  var latitude = $("#latitude").val();
  var bonus = $('#bonus').val();

  if (paymentMethod != 1 && paymentMethod != 2) {
    hideLoader();
    swal("", "Выберите способ оплаты!", "error");
  } else if (deliveryMethod != 1 && deliveryMethod != 2) {
    hideLoader();
    swal("", "Выберите способ доставки!", "error");
  } else {
    if (paymentMethod == 1 && change == "") {
      hideLoader();
      swal("", "Укажите сдачу с суммы!", "error");
    } else {
      if (deliveryMethod == 1) {
        if (address == "") {
          hideLoader();
          swal("", "Необходимо указать место доставки!", "error");
        }else if(apartment == '' && document.getElementById('typeAddress').value == 0){
          hideLoader();
          $("#exampleModal").modal('show');
          swal('', 'Необходимо указать номер квартиры!', 'error');

        }else {
          $.ajax({
            type: "GET",
            url: "/card/order",
            data: {
              change: change,
              address: address,
              longitude: longitude,
              latitude: latitude,
              deliveryMethod: deliveryMethod,
              paymentMethod: paymentMethod,
              bonus: bonus,
              apartment:apartment
            },
            success: function (data) {
              if (data == 1) {
                window.location.href = "/account/?tab=orders";
              } else if (data == 2) {
                window.location.href = "/card/gift";
              } else if (data == 0) {
                swal("", "Что-то ошло не так!", "error");
              } else if (data == 3) {
                window.location.href = "/";
              } else if (data == 4) {
                swal({
                  title: "",
                  text: "Хотите ли Вы зарегистрироваться?",
                  icon: "info",
                  buttons: ["Нет", "Да"],
                  dangerMode: true,
                }).then((willDelete) => {
                  if (willDelete) {
                    window.location.href = "/account/sign-up";
                  } else {
                    $("#card-confirm").modal("show");
                  }
                });
              } else if (data == 5) {
                window.location.href = "/paybox/index";
              }else if (data == 6) {

                Swal.fire({
                  title: 'Уважаемый покупатель!',
                  text: "Уведомляем Вас, что в случае осуществления заказа после 21:00, доставка лекарств переносится на следующий день. Вы согласны?",
                  icon: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: '#68c316',
                  cancelButtonColor: '#d33',
                  confirmButtonText: 'Да',
                  cancelButtonText : 'Нет'
                }).then((result) => {
                  if (result.value) {
                    $("#button-pay").click();
                  }
                });

              }else {
                swal("", data, "error");}

              hideLoader();
            },
            error: function () {
              hideLoader();
            },
          });
        }
      } else if (deliveryMethod == 2) {

        $.ajax({
          type: "GET",
          url: "/card/order",
          data: {
            change: change,
            deliveryMethod: deliveryMethod,
            paymentMethod: paymentMethod,
            bonus: bonus
          },
          success: function (data) {
            $("#button-pay").html("Оплатить");
            if (data == 1) {
              window.location.href = "/account/?tab=orders";
            } else if (data == 2) {
              window.location.href = "/card/gift";
            } else if (data == 0) {
              swal("", "Что-то ошло не так!", "error");
            } else if (data == 3) {
              window.location.href = "/";
            } else if (data == 4) {
              swal({
                title: "",
                text: "Хотите ли Вы зарегистрироваться?",
                icon: "info",
                buttons: ["Нет", "Да"],
                dangerMode: true,
              }).then((willDelete) => {
                if (willDelete) {
                  window.location.href = "/account/sign-up";
                } else {
                  $("#card-confirm").modal("show");
                }
              });
            } else if (data == 5) {
              window.location.href = "/paybox/index";
            }else if (data == 6) {

              Swal.fire({
                title: 'Уважаемый покупатель!',
                text: "Уведомляем Вас, что в случае осуществления заказа после 21:00, доставка лекарств переносится на следующий день. Вы согласны?",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#68c316',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Да',
                cancelButtonText : 'Нет'
              }).then((result) => {
                if (result.value) {
                  $("#button-pay").click();
                }
              });

            } else {
              swal("", data, "error");
            }

            hideLoader();
          },
          error: function () {
            hideLoader();
          },
        });
      }
    }
  }
});




$("body").on("click", ".order_guest_confirm", function (e) {
  var button = $(this).html();
  showButtonLoader(".order_guest_confirm");

  $.ajax({
    type: "POST",
    url: "/card/confirm-ordered-guest-data",
    data: $(this).closest("form").serialize(),
    success: function (response) {
      hideButtonLoader(".order_guest_confirm", button);
      if (response == 1) {
        $(".close").click();
        $("#button-pay").click();
      } else {
        swal("", response, "error");
      }
    },
    error: function () {
      hideButtonLoader(".order_guest_confirm", button);
      swal("Упс!", "Что-то пошло не так.", "error");
    },
  });
});

$('#change').on('keyup ', function(){

    var change = $(this).val();
    if(change < 0){
      $(this).val(0);
    }
});


$('#bonus').on('change', function(){


  showLoader();
  var bonus = $(this).val();
  var max = $(this).attr('max');


  if(bonus < 0){
    $(this).val(0);
    bonus = 0;
  }

  if(parseInt(bonus) > parseInt(max)){
    $(this).val(max);
    bonus = max;
  }

  if(bonus == ""){
    bonus = 0;
  }



  if(bonus > -1) {
    $.ajax({
      url: "/card/bonus",
      type: "GET",
      dataType: "json",
      data: {bonus:bonus},
      success: function (data) {

          $("#sumBasket").html(data.sum + " <span> тг</span>");
          $(".deliveryPrice").html(data.deliveryPrice + " <span> тг</span>");
          if(data.bonus != 0){
            $("#bonus").val(data.bonus);
          }
          hideLoader();
      },
      error: function () {
          hideLoader();
          swal("Упс!", "Что-то пошло не так.", "error");
      },
    });
  }else{
    hideLoader();
  }

});



$("#typePay").on("change", function () {

  if ($(this).val() == 1) {
    $(".sdacha").show();
    $("#button-pay").html("Заказать");
  } else {
    $(".sdacha").hide();
    $("#button-pay").html("Оплатить");
  }

  $.ajax({
    url: "/card/set-payment-choice",
    type: "GET",
    data: { payment_choice: $(this).val() },
    success: function (data) {},
  });

});



$("#typeDelivery").on("change", function () {

    if ($(this).val() == 1) {
      $("#deliveryInform").show();
      $("#addressInform").hide();

    } else if ($(this).val() == 2) {

      $("#addressInform").show();
      $("#deliveryInform").hide();
    } else {
      $("#deliveryInform").hide();
      $("#addressInform").hide();
    }

    $.ajax({
      url: "/card/set-delivery-choice",
      type: "GET",
      data: { delivery_choice: $(this).val() },
      success: function (data) {

      },
    });

    if ($(this).val() == 1 && $("#fenceAddress").html() != '') {
      location.reload();
    } else if ($(this).val() == 2 && $("#deliveryAddress").html() != '') {
      location.reload();
    }
});

$("body").on("click", ".green-btn", function (e) {
  var button = $(this).html();
  showButtonLoader(".green-btn");
  var code = $(".promocode").val();
  if (code == "") {
    hideButtonLoader(".green-btn", button);
    swal("", "Промокод не может быть пустым", "error");
  } else {
    $.ajax({
      url: "/card/check-promo",
      type: "GET",
      dataType: "json",
      data: { code: code },
      success: function (data) {
        if (data.status == 1) {
          $("#percentPromo").attr("data-discount", data.percent);
          $("#percentPromo").html(data.percent + "<span>%</span>");
          $("#sumBasket").html(data.sum + " <span> тг</span>");
          $(".deliveryPrice").html(data.deliveryPrice + " <span> тг</span>");
        } else swal("", data.error);

        hideButtonLoader(".green-btn", button);
      },
      error: function () {
        hideButtonLoader(".green-btn", button);
        swal("Упс!", "Что-то пошло не так.", "error");
      },
    });
  }
});



$("body").on("click", ".gift_for_price", function () {
  showLoader();
  var id = $(this).attr("data-id");
  var type = 1;
  $.ajax({
    url: "/card/check-gift",
    type: "GET",
    data: { id: id, type: type },
    success: function (data) {
      $("#checkResult").html(data);
      hideLoader();
    },
    error: function () {
      hideLoader();
      swal("Упс!", "Что-то пошло не так.", "error");
    },
  });
});

$("body").on("click", ".gift_for_product", function () {
  showLoader();
  var id = $(this).attr("data-id");
  var type = 0;
  $.ajax({
    url: "/card/check-gift",
    type: "GET",
    data: { id: id, type: type },
    success: function (data) {
      $("#checkResult").html(data);
      hideLoader();
    },
    error: function () {
      hideLoader();
      swal("Упс!", "Что-то пошло не так.", "error");
    },
  });
});

$("body").on("click", "#save_gift", function () {
  showLoader();
  $.ajax({
    url: "/card/save-gift",
    type: "GET",
    success: function (data) {
      hideLoader();
      if (data == 1) window.location.href = "/account/?tab=gift";
      else if (data == 2) window.location.href = "/card/gift";
      else swal("Ошибка!", data, "error");
    },
    error: function () {
      hideLoader();
      swal("Упс!", "Что-то пошло не так.", "error");
    },
  });
});
