$(document).ready(function () {

  $("body").on('click', '#show-more-catalog', function(){
      var status = $('#show-more-catalog').attr('aria-expanded');
      if(status == "false") {
        $('#show-more-catalog').html("- Скрыть");
      }else{
        $('#show-more-catalog').html("+ Показать все");
      }
  });

  $("body").on("keyup", ".from_price", function () {
    NProgress.start();
    //if($(this).val().length >= 3)
    var v_nalichii = 0;
    if ($(".v_nalichii").is(":checked")) v_nalichii = 1;
    var akcii = 0;
    if ($(".akcii").is(":checked")) akcii = 1;
    var category_id = $(".category_id").val();

    $.ajax({
      type: "GET",
      url: "/catalog/filter",
      data: {
        from: $(".from_price").val(),
        to: $(".to_price").val(),
        category_id: category_id,
        v_nalichii: v_nalichii,
        akcii: akcii,
        view: "filter"
      },
      success: function (html) {
        $(".tablet-version-hide.items").html(html);
        NProgress.done();
      },
    });
  });

  $("body").on("keyup", ".to_price", function () {
    NProgress.start();
    //if($(this).val().length >= 3)
    var v_nalichii = 0;
    if ($(".v_nalichii").is(":checked")) v_nalichii = 1;
    var akcii = 0;
    if ($(".akcii").is(":checked")) akcii = 1;
    var category_id = $(".category_id").val();

    $.ajax({
      type: "GET",
      url: "/catalog/filter",
      data: {
        from: $(".from_price").val(),
        to: $(".to_price").val(),
        category_id: category_id,
        v_nalichii: v_nalichii,
        akcii: akcii,
        view: "filter"
      },
      success: function (html) {
        $(".tablet-version-hide.items").html(html);
        NProgress.done();
      },
    });
  });



  $("body").on("click", ".v_nalichii", function () {
    NProgress.start();
    //if($(this).val().length >= 3)
    var v_nalichii = 0;
    if ($(".v_nalichii").is(":checked")) v_nalichii = 1;
    var akcii = 0;
    if ($(".akcii").is(":checked")) akcii = 1;
    var category_id = $(".category_id").val();

    $.ajax({
      type: "GET",
      url: "/catalog/filter",
      data: {
        from: $(".from_price").val(),
        to: $(".to_price").val(),
        category_id: category_id,
        v_nalichii: v_nalichii,
        akcii: akcii,
        view: "filter"
      },
      success: function (html) {
        $(".tablet-version-hide.items").html(html);
        NProgress.done();
      },
    });
  });

  $("body").on("click", ".akcii", function () {
    NProgress.start();
    //if($(this).val().length >= 3)
    var v_nalichii = 0;
    if ($(".v_nalichii").is(":checked")) v_nalichii = 1;
    var akcii = 0;
    if ($(".akcii").is(":checked")) akcii = 1;
    var category_id = $(".category_id").val();

    $.ajax({
      type: "GET",
      url: "/catalog/filter",
      data: {
        from: $(".from_price").val(),
        to: $(".to_price").val(),
        category_id: category_id,
        v_nalichii: v_nalichii,
        akcii: akcii,
        view: "filter"
      },
      success: function (html) {
        $(".tablet-version-hide.items").html(html);
        NProgress.done();
      },
    });
  });

  $("body").on("keyup", ".from_price_mob", function () {
    NProgress.start();
    var v_nalichiiMob = 0;
    if ($(".v_nalichiiMob").is(":checked")) v_nalichiiMob = 1;
    var akciiMob = 0;
    if ($(".akciiMob").is(":checked")) akciiMob = 1;
    var category_id = $(".category_id").val();

    $.ajax({
      type: "GET",
      url: "/catalog/filter",
      data: {
        from: $(".from_price_mob").val(),
        to: $(".to_price_mob").val(),
        v_nalichii: v_nalichiiMob,
        akcii: akciiMob,
        category_id: category_id,
        view: "filterMob"
      },
      success: function (html) {
        $("#home").html(html);
        NProgress.done();
      },
    });
  });

  $("body").on("keyup", ".to_price_mob", function () {
    NProgress.start();
    var v_nalichiiMob = 0;
    if ($(".v_nalichiiMob").is(":checked")) v_nalichiiMob = 1;
    var akciiMob = 0;
    if ($(".akciiMob").is(":checked")) akciiMob = 1;
    var category_id = $(".category_id").val();

    $.ajax({
      type: "GET",
      url: "/catalog/filter",
      data: {
        from: $(".from_price_mob").val(),
        to: $(".to_price_mob").val(),
        v_nalichii: v_nalichiiMob,
        akcii: akciiMob,
        category_id: category_id,
        view: "filterMob"
      },
      success: function (html) {
        $("#home").html(html);
        NProgress.done();
      },
    });
  });

  $("body").on("click", ".v_nalichiiMob", function () {
    NProgress.start();
    var v_nalichiiMob = 0;
    if ($(".v_nalichiiMob").is(":checked")) v_nalichiiMob = 1;
    var akciiMob = 0;
    if ($(".akciiMob").is(":checked")) akciiMob = 1;
    var category_id = $(".category_id").val();

    $.ajax({
      type: "GET",
      url: "/catalog/filter",
      data: {
        from: $(".from_price_mob").val(),
        to: $(".to_price_mob").val(),
        v_nalichii: v_nalichiiMob,
        akcii: akciiMob,
        category_id: category_id,
        view: "filterMob"
      },
      success: function (html) {
        $("#home").html(html);
        NProgress.done();
      },
    });
  });

  $("body").on("click", ".akciiMob", function () {
    NProgress.start();
    var v_nalichiiMob = 0;
    if ($(".v_nalichiiMob").is(":checked")) v_nalichiiMob = 1;
    var akciiMob = 0;
    if ($(".akciiMob").is(":checked")) akciiMob = 1;
    var category_id = $(".category_id").val();

    $.ajax({
      type: "GET",
      url: "/catalog/filter",
      data: {
        from: $(".from_price_mob").val(),
        to: $(".to_price_mob").val(),
        v_nalichii: v_nalichiiMob,
        akcii: akciiMob,
        category_id: category_id,
        view: "filterMob"
      },
      success: function (html) {
        $("#home").html(html);
        NProgress.done();
      },
    });
  });

  $("body").on("click", "#load-more button", function () {
    NProgress.start();
    var v_nalichii = 0;
    if ($(".v_nalichii").is(":checked")) v_nalichii = 1;
    var akcii = 0;
    if ($(".akcii").is(":checked")) akcii = 1;

    var category_id = $(".category_id").val();

    $.ajax({
      type: "GET",
      url: "/catalog/load-more-products",
      data: {
        category_id: category_id,
        from: $(".from_price").val(),
        to: $(".to_price").val(),
        v_nalichii: v_nalichii,
        akcii: akcii,
      },
      success: function (html) {
        $("#loadMoreResults").append(html);
      },
    });

    $.ajax({
      type: "GET",
      url: "/catalog/load-more-button",
      data: {
        category_id: category_id,
        from: $(".from_price").val(),
        to: $(".to_price").val(),
        v_nalichii: v_nalichii,
        akcii: akcii,
        isMobile: 0,
      },
      success: function (html) {
        $("#loadMoreButton").html(html);
        NProgress.done();
      },
    });
  });

  $("body").on("click", "#load-more-mob button", function () {

    NProgress.start();
    var v_nalichiiMob = 0;
    if ($(".v_nalichiiMob").is(":checked")) v_nalichiiMob = 1;
    var akciiMob = 0;
    if ($(".akciiMob").is(":checked")) akciiMob = 1;

    var category_id = $(".category_id").val();

    $.ajax({
      type: "GET",
      url: "/catalog/load-more-products",
      data: {
        category_id: category_id,
        from: $(".from_price_mob").val(),
        to: $(".to_price_mob").val(),
        v_nalichii: v_nalichiiMob,
        akcii: akciiMob,
      },
      success: function (html) {
        $("#loadMoreResultsMob").append(html);
      },
    });

    $.ajax({
      type: "GET",
      url: "/catalog/load-more-button",
      data: {
        category_id: category_id,
        from: $(".from_price_mob").val(),
        to: $(".to_price_mob").val(),
        v_nalichii: v_nalichiiMob,
        akcii: akciiMob,
        isMobile: 1,
      },
      success: function (html) {
        $("#loadMoreButtonMob").html(html);
        NProgress.done();
      },
    });
  });

  $("body").on("click", ".hit-product-load-more button", function () {
    NProgress.start();
    $.ajax({
      type: "GET",
      url: "/site/load-more-hit-products",
      data: { id: $(this).attr("data-id") },
      success: function (html) {
        $("#hitProducts").append(html);
      },
    });

    $.ajax({
      type: "GET",
      url: "/site/load-more-hit-button",
      data: {},
      dataType: "json",
      success: function (data) {
        if (data.status == 0) {
          $("#hitProductLoadMore").hide();
        }

        NProgress.done();
      },
    });
  });

  $("body").on("click", ".top-month-product-load-more button", function () {
    NProgress.start();
    $.ajax({
      type: "GET",
      url: "/site/load-more-top-month-products",
      data: { id: $(this).attr("data-id") },
      success: function (html) {
        $("#topMonthProducts").append(html);
      },
    });

    $.ajax({
      type: "GET",
      url: "/site/load-more-top-month-button",
      data: { id: $(this).attr("data-id") },
      dataType: "json",
      success: function (data) {
        if (data.status == 0) {
          $("#topMonthProductLoadMore").hide();
        }

        NProgress.done();
      },
    });
  });

  $("body").on("click", ".top-month-product-load-more-mob button", function () {
    NProgress.start();

    $.ajax({
      type: "GET",
      url: "/site/load-more-top-month-products-mobile",
      data: { id: $(this).attr("data-id") },
      success: function (html) {
        $("#mobileTopMonthProducts").append(html);
      },
    });

    $.ajax({
      type: "GET",
      url: "/site/load-more-top-month-button",
      data: { id: $(this).attr("data-id") },
      dataType: "json",
      success: function (data) {
        if (data.status == 0) {
          $("#topMonthProductLoadMoreMobile").hide();
        }
        NProgress.done();
      },
    });
  });



  $("body").on("click", ".hit-product-load-more-mob button", function () {
    NProgress.start();
    $.ajax({
      type: "GET",
      url: "/site/load-more-hit-products-mobile",
      data: { id: $(this).attr("data-id") },
      success: function (html) {
        $("#mobileHitProducts").append(html);
      },
    });

    $.ajax({
      type: "GET",
      url: "/site/load-more-hit-button",
      data: {},
      dataType: "json",
      success: function (data) {
        if (data.status == 0) {
          $("#hitProductLoadMoreMob").hide();
        }

        NProgress.done();
      },
    });
  });
});
