<footer class="footer">
    <div class="container">
        <div class="row wow fadeInUp">
            <div class="mobile-version col-sm-12">
                <div class="mobile-phone">
                    <a href="tel:+7 747 094-13-00"><img src="images/mobile-phone.png" alt=""></a>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="footer-text">
                    <p>@Добрая Аптека 2-14-2019</p>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="footer-text">
                    <p>Мы в соцсетях: <a href=""><img src="images/soc1.png" alt=""></a><a href=""><img src="images/soc2.png" alt=""></a></p>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="footer-text">
                    <p>Разработано в <a href="https://www.a-lux.kz/">A-lux</a></p>
                </div>
            </div>
        </div>
    </div>
</footer>
<script src="js/jquery-3.2.1.min.js"></script>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>

<script src="js/owl.carousel.min.js"></script>
<script src="js/hc-offcanvas-nav.js"></script>
<script src="js/main.js"></script>
<!--<script src="js/wow.min.js"></script>-->
<!--<script>-->
<!--    new WOW().init();-->
<!--</script>-->

</body>


</html>