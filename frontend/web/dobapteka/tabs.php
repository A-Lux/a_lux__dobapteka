<?php require_once 'header.php' ?>
<div class="main">
    <div class="tab-line">
        <div class="container">
            <ul class="nav nav-tabs wow slideInDown" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Личные данные</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Заказы</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#bonus" role="tab" aria-controls="bonus" aria-selected="false">Бонусы</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Подарки</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#favorite" role="tab" aria-controls="favorite" aria-selected="false">Избранное</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="white-section">
        <div class="container">
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade  show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                    <div class="row">
                        <div class="col-sm-6 wow fadeInLeft">
                            <div class="personal-input">
                                <div class="icon">
                                    <img src="images/user-icon-light.png" alt="">
                                </div>
                                <input type="text" placeholder="Фамилия">
                            </div>
                            <div class="personal-input">
                                <div class="icon">
                                    <img src="images/user-icon-light.png" alt="">
                                </div>
                                <input type="text" placeholder="Имя">
                            </div>
                            <div class="personal-input">
                                <div class="icon">
                                    <img src="images/user-icon-light.png" alt="">
                                </div>
                                <input type="text" placeholder="Отчество">
                            </div>
                            <div class="personal-input">
                                <div class="icon">
                                    <img src="images/msg-icon.png" alt="">
                                </div>
                                <input type="text" placeholder="Почта">
                            </div>
                            <div class="personal-input">
                                <div class="icon">
                                    <img src="images/lock-icon.png" alt="">
                                </div>
                                <input type="password" id="myInput" placeholder="Пароль">
                                <button class="show-pass" onclick="showPass()"><img src="images/show-icon.png" alt=""></button>
                            </div>
                            <div class="personal-input">
                                <div class="icon">
                                    <img src="images/loc-icon.png" alt="">
                                </div>
                                <input type="text" placeholder="Адрес">
                            </div>
                            <div class="field_wrapper">
                                <div>

                                </div>
                                <div class="add-address gradient-text">
                                    <a class="add_button" href="javascript:void(0);">+ Добавить адрес</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 wow fadeInRight">
                            <div class="personal-input">
                                <div class="icon">
                                    <img src="images/user-icon-light.png" alt="">
                                </div>
                                <input type="text" placeholder="Логин">
                            </div>
                            <div class="personal-input">
                                <div class="icon">
                                    <img src="images/phone-icon-light.png" alt="">
                                </div>
                                <input type="text" placeholder="Номер  ">
                            </div>
                            <div class="personal-input">
                                <div class="icon">
                                    <img src="images/lock-icon.png" alt="">
                                </div>
                                <input type="password" id="myInput2" placeholder="Повторить пароль">
                                <button class="show-pass" onclick="showPass2()"><img src="images/show-icon.png" alt=""></button>
                            </div>
                            <div class="personal-input">
                                <div class="icon">
                                    <img src="images/date-icon.png" alt="">
                                </div>
                                <input type="text" placeholder="Дата рождения">
                            </div>
                        </div>
                        <div class="col-sm-12 wow fadeInUp">
                            <div class="save-btn">
                                <button>Сохранить изменения</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                    <div class="row">
                        <div class="col-sm-6 ">
                            <div class="purchased-item">
                                <div class="title">
                                    <h5>Йодомарин 10 табл.</h5>
                                    <div class="text">
                                        <div class="left">
                                            <p>Номер заказа:</p>
                                        </div>
                                        <div class="right">
                                            <p>3589458435</p>
                                        </div>
                                    </div>
                                    <div class="text">
                                        <div class="left">
                                            <p>Статус:</p>
                                        </div>
                                        <div class="right green-text">
                                            <p class="gradient-text">Оплачено</p>
                                        </div>
                                    </div>
                                    <div class="text">
                                        <div class="left">
                                            <p>Цена:</p>
                                        </div>
                                        <div class="right">
                                            <p>439 тг</p>
                                        </div>
                                    </div>
                                    <div class="text">
                                        <div class="left">
                                            <p>Дата прибытие:</p>
                                        </div>
                                        <div class="right">
                                            <p>24.03.19</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="purchased-item space-top-sm">
                                <div class="title">
                                    <h5>Doliva маска расслабляющая 30 мл</h5>
                                    <div class="text">
                                        <div class="left">
                                            <p>Номер заказа:</p>
                                        </div>
                                        <div class="right">
                                            <p>3589458435</p>
                                        </div>
                                    </div>
                                    <div class="text">
                                        <div class="left">
                                            <p>Статус:</p>
                                        </div>
                                        <div class="right green-text">
                                            <p class="red-text">Ожидает оплату</p>
                                        </div>
                                    </div>
                                    <div class="text">
                                        <div class="left">
                                            <p>Цена:</p>
                                        </div>
                                        <div class="right">
                                            <p>639 тг</p>
                                        </div>
                                    </div>
                                    <div class="text">
                                        <div class="left">
                                            <p>Дата прибытие:</p>
                                        </div>
                                        <div class="right">
                                            <p>----</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="pagination">
                                <ul>
                                    <li><a href=""><i class="fas fa-chevron-left"></i></a></li>
                                    <li><a href="">1</a></li>
                                    <li><a href="">2</a></li>
                                    <li><a href="">3</a></li>
                                    <li><a href=""><i class="fas fa-chevron-right"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="bonus" role="tabpanel" aria-labelledby="contact-tab">
                    <div class="row">
                        <div class="col-sm-4 ">
                            <img src="images/bonus-img.png" class="img-fluid" alt="">
                        </div>
                        <div class="col-sm-8 ">
                            <div class="bonus-title">
                                <h5>Балланс:</h5>
                            </div>
                            <div class="bonus-text">
                                <span class="gradient-text">460</span> бонусов
                            </div>
                            <div class="bonus-btn">
                                <button>Обменять</button>
                                <button>Снять</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                    <div class="row wow fadeInUp">
                        <div class="col-sm-3">
                            <div class="gift-item">
                                <img src="images/img1.png" alt="">
                                <p>Йодомарин 10 табл.</p>
                                <div class="free">
                                    <p class="gradient-text">Бесплатно</p>
                                </div>
                                <div class="checked">
                                    <img src="images/check.png" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="gift-item">
                                <img src="images/img3.png" alt="">
                                <p>Йодомарин 10 табл.</p>
                                <div class="free">
                                    <p class="gradient-text">Бесплатно</p>
                                </div>
                                <div class="checked">
                                    <img src="images/check.png" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="gift-item">
                                <img src="images/img1.png" alt="">
                                <p>Йодомарин 10 табл.</p>
                                <div class="free">
                                    <p class="gradient-text">Бесплатно</p>
                                </div>
                                <div class="checked">
                                    <img src="images/check.png" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="gift-item">
                                <img src="images/img1.png" alt="">
                                <p>Doliva маска расслабляющая 30 мл</p>
                                <div class="free">
                                    <p class="gradient-text">Бесплатно</p>
                                </div>
                                <div class="checked">
                                    <img src="images/check.png" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="basket-button">
                                <button style="margin: 40px 0;"><img src="images/light-basket.png" alt="">В корзину</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="favorite" role="tabpanel" aria-labelledby="contact-tab">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="catalog-item">
                                <div class="image">
                                    <img src="images/img1.png" alt="">
                                </div>
                                <div class="text-block">
                                    <div class="delete-icon">
                                        &#10005
                                    </div>
                                    <div class="name">
                                        <p>Йодомарин 10 табл.</p>
                                    </div>
                                    <div class="status">
                                        <p><img src="images/ok.png" alt="">Есть в наличии</p>
                                    </div>
                                    <div class="flex-one">
                                        <div class="price">
                                            <p class="gradient-text">369 <span> тг.</span></p>
                                        </div>
                                        <div class="sale-basket-button">
                                            <button><img src="images/light-basket.png" alt="">В корзину</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="catalog-item">
                                <div class="image">
                                    <img src="images/img1.png" alt="">
                                </div>
                                <div class="text-block">
                                    <div class="delete-icon">
                                        &#10005
                                    </div>
                                    <div class="name">
                                        <p>Йодомарин 10 табл.</p>
                                    </div>
                                    <div class="status">
                                        <p><img src="images/ok.png" alt="">Есть в наличии</p>
                                    </div>
                                    <div class="flex-one">
                                        <div class="price">
                                            <p class="gradient-text">369 <span> тг.</span></p>
                                        </div>
                                        <div class="sale-basket-button">
                                            <button><img src="images/light-basket.png" alt="">В корзину</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="catalog-item">
                                <div class="image">
                                    <img src="images/img1.png" alt="">
                                </div>
                                <div class="text-block">
                                    <div class="delete-icon">
                                        &#10005
                                    </div>
                                    <div class="name">
                                        <p>Йодомарин 10 табл.</p>
                                    </div>
                                    <div class="status">
                                        <p><img src="images/ok.png" alt="">Есть в наличии</p>
                                    </div>
                                    <div class="flex-one">
                                        <div class="price">
                                            <p class="gradient-text">369 <span> тг.</span></p>
                                        </div>
                                        <div class="sale-basket-button">
                                            <button><img src="images/light-basket.png" alt="">В корзину</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="catalog-item">
                                <div class="image">
                                    <img src="images/img1.png" alt="">
                                </div>
                                <div class="text-block">
                                    <div class="delete-icon">
                                        &#10005
                                    </div>
                                    <div class="name">
                                        <p>Йодомарин 10 табл.</p>
                                    </div>
                                    <div class="status">
                                        <p><img src="images/ok.png" alt="">Есть в наличии</p>
                                    </div>
                                    <div class="flex-one">
                                        <div class="price">
                                            <p class="gradient-text">369 <span> тг.</span></p>
                                        </div>
                                        <div class="sale-basket-button">
                                            <button><img src="images/light-basket.png" alt="">В корзину</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="catalog-item">
                                <div class="image">
                                    <img src="images/img1.png" alt="">
                                </div>
                                <div class="text-block">
                                    <div class="delete-icon">
                                        &#10005
                                    </div>
                                    <div class="name">
                                        <p>Йодомарин 10 табл.</p>
                                    </div>
                                    <div class="status">
                                        <p><img src="images/ok.png" alt="">Есть в наличии</p>
                                    </div>
                                    <div class="flex-one">
                                        <div class="price">
                                            <p class="gradient-text">369 <span> тг.</span></p>
                                        </div>
                                        <div class="sale-basket-button">
                                            <button><img src="images/light-basket.png" alt="">В корзину</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="catalog-item">
                                <div class="image">
                                    <img src="images/img1.png" alt="">
                                </div>
                                <div class="text-block">
                                    <div class="delete-icon">
                                        &#10005
                                    </div>
                                    <div class="name">
                                        <p>Йодомарин 10 табл.</p>
                                    </div>
                                    <div class="status">
                                        <p><img src="images/ok.png" alt="">Есть в наличии</p>
                                    </div>
                                    <div class="flex-one">
                                        <div class="price">
                                            <p class="gradient-text">369 <span> тг.</span></p>
                                        </div>
                                        <div class="sale-basket-button">
                                            <button><img src="images/light-basket.png" alt="">В корзину</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="catalog-item">
                                <div class="image">
                                    <img src="images/img1.png" alt="">
                                </div>
                                <div class="text-block">
                                    <div class="delete-icon">
                                        &#10005
                                    </div>
                                    <div class="name">
                                        <p>Йодомарин 10 табл.</p>
                                    </div>
                                    <div class="status">
                                        <p><img src="images/ok.png" alt="">Есть в наличии</p>
                                    </div>
                                    <div class="flex-one">
                                        <div class="price">
                                            <p class="gradient-text">369 <span> тг.</span></p>
                                        </div>
                                        <div class="sale-basket-button">
                                            <button><img src="images/light-basket.png" alt="">В корзину</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="catalog-item">
                                <div class="image">
                                    <img src="images/img1.png" alt="">
                                </div>
                                <div class="text-block">
                                    <div class="delete-icon">
                                        &#10005
                                    </div>
                                    <div class="name">
                                        <p>Йодомарин 10 табл.</p>
                                    </div>
                                    <div class="status">
                                        <p><img src="images/ok.png" alt="">Есть в наличии</p>
                                    </div>
                                    <div class="flex-one">
                                        <div class="price">
                                            <p class="gradient-text">369 <span> тг.</span></p>
                                        </div>
                                        <div class="sale-basket-button">
                                            <button><img src="images/light-basket.png" alt="">В корзину</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="catalog-item">
                                <div class="image">
                                    <img src="images/img1.png" alt="">
                                </div>
                                <div class="text-block">
                                    <div class="delete-icon">
                                        &#10005
                                    </div>
                                    <div class="name">
                                        <p>Йодомарин 10 табл.</p>
                                    </div>
                                    <div class="status">
                                        <p><img src="images/ok.png" alt="">Есть в наличии</p>
                                    </div>
                                    <div class="flex-one">
                                        <div class="price">
                                            <p class="gradient-text">369 <span> тг.</span></p>
                                        </div>
                                        <div class="sale-basket-button">
                                            <button><img src="images/light-basket.png" alt="">В корзину</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="catalog-item">
                                <div class="image">
                                    <img src="images/img1.png" alt="">
                                </div>
                                <div class="text-block">
                                    <div class="delete-icon">
                                        &#10005
                                    </div>
                                    <div class="name">
                                        <p>Йодомарин 10 табл.</p>
                                    </div>
                                    <div class="status">
                                        <p><img src="images/ok.png" alt="">Есть в наличии</p>
                                    </div>
                                    <div class="flex-one">
                                        <div class="price">
                                            <p class="gradient-text">369 <span> тг.</span></p>
                                        </div>
                                        <div class="sale-basket-button">
                                            <button><img src="images/light-basket.png" alt="">В корзину</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="catalog-item">
                                <div class="image">
                                    <img src="images/img1.png" alt="">
                                </div>
                                <div class="text-block">
                                    <div class="delete-icon">
                                        &#10005
                                    </div>
                                    <div class="name">
                                        <p>Йодомарин 10 табл.</p>
                                    </div>
                                    <div class="status">
                                        <p><img src="images/ok.png" alt="">Есть в наличии</p>
                                    </div>
                                    <div class="flex-one">
                                        <div class="price">
                                            <p class="gradient-text">369 <span> тг.</span></p>
                                        </div>
                                        <div class="sale-basket-button">
                                            <button><img src="images/light-basket.png" alt="">В корзину</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="catalog-item">
                                <div class="image">
                                    <img src="images/img1.png" alt="">
                                </div>
                                <div class="text-block">
                                    <div class="delete-icon">
                                        &#10005
                                    </div>
                                    <div class="name">
                                        <p>Йодомарин 10 табл.</p>
                                    </div>
                                    <div class="status">
                                        <p><img src="images/ok.png" alt="">Есть в наличии</p>
                                    </div>
                                    <div class="flex-one">
                                        <div class="price">
                                            <p class="gradient-text">369 <span> тг.</span></p>
                                        </div>
                                        <div class="sale-basket-button">
                                            <button><img src="images/light-basket.png" alt="">В корзину</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
</div>
<?php require_once 'footer.php' ?>