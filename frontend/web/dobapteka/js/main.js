$(window).scroll(function () {
  var sticky = $(".hidden-head"),
    scroll = $(window).scrollTop();
  if (scroll >= 100) sticky.addClass("mobile-header");
  else sticky.removeClass("mobile-header");
});
$(document).ready(function () {
  var maxField = 10; //Input fields increment limitation
  var addButton = $(".add_button"); //Add button selector
  var wrapper = $(".field_wrapper"); //Input field wrapper
  var fieldHTML = `<div class="personal-input"><div class="icon"><img src="images/loc-icon.png" alt=""></div><input type="text" placeholder="Адрес"><a href="javascript:void(0);" class="remove_button"><i class="fas fa-minus-circle"></i></a></div></div>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDue_iqvkRNPVNFsq6iyixSZdDJZwrenMY&libraries=places"></script>`; //New input field html`; //New input field html
  var x = 1; //Initial field counter is 1

  //Once add button is clicked
  $(addButton).click(function () {
    //Check maximum number of input fields
    if (x < maxField) {
      x++; //Increment field counter
      $(wrapper).append(fieldHTML); //Add field html
      var input = document.querySelectorAll(".searchTextField");
      var options = {
        componentRestrictions: { country: "kz" },
      };
      input.forEach((inputt) => {
        var autocomplete = new google.maps.places.Autocomplete(inputt, options);
      });
      function setAutocompleteCountry() {
        var country = document.getElementById("country").value;
        if (country == "all") {
          autocomplete.setComponentRestrictions({ country: [] });
          map.setCenter({ lat: 15, lng: 0 });
          map.setZoom(2);
        } else {
          autocomplete.setComponentRestrictions({ country: country });
          map.setCenter(countries[country].center);
          map.setZoom(countries[country].zoom);
        }
        clearResults();
        clearMarkers();
      }
    }
  });

  //Once remove button is clicked
  $(wrapper).on("click", ".remove_button", function (e) {
    e.preventDefault();
    $(this).parent("div").remove(); //Remove field html
    x--; //Decrement field counter
  });
});

(function ($) {
  $(".loc").click(function () {
    $(".pick-city-modal,.overlay").show();
  });
  $(".close-modal").click(function () {
    $(".pick-city-modal,.overlay").hide();
  });
  $(".bonus-cost").click(function () {
    $(".bonus-cost").toggleClass("opacity-bonus");
  });
  $(".gift-item").click(function () {
    $(this).find(".checked").toggle();
  });
  $(".instruction").click(function () {
    $(".rotate").toggle();
  });

  $(".modal-drug").hide();
  $(".catalog-menu li ").mouseenter(function () {
    $(this).find(".modal-drug").addClass("wow animated fadeIn").show(0);
  });
  $(".catalog-menu li ").mouseleave(function () {
    $(this).find(".modal-drug").hide();
  });

  $(".show-pass").click(function () {
    $(".show-pass").toggleClass("opacity-pass");
  });
  $("#main-nav").hcOffcanvasNav({
    maxWidth: 980,
  });

  // owl menu tabs

  // Change tab class and display content
  $(".tab-nav").click(function (e) {
    e.preventDefault();
    $(".tab-nav").removeClass("green");
    $(this).addClass("green");
  });
  $(".owl1 .item a").click(function (e) {
    e.preventDefault();

    var href = $(this).attr("href");

    $(".tab-item2 ").removeClass("tab-item2-active ");
    $(href).addClass("tab-item2-active ");
  });
  $(".item a").click(function (e) {
    e.preventDefault();

    var href = $(this).attr("href");

    $(".tab-item ").removeClass("tab-item-active ");
    $(href).addClass("tab-item-active ");
  });

  // end owl menu tabs

  // $('.owl1').owlCarousel({
  //     loop:true,
  //     margin:10,
  //     nav:true,
  //     responsive:{
  //         0:{
  //             items:2
  //         },
  //         600:{
  //             items:3
  //         },
  //         1000:{
  //             items:5,
  //             nav:true
  //         }
  //     }
  // });
  // $('.owl2').owlCarousel({
  //     loop:true,
  //     margin:10,
  //     nav:true,
  //     responsive:{
  //         0:{
  //             items:2
  //         },
  //         600:{
  //             items:3
  //         },
  //         1000:{
  //             items:5,
  //             nav:true
  //         }
  //     }
  // });
  // $('.owl-main').owlCarousel({
  //     loop:true,
  //     margin: 10,
  //     nav:true,
  //     responsive:{
  //         0:{
  //             items:1,
  //             dots: true,
  //         },
  //         600:{
  //             items:1,
  //         },
  //         1000:{
  //             items:1
  //         }
  //     }
  // });
})(jQuery);

function showPass() {
  var x = document.getElementById("myInput");
  if (x.type === "password") {
    x.type = "text";
  } else {
    x.type = "password";
  }
}
function showPass2() {
  var x = document.getElementById("myInput2");
  if (x.type === "password") {
    x.type = "text";
  } else {
    x.type = "password";
  }
}
function showPassm() {
  var x = document.getElementById("myInputm");
  if (x.type === "password") {
    x.type = "text";
  } else {
    x.type = "password";
  }
}
function showPassm2() {
  var x = document.getElementById("myInput2m");
  if (x.type === "password") {
    x.type = "text";
  } else {
    x.type = "password";
  }
}

// When the user scrolls the page, execute myFunction
window.onscroll = function () {
  myFunction();
};

// Get the header
var header = document.getElementById("myHeader");

// Get the offset position of the navbar
var sticky = header.offsetTop;
