<?php
namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 */
class User extends ActiveRecord implements IdentityInterface
{

    public $password;
    public $password_verify;

    const update_all_data = 'update_all_data';
    const update_data = 'update_data';
    const admin = 'admin';
    const client = 'client';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [

            [['email','password','password_verify', 'username'], 'required'],
            ['email','email'],
            [['password','password_verify'], 'string', 'min' => 6],
            [['password_verify'], 'compare', 'compareAttribute' => 'password'],
            [['username'], 'required'],
            [['username'], 'unique', 'targetClass'=>'common\models\User'],
        ];
    }



    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Логин',
            'password' => 'Пароль',
            'password_verify' => 'Подтверждение пароля',
            'created_at' => 'Дата создание',
            'updated_at' => 'Дата редактирование'
        ];
    }

    public function scenarios()
    {
        return [
            self::update_all_data => ['email','password','password_verify', 'username'],
            self::update_data => ['email', 'username'],
            self::admin => ['username'],
            self::client => ['username', 'email', 'password']
        ];
    }


    public function getCreatedDate(){
        Yii::$app->formatter->locale = 'ru-RU';
        return date('d/m/Y H:i:s', $this->created_at);
    }


    public function getUpdatedDate(){
        Yii::$app->formatter->locale = 'ru-RU';
        return date('d/m/Y H:i:s', $this->updated_at);
    }


    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    public static function findByResetToken($token)
    {
        return static::findOne(['password_reset_token' => $token]);
    }

    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /**
     * Removes password reset token
     */


    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                if(isset($this->password)){
                    $this->setPassword($this->password);
                }elseif(empty($this->password_hash)) {
                    $this->setPassword(\Yii::$app->security->generateRandomString());
                }
                $this->generateAuthKey();
            }
            return true;
        }

        return false;
    }


    public function signUp()
    {
        if (!$this->validate()) {
            return null;
        }

        $user = new User();
        $user->username = $this->email;
        $user->email = $this->email;
        $user->generateAuthKey();
        return $user->save(false) ? $user : null;
    }


    public static function getRandomPassword() {
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $password = "";
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, strlen($alphabet)-1);
            $password.= $alphabet[$n];
        }
        return $password;
    }

    public static function getAdmin(){
        return User::findOne(Yii::$app->user->id);
    }

    public function deleteAdmission(){
        Admission::deleteAll(['user_id' => $this->id]);
    }

    public function deleteProfile(){
        UserProfile::deleteAll(['user_id' => $this->id]);
    }

    public function deleteBonus(){
        UserBalance::deleteAll(['user_id' => $this->id]);
    }

    public function deleteAddresses(){
        UserAddress::deleteAll(['user_id' => $this->id]);
    }

    public function deleteProducts(){
        UserAddedProduct::deleteAll(['user_id' => $this->id]);
    }

    public function deleteFavorites(){
        UserFavorites::deleteAll(['user_id' => $this->id]);
    }

    public function deleteGift(){
        UserGift::deleteAll(['user_id' => $this->id]);
    }


    public function getProfile(){
        return $this->hasOne(UserProfile::className(), ['user_id' => 'id']);
    }


    public function getAddress(){
        return $this->hasOne(UserAddress::className(), ['user_id' => 'id']);
    }


    public function getFio(){
        return $this->profile ? $this->profile->fio : "Не указано";
    }


    public function getDateOfBirth(){
        return $this->profile ? $this->profile->date_of_birth : "Не указано";
    }

    public function getAddressName(){
        return $this->address ? $this->address->address : "Не указано";
    }


}
