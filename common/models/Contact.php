<?php
namespace common\models;

use Yii;

/**
 * This is the model class for table "Contact".
 *
 * @property int $id
 * @property string $telephone
 * @property string $address
 * @property string $instagram
 * @property string $facebook
 */


class Contact extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'contact';
    }

    public function rules()
    {
        return [
            [['telephone', 'address', 'instagram', 'facebook'], 'required'],
            [['address'], 'string'],
            [['telephone', 'instagram', 'facebook'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'telephone' => 'Телефон',
            'address' => 'Адрес',
            'instagram' => 'Ссылка на Instagram',
            'facebook' => 'Ссылка на Facebook',
        ];
    }

    public static function getContent(){
        return Contact::find()->one();
    }
}
