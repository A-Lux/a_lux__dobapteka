<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "menu".
 *
 * @property int $id
 * @property string $text
 * @property int $status
 * @property int $isNew
 * @property int $content
 * @property string $metaName
 * @property string $metaDesc
 * @property string $metaKey
 */
class Menu extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'menu';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['text', 'status', 'metaName','url'], 'required'],
            [['status','isNew'], 'integer'],
            [['metaDesc', 'metaKey','content'], 'string'],
            [['text', 'metaName','url', 'icon'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'text' => '	Заголовок',
            'status' => 'Меню',
            'content' => 'Содержание',
            'metaName' => 'Мета Названия',
            'metaDesc' => 'Мета Описание',
            'metaKey' => 'Ключевые слова',
            'url' => 'Ссылка',
            'icon' => 'Иконка'
        ];
    }

    public static function getAll(){
        return Menu::find()->where('status=1')->orderBy('sort asc')->all();
    }

}
