<?php
namespace common\models;

use Yii;

/**
 * This is the model class for table "TriggerHoliday".
 * @property int $id
 * @property string $content
 * property string $date
*/

class TriggerHoliday extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'trigger_holiday';
    }

    public function rules()
    {
        return [
            [['date', 'content'], 'required'],
            [['date'], 'safe'],
            [['content'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date' => 'Дата',
            'content' => 'Текст',
        ];
    }

    public function getDate()
    {
        Yii::$app->formatter->locale = 'ru-RU';
        return Yii::$app->formatter->asDate($this->date, 'dd MMMM');
    }

    public static function getAll(){
        return TriggerHoliday::find()->all();
    }
}
