<?php
namespace common\models;

use Yii;

/**
 * This is the model class for table "Mainsub".
 *
 * @property int $id
 * @property string $name
 */

class Mainsub extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'mainsub';
    }

    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
        ];
    }

    public static function getAll(){
        return Mainsub::find()->all();
    }
}
