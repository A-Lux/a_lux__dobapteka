<?php
namespace common\models;

use Yii;

/**
 * This is the model class for table "News".
 *
 * @property int $id
 * @property int $sort
 * @property string $name
 * @property string $image
 * @property string $short_description
 * @property string $full_description
 * @property string $metaName
 * @property string $metaDesc
 * @property string $metaKey
 * @property string $created_at
 * @property string $updated_at
 */

class News extends \yii\db\ActiveRecord
{
    public $files;
    public $path = 'images/news/';

    public static function tableName()
    {
        return 'news';
    }

    public function rules()
    {
        return [
            [['name', 'short_description', 'full_description', 'metaName'], 'required'],
            [['short_description', 'full_description', 'metaDesc', 'metaKey'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['sort'], 'integer'],
            [['name', 'metaName'], 'string', 'max' => 255],
            [['image'], 'file', 'extensions' => 'png,jpg'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'image' => 'Картинка',
            'short_description' => 'Краткое описание',
            'full_description' => 'Полное описание',
            'metaName' => 'Мета Названия',
            'metaDesc' => 'Мета Описание',
            'metaKey' => 'Ключевые слова',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата редактирования',
            'sort' => 'Сортировка',
        ];
    }


    public function getImage(){
        return '/backend/web/'.$this->path.''.$this->image;
    }

    public function getImageUrl(){
        return '@backend/web/'.$this->path;
    }

    public static function getAll(){
        return self::find()->orderBy('sort ASC')->all();
    }




}
