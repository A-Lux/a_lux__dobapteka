<?php
namespace common\models;

use Yii;

/**
 * This is the model class for table "TriggerRemindBasket".
 * @property int $id
 * @property string $content
*/

class TriggerRemindBasket extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'trigger_remind_basket';
    }

    public function rules()
    {
        return [
            [['content'], 'required'],
            [['content'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'content' => 'Текст',
        ];
    }

    public static function getContent(){
        return TriggerRemindBasket::find()->one();
    }

}
