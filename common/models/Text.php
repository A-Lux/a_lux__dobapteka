<?php
namespace common\models;

use Yii;

/**
* This is the model class for table "Text".
* @property int $id
* @property string $text
* @property int $type_id
*/

class Text extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'text';
    }

    public function rules()
    {
        return [
            [['text', 'type_id'], 'required'],
            [['text'], 'string'],
            [['type_id'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'text' => 'Текст',
            'type_id' => 'Тип',
        ];
    }

    public function getType()
    {
        return $this->hasOne(TextType::className(), ['id' => 'type_id']);
    }

    public static function getSMS(){
        $model = Text::findOne(["type_id" => 4]);
        return $model->text;
    }

    public static function getAll(){
        return self::find()->all();
    }
}
