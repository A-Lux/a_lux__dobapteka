<?php

use yii\helpers\Html;

$this->title = 'Создание Filial Product';
$this->params['breadcrumbs'][] = ['label' => 'Filial Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="filial-product-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
