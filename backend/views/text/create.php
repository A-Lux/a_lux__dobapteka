<?php

use yii\helpers\Html;

$this->title = 'Создание текста';
$this->params['breadcrumbs'][] = ['label' => 'Текст по сайту', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="text-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
