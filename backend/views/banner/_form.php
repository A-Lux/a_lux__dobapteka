<?php

use kartik\file\FileInput;
use mihaildev\ckeditor\CKEditor;
use yii\helpers\Html;
use yii\widgets\ActiveForm;


?>

<div class="banner-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php
        echo $form->field($model, 'text')->widget(CKEditor::className(),[
            'editorOptions' => [
                'preset' => 'full', // basic, standard, full
                'inline' => false, //по умолчанию false
            ],
        ]);
    ?>

    <?= $form->field($model, 'url')->textarea() ?>


    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>
