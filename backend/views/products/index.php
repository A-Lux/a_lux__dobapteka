<?php


use backend\controllers\LabelNew;
use backend\controllers\LabelPopular;
use yii\helpers\Html;
use yii\grid\GridView;

$this->title = ' Продукция';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="products-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>

        <?= Html::a('Сортировка (Топ продаж)', ['products/index-hit'], ['class' => 'btn btn-primary']) ?>

        <?= Html::a('Сортировка (Товар месяца)', ['products/index-top-month'], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                 'attribute' => 'category_id',
                 'value' => function ($model) {
                    if($model->category != null){
                        return $model->category->name;
                    }else{
                        return null;
                    }

                 },
                 'format' => 'raw',
                 'contentOptions' => ['style' => 'text-align: center'],
            ],

            [
                'attribute' => 'name',
                'value' => function ($data) {
                    return $data->name;
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'isHit',
                'filter' => LabelPopular::statusList(),
                'value' => function ($model) {
                    return LabelPopular::statusLabel($model->isHit);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'isNew',
                'filter' => LabelNew::statusList(),
                'value' => function ($model) {
                    return LabelNew::statusLabel($model->isNew);
                },
                'format' => 'raw',
            ],

                ['class' => 'yii\grid\ActionColumn'],
            ],
    ]); ?>
</div>
