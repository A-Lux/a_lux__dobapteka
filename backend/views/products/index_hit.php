<?php


use backend\controllers\LabelNew;
use backend\controllers\LabelPopular;
use yii\helpers\Html;
use yii\grid\GridView;

$this->title = ' Сортировка';
$this->params['breadcrumbs'][] = ['label' => 'Продукция', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="products-index">


    <h1>Сортировка (Топ продаж)</h1>

    <a href="/admin/products" class="btn btn-primary">Назад</a>
    <br><br>

    <div id="sortResults">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'id',
                [
                    'attribute' => 'name',
                    'value' => function($model){
                        return $model->name;
                    },
                    'format' => 'raw',
                ],

                [
                    'attribute' => 'sort',
                    'filter' => false,
                    'format' => 'raw',
                    'value' => function ($data) {
                            return '<input type="number" value="' . $data->sort_hit . '" 
                        class = "sort" kind="isHit" id="'.$data->id.'">';
                    }

                ],

                [

                    'format' => 'raw',

                    'value' => function($model) {

                        return Html::a('<button class="btn btn-success" title="Сохранить">
                                    <span class="glyphicon glyphicon-saved" style="color: white;"></span></button>',
                            \yii\helpers\Url::to(['products/move', 'id' => $model->id, 'new_sort' => $model->sort_hit, 'type' => 'isHit']),[
                                'id' => 'a_'.$model->id,
                            ]);

                    }

                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                ],
            ],
        ]); ?>
    </div>
</div>



<script>

    $("body").on("keyup", ".sort", function (e) {

        let id = $(this).attr('id');
        let type = $(this).attr('kind');
        let new_sort = $(this).val();
        document.getElementById("a_"+id).href= '/admin/products/move?id='+id+'&new_sort='+new_sort+'&type='+type;
    });
</script>
