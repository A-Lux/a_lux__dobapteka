<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Emailforrequest */

$this->title = 'Эл. почта для связи';
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="emailforrequest-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
