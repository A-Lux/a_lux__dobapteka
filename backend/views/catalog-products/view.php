<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Категории', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="catalog-products-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'url' => $model->url], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'url' => $model->url], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить этот элемент?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'parent_id',
                'value' => function ($model) {
                    if($model->parent != null){
                        return
                            Html::a($model->parent->name, ['view', 'url' => $model->parent->url]);
                    }else{
                        return null;
                    }
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'title',
                'value' => $model->title,
                'format' => 'raw',
            ],
            [
                'attribute' => 'description',
                'value' => $model->description,
                'format' => 'raw',
            ],
            [
                'attribute' => 'name',
                'value' => $model->name,
                'format' => 'raw',
            ],
            [
                'attribute' => 'url',
                'value' => $model->url,
                'format' => 'raw',
            ],

            [
                'format' => 'html',
                'attribute' => 'image',
                'value' => function($data){
                    return Html::img($data->getDesktopImage(), ['width'=>50]);
                }
            ],
            [
                'format' => 'html',
                'attribute' => 'mobile_image',
                'value' => function($data){
                    return Html::img($data->getMobileImage(), ['width'=>50]);
                }
            ],

            [
                'attribute' => 'status',
                'filter' => \backend\controllers\Label::statusList(),
                'value' => function ($model) {
                    return \backend\controllers\Label::statusLabel($model->status);
                },
                'format' => 'raw',
            ],
            'created_at',
        ],
    ]) ?>

</div>

<br>

<h3>
    Категории
</h3>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        'id',
        [
            'value' => function ($data) {

                return
                    Html::a('<span class="glyphicon glyphicon-arrow-up"></span>', ['move-up-by-category', 'id' => $data->id, 'catalog_id' => $data->parent_id]) .
                    Html::a('<span class="glyphicon glyphicon-arrow-down"></span>', ['move-down-by-category', 'id' => $data->id,'catalog_id' => $data->parent_id]);
            },
            'format' => 'raw',
            'contentOptions' => ['style' => 'text-align: center'],
        ],



        [
            'attribute' => 'name',
            'value' => function ($data) {
                return $data->name;
            },
            'format' => 'raw',
        ],

        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{view} {update} {delete}',
            'buttons'=>[
                'view' => function ($url, $data) {
                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', '/admin/catalog-products/view?url='.$data->url);
                },
                'update' => function ($url, $data) {
                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', '/admin/catalog-products/update?url='.$data->url);
                },
                'delete' => function ($url, $data) {
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', '/admin/catalog-products/delete?url='.$data->url);
                },
            ]
        ],
    ],
]); ?>
