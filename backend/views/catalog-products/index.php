<?php
use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'Категории';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="catalog-products-index">

    <h1><?= Html::encode($this->title) ?></h1>
<!--    <p>-->
<!--        --><?//= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
<!--    </p>-->
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'value' => function ($model) {
                    if($model->level == 1){
                        return
                            Html::a('<span class="glyphicon glyphicon-arrow-up"></span>', ['move-up', 'id' => $model->id]) .
                            Html::a('<span class="glyphicon glyphicon-arrow-down"></span>', ['move-down', 'id' => $model->id]);
                    }else{
                        return '';
                    }

                },
                'format' => 'raw',
                'contentOptions' => ['style' => 'text-align: center'],
            ],
            [
                'attribute' => 'parent_id',
                'value' => function ($model) {
                    if($model->parent != null){
                        return
                            Html::a($model->parent->name, ['view', 'url' => $model->parent->url]);
                    }else{
                        return null;
                    }
                },
                'format' => 'raw',
            ],


            [
                'attribute' => 'name',
                'value' => function ($data) {
                    return $data->name;
                },
                'format' => 'raw',
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {delete}',
                'buttons'=>[
                    'view' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', '/admin/catalog-products/view?url='.$model->url);
                    },
                    'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', '/admin/catalog-products/update?url='.$model->url);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', '/admin/catalog-products/delete?url='.$model->url);
                    },
                ]
            ],
        ],
    ]); ?>
</div>
