<?php

use kartik\date\DatePicker;
use kartik\datetime\DateTimePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use mihaildev\ckeditor\CKEditor;

$percent = [];
for($i=1;$i<99;$i++){
    $percent[$i] = $i;
}
?>

<?php if(Yii::$app->session->getFlash('start_date_error')):?>
    <div class="alert alert-danger" role="alert">
        <?= Yii::$app->session->getFlash('start_date_error'); ?>
    </div>
<?php endif;?>
<?php if(Yii::$app->session->getFlash('end_date_error')):?>
    <div class="alert alert-danger" role="alert">
        <?= Yii::$app->session->getFlash('end_date_error'); ?>
    </div>
<?php endif;?>

<div class="promo-code-form">
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'percent')->dropDownList($percent) ?>

    <?= $form->field($model, 'quantity')->textInput() ?>

    <div class="form-group field-promocode-start_date required">
        <label class="control-label" for="promocode-start_date">Дата начала</label>
        <?php
        echo DateTimePicker::widget([
            'name' => 'PromoCode[start_date]',
            'value' => $model->start_date,
            'options' => ['placeholder' => 'Выберите дату...'],
            'pluginOptions' => [
                'format' => 'yyyy-mm-dd hh:i',
                'todayHighlight' => true
            ]
        ]);
        ?>
        <div class="help-block"></div>
    </div>


    <div class="form-group field-promocode-end_date required">
        <label class="control-label" for="promocode-end_date">Дата окончания</label>
        <?php
        echo DateTimePicker::widget([
            'name' => 'PromoCode[end_date]',
            'value' => $model->end_date,
            'options' => ['placeholder' => 'Выберите дату...'],
            'pluginOptions' => [
                'format' => 'yyyy-mm-dd hh:i',
                'todayHighlight' => true
            ]
        ]);
        ?>
        <div class="help-block"></div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    </div>
