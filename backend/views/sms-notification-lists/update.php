<?php

use yii\helpers\Html;

$this->title = 'Редактирование' . $model->phone;
$this->params['breadcrumbs'][] = ['label' => 'Список рассылки СМС-уведомлений', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->phone, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="sms-notification-lists-update">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
