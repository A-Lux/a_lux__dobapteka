<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = 'Бесплатная доставка';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="price-free-delivery-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update'], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'format' => 'raw',
                'attribute' => 'price',
                'value' => function ($data) {
                    return number_format($data->price, 0, '', ' ') . ' тг';
                }
            ],
        ],
    ]) ?>

</div>