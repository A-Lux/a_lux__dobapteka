<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = 'Заказ: '.$model->id;
$this->params['breadcrumbs'][] = ['label' => 'Заказы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="orders-view" style="padding-bottom: 600px;">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить этот элемент?',
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::button('Копировать', ['class' => 'btn btn-success', 'data-clipboard-target' => '#data' ,'id' => 'copy']) ?>
    </p>

    <? if($model->typeDelivery == 1) {
        $address =  ['attribute' => 'address', 'value' => function($model){return $model->address;}];
        $statusProgress =  ['attribute' => 'statusProgress','value' => function ($model) {return backend\controllers\LabelProgress::statusLabel($model->statusProgress);},'format' => 'raw'];
    }else {
        $address = ['attribute' => 'Адрес забора', 'value' => function ($model) {return $model->address;}];
        $statusProgress =  ['attribute' => 'Статус забора','value' => function ($model) {return backend\controllers\LabelDelivery::statusLabel($model->statusProgress);},'format' => 'raw'];
    }?>


    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [

            'id',
            [
                'attribute'=>'Заказчик',
                'value'=>function($model) {
                    if ($model->user_id) return 'Зарегистрирован';
                    else return 'Не зарегистрирован';
                }
            ],
            [
                'attribute'=>'user_id',
                'value'=>function($model) {
                    if ($model->user_id) return $model->userProfile->fio;
                    else return $model->fio;
                }
            ],
            [
                'attribute'=>'Телефон',
                'value'=>function($model){
                    if ($model->user_id) return $model->user->username;
                    else return $model->telephone;
                }
            ],

            [
                'attribute'=> "Email",
                'value'=>function($model){
                    if ($model->user_id) return $model->user->email;
                     else return $model->email;
                }
            ],


            [
                'attribute' => 'typePay',
                'filter' => \backend\controllers\LabelType::statusList(),
                'value' => function ($model) {
                    return backend\controllers\LabelType::statusLabel($model->typePay);
                },
                'format' => 'raw',
            ],

            [
                'attribute' => 'typeDelivery',
                'filter' => \backend\controllers\LabelDeliveryMethod::statusList(),
                'value' => function ($model) {
                    return backend\controllers\LabelDeliveryMethod::statusLabel($model->typeDelivery);
                },
                'format' => 'raw',
            ],

            [
                'attribute' => 'statusPay',
                'filter' => \backend\controllers\LabelPay::statusList(),
                'value' => function ($model) {
                    return \backend\controllers\LabelPay::statusLabel($model->statusPay);
                },
                'format' => 'raw',
            ],


            $statusProgress,


            [
                'format' => 'raw',
                'attribute' => 'change',
                'value' => function($model){
                    return $model->change.' тг';
                }
            ],
            [
                'format' => 'raw',
                'attribute' => 'used_promo',
                'value' => function($model){
                    return $model->used_promo.' %';
                }
            ],
            [
                'format' => 'raw',
                'attribute' => 'used_elderly_discount',
                'value' => function($model){
                    return $model->used_elderly_discount	.' %';
                }
            ],
            [
                'format' => 'raw',
                'attribute' => 'used_bonus',
                'value' => function($model){
                    return $model->used_bonus.' тг';
                }
            ],

            [
                'format' => 'raw',
                'attribute' => 'deliverySum',
                'value' => function($model){
                    return $model->deliverySum.' тг';
                }
            ],

            [
                'format' => 'raw',
                'attribute' => 'sum',
                'value' => function($model){
                    return $model->sum.' тг';
                }
            ],


            $address,

            [
                'format' => 'raw',
                'attribute' => 'time',
                'value' => function($model){
                    if($model->time != null){
                        return $model->time.' минут';
                    }else{
                        return '(не задано)';
                    }

                }
            ],
            'delivery_id',
            'message_id',
            [
                'attribute' => 'created',
                'value' => function ($model) {
                    return $model->created;
                },
                'format' => 'raw',
            ],
        ],
    ]) ?>



    <div class="box" id = 'result'>
        <div class="box-header">
            <h3 class="box-title">Продукты заказа:</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Товар</th>
                    <th>Количества</th>
                    <th>Цена</th>
                </tr>
                </thead>
                <tbody>
                <? if($products != null):?>
                    <? foreach ($products as $v):?>

                        <? if($v->product != null):?>
                            <tr>
                                <td><a href="/admin/products/view?id=<?=$v->product->id;?>">
                                        <img src="<?=$v->product->getImage();?>" width="50">  <?=$v->product->name;?></a></td>
                                <td><?=$v->count;?></td>
                                <td><?=$v->product->calculatePrice*$v->count;?></td>
                            </tr>
                        <? else:?>
                            <tr>
                                <td>Не найдено, ID:<?=$v->product_id;?></td>
                                <td></td>
                                <td></td>
                            </tr>
                        <? endif;?>
                    <? endforeach;?>
                <? endif;?>
                </tbody>
            </table>
        </div>
    </div>



    <? if($gift != null):?>
        <div class="box" id = 'result'>
            <div class="box-header">
                <h3 class="box-title">Подарки:</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>Товар</th>
                        <th>Количества</th>
                    </tr>
                    </thead>
                    <tbody>
                    <? foreach ($gift as $v):?>
                        <? if($v->product != null):?>
                            <tr>
                                <td><a href="/admin/products/view?id=<?=$v->product->id;?>">
                                        <img src="<?=$v->product->getImage();?>" width="50">  <?=$v->product->name;?></a></td>
                                <td><?=$v->count;?></td>
                            </tr>
                        <? else:?>
                            <tr>
                                <td></td>
                                <td>Не найдено</td>
                                <td></td>
                            </tr>
                        <? endif;?>
                    <? endforeach;?>
                    </tbody>
                </table>
            </div>
        </div>
    <? endif;?>


    <? if($model->typeDelivery == 1):?>
    <br><br>
    <div class="box" id = 'result'>
        <div class="box-header">
            <h3 class="box-title">Ближайшие аптеки с товарами</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Аптека</th>
                    <th>Товар</th>
                    <th>Количество</th>
                </tr>
                </thead>
                <tbody id="nearlyPharmaciesData">
                    <tr>
                        <td></td>
                        <td style="text-align: center">
                            <i class="fa fa-refresh fa-spin" aria-hidden="true" style="font-size:46px;"></i>
                        </td>
                        <td></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    <br><br>
    <div id="map" style="width: 100%; height: 450px"></div>

    <?
        if ($model->user_id) $fio = $model->userProfile->fio;
        else $fio = $model->fio;

        if ($model->user_id) $tel =  $model->user->username;
        else $tel = $model->telephone;
    ?>

    <script>

        ymaps.ready(init);
        function init() {

            var center_map = [0, 0];
            var map = "";

            map = new ymaps.Map('map', {
                center: center_map,
                zoom: 10,
            });

            map.setCenter([<?=$model->latitude?>, <?=$model->longitude?>]);

            map.geoObjects.add(
                new ymaps.Placemark([<?=$model->latitude?>, <?=$model->longitude?>], {
                    balloonContent: '<div class="company-name wow fadeInUp"><?=$fio;?></br><?=$tel;?></div>',
                    iconCaption: '<?=$fio;?>',
            }, {
                preset: 'islands#darkGreenPersonIcon',
            }));



            // START NEARLY ADDRESS
            var address = [];
            var distance = [];
            var products = [];
            var remainder = [];

            <? foreach ($products as $val) : ?>
                products.push([<?=$val->product_id;?>, <?=$val->count;?>]);
            <? endforeach;?>

            <? foreach ($remainder as $val) : ?>
                <? if($val->filial_id != null && $val->filial_id != '' && $val->filial_id > 0):?>
                    if(remainder[<?=$val->filial_id;?>] == null){
                        remainder[<?=$val->filial_id;?>] = [];
                    }

                    remainder[<?=$val->filial_id;?>][<?=$val->product_id;?>] = <?=$val->amount;?>;
                <? endif;?>

            <? endforeach;?>

            <? foreach ($addresses as $val) : ?>

                var dist = ymaps.coordSystem.geo.getDistance([<?=$model->latitude;?>, <?=$model->longitude;?>],
                    [<?= $val->latitude; ?>, <?= $val->longitude; ?>]);
                distance.push(dist);
                address.push([dist, "<?= $val->address; ?>", <?= $val->id; ?>, "<?= $val->name;?>", "<?= $val->latitude ?>", "<?= $val->longitude ?>"]);

            <? endforeach; ?>

            var response = [];
            var deletes = [];
            var amounts = [];
            var selected_filials = [];
            var key = 0, i, k, z, q, max, product_id, product_count, pharmacy_id, count;
            var pharmacy_amount, pharmacy_distance, pharmacy_name, pharmacy_num, latitude, longitude;

            for (var f = 0; f < address.length; f++) {

                max = 0;
                // create and update addresses amounts
                for (var t = 0; t < address.length; t++) {
                    var amount = 0;
                    pharmacy_id = parseInt(address[t][2]);
                    for (z = 0; z < products.length; z++) {

                        var check = 0;
                        product_count = products[z][1];

                        if (typeof remainder[pharmacy_id] != "undefined") {
                            if (typeof remainder[pharmacy_id][parseInt(products[z][0])] != "undefined") {
                                check = remainder[pharmacy_id][parseInt(products[z][0])];
                            }
                        }

                        if (check >= product_count) {
                            amount++;
                        }
                    }

                    if (!selected_filials.includes(t)) {
                        if (max < amount) {
                            max = amount;
                        }
                        amounts[pharmacy_id] = amount;
                    }

                }
                // end create and update addresses amounts

                if(max > 0 && products.length > 0) {
                    key = null;
                    for (i = 0; i < address.length; i++) {
                        pharmacy_id = parseInt(address[i][2]);
                        pharmacy_amount = amounts[pharmacy_id];
                        if (pharmacy_amount == max && !selected_filials.includes(i)) {
                            if (key == null) {
                                key = i;
                            }
                            for (k = 0; k < address.length; k++) {
                                if (i != k && max == amounts[parseInt(address[k][2])]) {
                                    if (address[i][0] > address[k][0] && !selected_filials.includes(k)) {
                                        key = k;
                                    }
                                }
                            }
                        }
                    }
                }else{

                    for (i = 0; i < address.length; i++) {
                        pharmacy_id = parseInt(address[i][2]);
                        if (address[i][0] == Math.min.apply(Math, distance) && products.length > 0
                            && !selected_filials.includes(i)) {
                            key = i;
                        }else{
                            continue;
                        }
                    }
                }


                if(key == null){
                    continue;
                }

                pharmacy_distance = address[key][0];
                pharmacy_name = address[key][1];
                pharmacy_id = parseInt(address[key][2]);
                pharmacy_num = address[key][3];
                latitude = address[key][4];
                longitude = address[key][5];


                // delete max amount address
                const amount_index = pharmacy_id;
                const distance_index = distance.indexOf(pharmacy_distance);
                if (amount_index > -1 && distance_index > -1) {
                    amounts.splice(amount_index, 1);
                    distance.splice(distance_index, 1);
                }
                // end delete max amount address

                for (z = 0; z < products.length; z++) {

                    product_id = parseInt(products[z][0]);
                    product_count = products[z][1];
                    count = 0;

                    if (typeof remainder[pharmacy_id] != "undefined") {
                        if (typeof remainder[pharmacy_id][product_id] != "undefined") {
                            count = remainder[pharmacy_id][product_id];
                        }
                    }

                    if (count != 0 && count >= product_count) {
                        // if has all the product
                        response.push([product_id, product_count, pharmacy_name, pharmacy_num, latitude, longitude]);

                        // delete product
                        deletes.push(product_id);
                        // end delete product
                        if(!selected_filials.includes(key)){
                            selected_filials.push(key);
                        }

                    } else if (count != 0) {
                        response.push([product_id, count, pharmacy_name, pharmacy_num, latitude, longitude]);
                        products[z][1] -= count;
                        if(!selected_filials.includes(key)){
                            selected_filials.push(key);
                        }
                    }
                }

                for (q = 0; q < deletes.length; q++) {
                    for (z = 0; z < products.length; z++) {
                        product_id = parseInt(products[z][0]);
                        if (product_id == deletes[q]) {
                            products.splice(z, 1);
                        }
                    }
                }

            }


            for(i = 0;i < response.length;i++){
                map.geoObjects.add(new ymaps.Placemark([response[i][4], response[i][5]], {
                    balloonContent: '<div class="company-name wow fadeInUp">'+response[i][2]+'</div>',
                    iconCaption: response[i][2],
                }, {
                    preset: 'islands#blueMedicalIcon',
                    iconColor: '#0095b6'
                }));
            }


            if(response.length > 0) {
                $.ajax({
                    url: "/admin/orders/get-nearly-products",
                    type: "POST",
                    data: {data: response, view: 'copyNearlyProducts'},
                    success: function (data) {
                        $('#data').append(data)
                    },
                    error: function () {
                        // alert("Что-то пошло не так.");
                    },
                });

                $.ajax({
                    url: "/admin/orders/get-nearly-products",
                    type: "POST",
                    data: {data: response, view: 'nearlyProducts'},
                    success: function (data) {
                        $('#nearlyPharmaciesData').html(data)
                    },
                    error: function () {
                        // alert("Что-то пошло не так.");
                    },
                });
            }else{
                alert('Не найдено')
            }

        }

    </script>

    <? endif;?>


</div>


<? if($model->typeDelivery == 1) {
    $address =  "Адрес доставки: ".$model->address."\n";
}else {
    $address = "Адрес забора: ".$model->address."\n";
}?>


<? if($model->typeDelivery == 1):?>

<textarea id="data"  style="height:0;position:absolute;z-index: -1;">
ФИО: <?=$model->user_id ? $model->userProfile->fio."\n" : $model->fio."\n";?>
Телефон: <?=$model->user_id ? $model->user->username."\n" : $model->telephone."\n";?>
Email: <?=$model->user_id ? $model->user->email."\n" : $model->email."\n";?>
Тип заказа: <?=backend\controllers\LabelType::statusLabelValue($model->typePay)."\n";?>
Тип доставки: <?=backend\controllers\LabelDeliveryMethod::statusLabelValue($model->typeDelivery)."\n";?>
Статус оплаты: <?=\backend\controllers\LabelPay::statusLabelValue($model->statusPay)."\n";?>
Подготовить сдачу с суммы: <?=$model->change." тг\n"?>
Цена доставки: <?=$model->deliverySum ? $model->deliverySum." тг\n" : "Не указано\n";?>
Сумма заказа: <?=$model->sum." тг\n";?>
<?=$address;?>
Время доставки: <?=$model->time != null ? $model->time." минут\n" : "Не указано\n";?>
Дата заказа: <?=$model->created."\n";?>
</textarea>

<? else:?>

<textarea id="data"  style="height:0;position:absolute;z-index: -1;">
ФИО: <?=$model->user_id ? $model->userProfile->fio."\n" : $model->fio."\n";?>
Телефон: <?=$model->user_id ? $model->user->username."\n" : $model->telephone."\n";?>
Email: <?=$model->user_id ? $model->user->email."\n" : $model->email."\n";?>
Тип заказа: <?=backend\controllers\LabelType::statusLabelValue($model->typePay)."\n";?>
Тип доставки: <?=backend\controllers\LabelDeliveryMethod::statusLabelValue($model->typeDelivery)."\n";?>
Статус оплаты: <?=\backend\controllers\LabelPay::statusLabelValue($model->statusPay)."\n";?>
Подготовить сдачу с суммы: <?=$model->change." тг\n"?>
Цена доставки: <?=$model->deliverySum ? $model->deliverySum." тг\n" : "Не указано\n";?>
Сумма заказа: <?=$model->sum." тг\n";?>
<?=$address;?>
Время доставки: <?=$model->time != null ? $model->time." минут\n" : "Не указано\n";?>
Дата заказа: <?=$model->created."\n";?>

Продукты заказа:<?="\n"?> <? $m = 0;?>
<? foreach ($products as $v):?>
<? $m++;?>
№<?=$m?> <?=$v->product->name;?>  X  <?=$v->count."\n";?>
<? endforeach;?>

</textarea>

<? endif;?>



<script>
    var clipboard = new ClipboardJS('#copy');

    clipboard.on('success', function(e) {
        showSuccessAlert("Скопировано")
    });

</script>

