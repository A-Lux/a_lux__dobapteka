<?php
use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'Подарки';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gift-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a('Создать подарок', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'type',
                'filter' => \common\models\Gift::getTypes(),
                'value' => function ($model) {
                    return $model->typeName;
                },
                'format' => 'raw',
            ],

            [
                'attribute' => 'product_id',
                'filter' => \common\models\Gift::getProducts(),
                'value' => function ($model) {
                    return $model->product->name;
                },
                'format' => 'raw',
            ],
            'from_price',
            'to_price',
                ['class' => 'yii\grid\ActionColumn'],
            ],
    ]); ?>
</div>
