<?php

use yii\helpers\Html;

$this->title = 'Создание User Balance';
$this->params['breadcrumbs'][] = ['label' => 'User Balances', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-balance-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
