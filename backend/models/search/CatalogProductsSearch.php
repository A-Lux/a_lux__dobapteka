<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\CatalogProducts;

class CatalogProductsSearch extends CatalogProducts
{
    public function rules()
    {
        return [
            [['id', 'parent_id', 'sort', 'status', 'created_at'], 'integer'],
            [['title', 'description', 'name', 'url', 'img'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params, $catalog_id = null)
    {
        $query = CatalogProducts::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'parent_id' => $this->parent_id,
            'sort' => $this->sort,
            'status' => $this->status,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'url', $this->url]);



        if($catalog_id != null){
            $query->andFilterWhere(['parent_id' => $catalog_id])
                ->andFilterWhere(['<>','level', 1])
            ->orderBy([new \yii\db\Expression('sort IS NULL ASC, sort ASC')]);
        }else{
            $query->orderBy([new \yii\db\Expression('level = 1 DESC, sort ASC')]);
        }



        return $dataProvider;
    }
}
