<?php

namespace backend\controllers;

use Yii;
use common\models\PromoCode;
use backend\models\search\PromoCodeSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
class PromoCodeController extends BackendController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST', 'GET'],
                ],
            ],
        ];
    }


    public function afterAction($action, $result)
    {
        if (!Yii::$app->view->params['admission']->promocode){
            throw new NotFoundHttpException();
        }else {
            return parent::afterAction($action, $result); // TODO: Change the autogenerated stub
        }
    }

    public function actionIndex()
    {
        $searchModel = new PromoCodeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionCreate()
    {
        $model = new PromoCode();
        if ($model->load(Yii::$app->request->post())) {
            if($model->start_date == null){
                Yii::$app->getSession()->setFlash('start_date_error', "Дата начало не должен быть пустым!");
                return $this->render('create', compact('model'));
            }
            if($model->end_date == null){
                Yii::$app->getSession()->setFlash('end_date_error', "Дата окончания не должен быть пустым!");
                return $this->render('create', compact('model'));
            }
            $code = Yii::$app->security->generateRandomString(12);
            $model->code = $code;
            if($model->save()){
                return $this->redirect(['view', 'id' => $model->id]);
            }

        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post())) {
            if($model->start_date == null){
                Yii::$app->getSession()->setFlash('start_date_error', "Дата начало не должен быть пустым!");
                return $this->render('create', compact('model'));
            }
            if($model->end_date == null){
                Yii::$app->getSession()->setFlash('end_date_error', "Дата окончания не должен быть пустым!");
                return $this->render('create', compact('model'));
            }
            if($model->save()){
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = PromoCode::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
