<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 01.03.2019
 * Time: 17:04
 */



function debug($arr){
    return "<pre>".print_r($arr)."</pre>";
}

function isMobile(){
    return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
}

function repairSerializeString($value)
{

    $fixed_data = preg_replace_callback ( '!s:(\d+):"(.*?)";!', function($match) {
        return ($match[1] == strlen($match[2])) ? $match[0] : 's:' . strlen($match[2]) . ':"' . $match[2] . '";';
    },$value );

    return $fixed_data;
}

?>
